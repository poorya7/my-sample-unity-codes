﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace Beelezeball{
public class BeelzeballPlayerManager : ATuioPlayerManager 
{
	int minimumPlayersToStart=1;
	private float timeToReturnToMainScene=8;
	private float timerToReturnToMainScene;
	private float playerRadius=52f;
	private float tailRadius=52;
	private float countdown=0;
	public Text countdownGui;
	public List<GameObject> playersInCenter;
	private Dictionary<long,int> positionIndex;
	private int powerupTailGrowth=3;
	private int tailCount=5;
	private int tailDistance=70;
	private int countDownNumber=3;
	private int totalGameTime=90;
	private const float tailScale = 0.66f;
	public bool gameStarted=false;
	public bool gameFinished=false;
	private float totalGameCountdown=0;
	float timeInCenter=0;
	bool countdownStarted=false;
	
	public GameObject tailPrefab;
	public AudioSource audioSourceCountdown;
	public AudioClip countdown1;
	public AudioClip countdown2;
	public AudioClip countdown3;
	public AudioClip countdown4;
	public AudioClip countdown5;
	float lastCountdownNumber=0;

//	private GUIText winner1Gui;
//	private GUIText winner2Gui;
//	private GUIText winner3Gui;

	//public GameObject playerToMakeTailSticky;

	public string temp="";

	private string[] hexColors={"c000ff" , "bbabff", "3600ff" ,"1bacff" ,"ff1e48" ,"00ffba" ,"00ff1e", "baff00",
			"ffde00", "ff7800", "e6ff72", "ffe4ab","ff0090" ,"abffc1"
	};
	
	private List<Color> playerColors;

	public Dictionary<ATuioPlayer,List<GameObject>> playerTails;

	private Dictionary<ATuioPlayer,List<Vector3>> lastPositions;
	//---------------------------------------------------------------------------------------------------------------------
	protected override void Awake()
	{
		base.Awake();

		//playerToMakeTailSticky=null;
		playersInCenter=new List<GameObject>();
		playerColors=new List<Color>();

		foreach(string hexColor in hexColors)
		{
			Color c=HexToColor(hexColor);
			playerColors.Add(c);
		}
		
		playerTails=new Dictionary<ATuioPlayer,List<GameObject>>();
		lastPositions=new Dictionary<ATuioPlayer,List<Vector3>>();
		positionIndex=new Dictionary<long,int>();

	}
	//---------------------------------------------------------------------------------------------------------------------
	Color HexToColor(string hex)
	{
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		Color c=new Color(r/255f,g/255f,b/255f, 255);
		return c;
	}
	//---------------------------------------------------------------------------------------------------------------------
	private void addTail(ATuioPlayer player,Vector2 position)
	{
		List<GameObject> tailsList=new List<GameObject>();
		for (int i=0;i<tailCount;i++)
		{
			GameObject newTail = (GameObject.Instantiate(tailPrefab, new Vector3(position.x,position.y,0), Quaternion.identity) as GameObject);
			newTail.GetComponent<SpriteRenderer>().color=player.GetComponent<SpriteRenderer>().color;
			newTail.transform.localScale= new Vector3(tailScale,tailScale,tailScale);
			newTail.transform.GetComponent<CircleCollider2D>().radius=tailRadius;
			newTail.GetComponent<Renderer>().enabled=false;
			tailsList.Add(newTail);
		}
		playerTails.Add(player,tailsList);
	}
	//---------------------------------------------------------------------------------------------------------------------
	private void growTail(ATuioPlayer player,Vector2 position)
	{
		List<GameObject> tailsList;
		if (playerTails.ContainsKey(player))
		{
			tailsList=playerTails[player];
		}
		else
		{
			tailsList=new List<GameObject>();
		}
		//ADD 10 MORE TAILS
		for (int i=0;i<powerupTailGrowth;i++)
		{
			GameObject newTail = (GameObject.Instantiate(tailPrefab, new Vector3(position.x,position.y,0), Quaternion.identity) as GameObject);
			newTail.GetComponent<SpriteRenderer>().color=player.GetComponent<SpriteRenderer>().color;
			newTail.transform.localScale=new Vector3(tailScale,tailScale,tailScale);
			newTail.transform.GetComponent<CircleCollider2D>().radius=tailRadius;
			//newTail.renderer.enabled=false;
			newTail.GetComponent<Renderer>().enabled=true;
			newTail.gameObject.GetComponent<Collider2D>().isTrigger=false;
			newTail.tag="Player";
			tailsList.Add(newTail);
		}
		playerTails.Remove(player);
		playerTails.Add(player,tailsList);

	}
	//---------------------------------------------------------------------------------------------------------------------
	private void enableTails()
	{
		foreach(ATuioPlayer player in m_playerList)
		{
			if (playerTails.ContainsKey(player))
			{
				List<GameObject> tailsList=playerTails[player];
				
				foreach(GameObject tail in tailsList)
				{
					tail.GetComponent<Renderer>().enabled=true;
				}
			}
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	public override void AddPlayer (long sessionID, Vector2 position)
	{
		ATuioPlayer player = (GameObject.Instantiate(m_playerPrefab, new Vector3(position.x,position.y,0), Quaternion.identity) as GameObject).GetComponent<ATuioPlayer>();
		player.gameObject.transform.GetComponent<CircleCollider2D>().radius=playerRadius;
		player.SessionID = sessionID;

		if (!positionIndex.ContainsKey(sessionID))
			positionIndex.Add(sessionID,0);
		else
		{
			positionIndex.Remove(sessionID);
			positionIndex.Add(sessionID,0);
		}
		m_playerList.Add(player);
		addTail(player,position);

		if (gameStarted==true && gameFinished==false)
		{
			colorPlayers();
			enableTails();
			enablePlayers();
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	private void colorPlayers()
	{
		foreach(ATuioPlayer player in m_playerList)
		{
            if (gameStarted && !player.GetComponent<BeelzeballPlayer>().IsColored)
			{
                player.GetComponent<BeelzeballPlayer>().IsColored = true;
				GameObject[] players =GameObject.FindGameObjectsWithTag("PlayerUnActive");
				foreach(GameObject p in players)
				{
					p.tag="Player";
				}
				if (playerColors.Count>0)
				{
					player.GetComponent<SpriteRenderer>().color=playerColors[0];
					playerColors.RemoveAt(0);
				}
				else
				{
					player.GetComponent<SpriteRenderer>().color=new Color(Random.Range(0.0f,1.0f),Random.Range(0.0f,1.0f), Random.Range(0.0f,1.0f));
				}
				//COLOR TAILS
				try
				{
					if (playerTails.ContainsKey(player))
					{
						List<GameObject> tailsList=playerTails[player];
						foreach(GameObject tail in tailsList)
						{
							tail.GetComponent<SpriteRenderer>().color=player.GetComponent<SpriteRenderer>().color;
						}
					}
				}
				catch(UnityException ex)
				{
					Debug.Log (ex);
				}
			}
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	public void countDown()
	{
		countdown=countDownNumber-(Time.time-timeInCenter);
		countdownGui.text=((int)countdown+1).ToString();
		if ((int)lastCountdownNumber!=(int)countdown)
			playCountdownSound();
		lastCountdownNumber=countdown;
		if (Time.time-timeInCenter>countDownNumber)
		{
			//STOP COUNTDOWN & START THE GAME
			countdownStarted=false;
			gameStarted=true;
			colorPlayers();
			enableTails();
			enablePlayers();
			totalGameCountdown=Time.time;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playCountdownSound()
	{
		switch((int)countdown+1)
		{
		case 1:
			audioSourceCountdown.clip=countdown1;
			break;
		case 2:
			audioSourceCountdown.clip=countdown2;
			break;
		case 3:
			audioSourceCountdown.clip=countdown3;
			break;
		case 4:
			audioSourceCountdown.clip=countdown4;		
			break;
		case 5:
			audioSourceCountdown.clip=countdown5;		
			break;
		default:
			break;
		}
		if (!audioSourceCountdown.isPlaying)
		{
			audioSourceCountdown.Play();
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	string makeTimeReadable(float countdown)
	{
		string minutes=((int)(countdown/60)).ToString();
		if (minutes.Length<2)
			minutes="0"+minutes;
		string seconds=((int)countdown%60).ToString();
		if (seconds.Length<2)
			seconds="0"+seconds;
		return minutes+":"+seconds;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void enablePlayers()
	{
		GameObject[] players =GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject p in players)
		{
			p.gameObject.GetComponent<Collider2D>().isTrigger=false;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void disablePlayers()
	{
		GameObject[] players =GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject p in players)
		{
			p.gameObject.GetComponent<Collider2D>().isTrigger=true;
		}
		GameObject[] playersSticky =GameObject.FindGameObjectsWithTag("PlayerSticky");
		foreach(GameObject p in playersSticky)
		{
			p.gameObject.GetComponent<Collider2D>().isTrigger=true;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	public void Update()
	{
		GameObject startGameObject=GameObject.FindGameObjectWithTag("startupObject");
		StartGame startGameScript=startGameObject.GetComponent<StartGame>();

		if (startGameScript.creditsFaded==true)
		{
			if (countdownStarted==true)
			{
				countDown();
			}
			updatePlayersInCenterList();
			if (playersInCenter.Count>minimumPlayersToStart-1 && gameStarted==false && gameFinished==false)
			{
				if (countdownStarted==false)
				{
					//START COUNTDOWN
					countdownStarted=true;
					timeInCenter=Time.time;
					countdownGui.enabled=true;
				}
			}
			else if (gameStarted==true)
			{
				countdown=totalGameTime-(Time.time-totalGameCountdown)+1;
				countdownGui.text=makeTimeReadable(countdown);
				if (countdown<=0)
				{
					timerToReturnToMainScene=Time.time;
					finishGame();
				}
			}
			else if (gameFinished==false)
			{
				lastCountdownNumber=0;
				countdownStarted=false;
				timeInCenter=Time.time;
				if (countdownGui!=null)
					countdownGui.text="";
			}
			else
			{
				if (Time.time-timerToReturnToMainScene>timeToReturnToMainScene)
				{
					Application.LoadLevel("GameSelection");
				}
			}
		}

			
		if(Input.GetButtonDown("Quit Game") || Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Q))
		{
			Application.LoadLevel("GameSelection");
		}
		if(Input.GetButtonDown("Start New Round"))
		{
			Application.LoadLevel(Application.loadedLevel);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	public void finishGame()
	{
		GameObject[] remainingBricks=GameObject.FindGameObjectsWithTag("Brick");
		foreach(GameObject br in remainingBricks)
				br.tag = "Test"; // just changing the tag to a unused tag so that bricks don't get destroyed

		//GAME FINISHED
		countdownGui.enabled=false;
		gameStarted=false;
		gameFinished=true;
		
		//DEACTIVATE PLAYERS
		disablePlayers();

		//DEACTIVATE BRICKS
		GameObject[] bricks =GameObject.FindGameObjectsWithTag("Brick");
		foreach(GameObject b in bricks)
		{
			b.gameObject.GetComponent<Collider2D>().isTrigger=true;
		}
		
		//DISABLE BALL SOUNDS
		disableBallSounds();
		
		//REMOVE POWERUPS
		removePowerups();
		
		//GET WINNERS
		showWinners();
	}
	//---------------------------------------------------------------------------------------------------------------------
	void removePowerups()
	{
		GameObject[] tailPowerups=GameObject.FindGameObjectsWithTag("powerupTail");
		GameObject[] tailPowerups2=GameObject.FindGameObjectsWithTag("newPowerupTail");

		GameObject[] stickyPowerups=GameObject.FindGameObjectsWithTag("powerupSticky");
		GameObject[] stickyPowerups2=GameObject.FindGameObjectsWithTag("newPowerupSticky");

		GameObject[] tailPowerups3=GameObject.FindGameObjectsWithTag("onDestroyQueue");

		foreach(GameObject tailPowerup in tailPowerups)
		{
			Destroy(tailPowerup);
		}
		foreach(GameObject tailPowerup in tailPowerups2)
		{
			Destroy(tailPowerup);
		}
		foreach(GameObject tailPowerup in tailPowerups3)
		{
			Destroy(tailPowerup);
		}
		foreach(GameObject stickyPowerup in stickyPowerups)
		{
			Destroy(stickyPowerup);
		}
		foreach(GameObject stickyPowerup in stickyPowerups2)
		{
			Destroy(stickyPowerup);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void disableBallSounds()
	{
		GameObject ball=GameObject.FindGameObjectWithTag("Ball");
		AudioSource[] audioSources=ball.GetComponents<AudioSource>();
		foreach(AudioSource source in audioSources)
		{
			source.enabled=false;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	private void showWinners()
	{

		showEndCredits();

		Text[] winnersGui=new Text[3];
		string[] winnerText=new string[]{"1st - ","2nd - ","3rd - "};

		GameObject ball=GameObject.FindGameObjectWithTag("Ball");
		BallMove ballMoveScript = ball.GetComponent<BallMove>();
		Dictionary<Color,int> top3Winners=ballMoveScript.top3Winners;
		string[] winners=new string[3];
		for (int i=0;i<3;i++)
			winners[i]="";

		
		if (top3Winners.Count<1)
		{
			winnersGui[0]=GameObject.FindGameObjectWithTag("Winner2Gui").GetComponent<Text>();
			winnersGui[0].enabled=true;
			winnersGui[0].text="No Winner!";

			winnersGui[0].color=Color.white;
		}
		else
		{
//			GameObject player=GameObject.FindGameObjectWithTag("Player");
			for (int i=0;i<3;i++)
			{
				string guiName="Winner"+(i+1)+"Gui";
				winnersGui[i]=GameObject.FindGameObjectWithTag(guiName).GetComponent<Text>();
				if (top3Winners.Count()>i)
				{
					winners[i]=top3Winners.ElementAt(i).Value.ToString();
					winnersGui[i].enabled=true;
					winnersGui[i].text=winnerText[i]+winners[i]+" Bricks Destroyed";
					winnersGui[i].color=top3Winners.ElementAt(i).Key;
					//winnersGui[i].renderer.material.color=top3Winners.ElementAt(i).Key;

//					Vector3 pos =new Vector3(winnersGui[i].transform.position.x*Screen.currentResolution.width-420+(i*20),winnersGui[i].transform.position.y*Screen.currentResolution.height-10,winnersGui[i].transform.position.z);
//					GameObject playerAvatar = (GameObject.Instantiate(player, pos, Quaternion.identity) as GameObject);
//					playerAvatar.renderer.material.color=winnersGui[i].color;
				}
				switch(i)
				{
					case 0:
						winnersGui[i].fontSize = 140;
						break;
					case 1:
						winnersGui[i].fontSize = 120;
						break;
					case 2:
						winnersGui[i].fontSize = 100;
						break;
				}
			}
		}


	}
	//---------------------------------------------------------------------------------------------------------------------
		void showEndCredits()
		{
			GameObject credits=GameObject.FindGameObjectWithTag("EndCredits");
			credits.transform.position = new Vector3(UnityTuioManager.Instance.TargetScreenWidth/2f,UnityTuioManager.Instance.TargetScreenHeight/5f,-1);
		}
	//---------------------------------------------------------------------------------------------------------------------
	void updatePlayersInCenterList()
	{
		playersInCenter.Clear();
		foreach (ATuioPlayer player in m_playerList) 
		{
			playersInCenter.Add(player.gameObject);
			if (!isInCenter(player.transform.position))
			{
				if (playersInCenter.Contains(player.gameObject))
				{
					playersInCenter.Remove(player.gameObject);
				}
			}
		}
		//Debug.Log (playersInCenter.Count);
	}
	//---------------------------------------------------------------------------------------------------------------------
	public bool isInCenter(Vector2 position)
	{
		GameObject center=GameObject.FindGameObjectWithTag("BigCenter");
		float playerX=position.x;
		float playerY=position.y;
		float x=center.transform.position.x;
		float y=center.transform.position.y;
		float radius=center.transform.localScale.x/2;
		if (playerX>x-radius && playerX<x+radius && playerY>y-radius && playerY<y+radius)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	public override void UpdatePlayerPosition (long sessionID, Vector2 position)
	{
		bool playerExists = false;
		foreach (ATuioPlayer player in m_playerList) 
		{
			if(player.SessionID.Equals(sessionID))
			{
				playerExists = true;
				break;
			}
		}
		if(!playerExists)
		{
			AddPlayer(sessionID, position);
		}

		positionIndex[sessionID]++;
		foreach (ATuioPlayer player in m_playerList) 
		{
			if (player.gameObject.tag=="PlayerLongTail")
			{
				growTail(player,position);
				player.gameObject.tag="Player";
			}

			else if (player.gameObject.tag=="PlayerSticky")
			{
				//(player,position,currentGameManager);
				//stickBallToPlayer
				//player.gameObject.tag="Player";
				//Dictionary<ATuioPlayer,List<GameObject>> playerTails=tuioPlayerManagerScript.playerTails;
				List<GameObject> tailsList=playerTails[player];
				foreach(GameObject tail in tailsList)
				{
					tail.tag="PlayerSticky";
				}
			}

			if(player.SessionID.Equals(sessionID))
			{
				
				player.MoveTo(position);
				List<Vector3> thisPlayersPositions;
				lastPositions.TryGetValue(player,out thisPlayersPositions);
				if (thisPlayersPositions==null)
				{
					thisPlayersPositions=new List<Vector3>();
					lastPositions.Add(player,thisPlayersPositions);
				}
				Vector3 distance=new Vector3();
				if (thisPlayersPositions.Count>0)
				{
					Vector3 lastPos=thisPlayersPositions[thisPlayersPositions.Count-1];
					distance=lastPos-player.transform.position;
				}

				if (distance.magnitude>tailDistance || thisPlayersPositions.Count==0)
				{
						thisPlayersPositions.Add(player.transform.position);

					int thisPlayerTailCount=0;
					if (playerTails.ContainsKey(player))
					{
						thisPlayerTailCount=playerTails[player].Count;
					}
					if (thisPlayersPositions.Count>thisPlayerTailCount)
						thisPlayersPositions.RemoveAt(0);
					positionIndex[sessionID]=0;
				}

				List<GameObject> tailsList;
				playerTails.TryGetValue(player,out tailsList);
			}
		}


	}

	//---------------------------------------------------------------------------------------------------------------------
	public void makeTail(ATuioPlayer player)
	{
		//Debug.Log("here");
	}
	//---------------------------------------------------------------------------------------------------------------------
	public override void RemovePlayer (long sessionID)
	{
		foreach (ATuioPlayer player in m_playerList.ToArray()) 
		{
			if(player.SessionID.Equals(sessionID))
			{
				List<GameObject> tailsList;
				playerTails.TryGetValue(player,out tailsList);
				for (int i=0;i<tailsList.Count;i++)
					if (tailsList[i] != null)
						GameObject.Destroy(tailsList[i]);
				playerTails.Remove(player);
				lastPositions.Remove(player);
				GameObject.Destroy(player.gameObject);
				m_playerList.Remove(player);
			}	
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
}
}