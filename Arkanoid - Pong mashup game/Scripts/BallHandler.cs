﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Beelezeball{
public class BallHandler : MonoBehaviour 
{
	//---------------------------------------------------------------------------------------------------------------------
	private float stickyPowerupTimer=0;
	private float tailPowerupTimer=0;
	private GameObject stickyPlayersHead;
	private GameObject stickyPlayersTail;
	private GameObject stickyPlayer;
	private GameObject lastPlayersHead;
	private GameObject lastPlayersTail;
	private bool ballIsSticky;
	public Vector3 newDirection;
	public Dictionary<Color,int> top3Winners;

	public AudioSource audioSourceBrick;
	public AudioSource audioSourceBall;
	public AudioSource audioSourceOther;

	public AudioClip wallHit;
	public AudioClip playerHit;
	public AudioClip brickHit;
	public AudioClip ballMove;
	public AudioClip powerupTailAppear;
	public AudioClip powerupStickyAppear;
	public AudioClip ballStick;
	public AudioClip ballStickShoot;
	public AudioClip stickyArrow;

	float stickTime;
	float stickySoundTimer=0;
	float stickBlinkTimer;

	bool backgroundMusicIsPlaying;
	
	float m_ballWidth;
	//---------------------------------------------------------------------------------------------------------------------
	void Start () 
	{
		backgroundMusicIsPlaying=false;
		audioSourceOther.enabled=true;
		stickyPlayer=null;
		ballIsSticky=false;

		top3Winners=new Dictionary<Color,int>();
		audioSourceBall.clip=ballMove;
		audioSourceBall.loop=true;
		audioSourceBall.volume=.34f;

		GameObject ball=GameObject.FindGameObjectWithTag("Ball");
		m_ballWidth=ball.GetComponent<SpriteRenderer>().bounds.size.x;
		
	}
	//---------------------------------------------------------------------------------------------------------------------
	void Update()
	{
		try
		{
			if (ballIsSticky==false)
			{
				adjustBallPosition();
				playBallMoveSound();
			}
			else
			{
				//BALL IS STICKY!
				GameObject playerManager=GameObject.FindGameObjectWithTag("PlayerManager");
				BeelzeballPlayerManager scriptt=playerManager.gameObject.GetComponent<BeelzeballPlayerManager>();
				if (stickyPlayer!=null && scriptt.gameFinished==false)
				{
					moveBallWithStickyPlayer();
				}
				else
				{
					//THE GAME FINISHED OR A PLAYER DISAPPEARD WHILE THE BALL WAS STUCK TO HIM 
					//RELEASE THE BALL AND REMOVE THE ARROW
					newDirection=new Vector3(Random.Range (5,10),Random.Range (5,10),0);
					GameObject arrow=GameObject.FindGameObjectWithTag("Arrow");
					arrow.transform.position=new Vector3(-100,-100,0);
					playStickyBallReleaseSound();
					ballIsSticky=false;
					gameObject.GetComponent<Collider2D>().isTrigger=false;
				}
			}
			if(Input.GetKeyUp(KeyCode.E))
			{
				Global.incSpeed();
			}
			else if(Input.GetKeyUp(KeyCode.D))
			{
				Global.decSpeed();
			}
		}
		catch(UnityException uex)
		{
			Debug.Log (uex);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void FixedUpdate()
	{
		
		spawnPowerups();
	}
	//---------------------------------------------------------------------------------------------------------------------
	void spawnPowerups() 
	{
		if (newDirection.magnitude!=0)
		{
				if (Time.time-stickyPowerupTimer>Global.getStickySpawnTime())
				{
					createStickyPowerups();
					stickyPowerupTimer=Time.time;
				}
				if (Time.time-tailPowerupTimer>Global.getTailSpawnTime())
				{
					createTailPowerups();
					tailPowerupTimer=Time.time;
				}
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void moveBallWithStickyPlayer()
	{
		transform.position=new Vector3(stickyPlayer.transform.position.x,stickyPlayer.transform.position.y,transform.position.z);
		if (stickyPlayersHead!=null)
		{
			newDirection = (stickyPlayersHead.transform.position-gameObject.transform.position).normalized;
			newDirection.z=0;
			newDirection*=10;
			if (newDirection.magnitude<1)
			{
				newDirection=new Vector3(Random.Range (5,10),Random.Range (5,10),0);
			}
		}
		GameObject arrow=GameObject.FindGameObjectWithTag("Arrow");
		moveArrowWithStickyPlayer(arrow);
		
		if (Time.time-stickTime>Global.getStickDuration())
		{
			//SHOOT THE STICKY BALL
			releaseStickyBall();
			playStickyBallReleaseSound();
			arrow.transform.position=new Vector3(-100,-100,0);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void moveArrowWithStickyPlayer(GameObject arrow)
	{
		float angle;
		Vector3 cross;
		arrow.GetComponent<SpriteRenderer>().color=GetComponent<SpriteRenderer>().color;
		//BLINK THE ARROW
		if (Time.time-stickBlinkTimer>Global.getStickBlinkTime())
		{
			arrow.GetComponent<Renderer>().enabled=!arrow.GetComponent<Renderer>().enabled;
			stickBlinkTimer=Time.time;
		}
		if (Time.time-stickySoundTimer>.3f)
		{
			playArrowBlinkSound();
		}
		angle=Vector3.Angle(newDirection,new Vector3(100,0,0));
		//angle+=180;
		cross=Vector3.Cross(newDirection ,new Vector3(100,0,0));
		if (cross.z>0)
			angle=-angle;
		if (stickyPlayersHead!=null)
		{
			arrow.gameObject.transform.position=stickyPlayersHead.transform.position+(newDirection).normalized*130f;
			arrow.gameObject.transform.rotation=Quaternion.Euler(0,0,angle);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void releaseStickyBall()
	{
		audioSourceOther.volume=1;
		stickyPlayer.tag="Player";
		GameObject[] stickeyTails=GameObject.FindGameObjectsWithTag("PlayerSticky");
		
		//REMOVE STICKY TAILS
		foreach (GameObject tail in stickeyTails) 
		{
			if (tail.GetComponent<SpriteRenderer>().color==stickyPlayer.GetComponent<SpriteRenderer>().color)
				tail.tag="Player";	
		}
		
		//RELEASE THE BALL
		if (stickyPlayersHead!=null)
			gameObject.transform.position=stickyPlayersHead.transform.position;
		ballIsSticky=false;
		gameObject.GetComponent<Collider2D>().isTrigger=false;

	}
	//---------------------------------------------------------------------------------------------------------------------
	void adjustBallPosition()
	{
		float halfWidth=m_ballWidth/2;
		float screenWidth=Global.getScreenWidth();
		float screenHeight=Global.getScreenHeight();
		float x=transform.position.x;
		float y=transform.position.y;
		float z=transform.position.z;
		if (x-halfWidth<0 || x+halfWidth>screenWidth)
		{
			playBallHitWallSound();
			
			if (x-halfWidth<0)
			{
				transform.position=new Vector3(halfWidth,y,z);
			}
			else if (x+halfWidth>screenWidth)
			{
				transform.position=new Vector3(screenWidth-halfWidth,y,z);
			}
			newDirection.x=-newDirection.x;
		}
		if (y-halfWidth<0 || y+halfWidth>screenHeight)
		{
			playBallHitWallSound();
			
			if (y-halfWidth<0)
			{
				transform.position=new Vector3(x,halfWidth,z);
			}
			else if (y+halfWidth>screenHeight)
			{
				transform.position=new Vector3(x,screenHeight-halfWidth,z);
			}
			newDirection.y=-newDirection.y;
		}
		transform.position += newDirection.normalized * Global.getSpeed() * Time.deltaTime;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playArrowBlinkSound()
	{
		if (stickyPlayersHead!=null)
		{
			int screenWidth=Global.getScreenWidth();
			float panAmount=stickyPlayersHead.transform.position.x -(screenWidth/2);
			panAmount/=(screenWidth*1.05f/2);
			audioSourceOther.enabled=true;
			audioSourceOther.panStereo=panAmount;
			if (!audioSourceOther.isPlaying)
			{
				audioSourceOther.volume=.1f;//.001f;
				audioSourceOther.PlayOneShot(stickyArrow);
			}
			stickySoundTimer=Time.time;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playStickyBallReleaseSound()
	{
		audioSourceOther.enabled=true;
		if (ballIsSticky==true && !audioSourceOther.isPlaying)
			audioSourceOther.PlayOneShot(ballStickShoot);
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playBallMoveSound()
	{
		int screenWidth=Global.getScreenWidth();
		float panAmount;
		float ballX=transform.position.x;
		panAmount=ballX-(screenWidth/2);
		panAmount/=(screenWidth/2);
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playBallHitWallSound()
	{
		int screenWidth=Global.getScreenWidth();
		audioSourceOther.enabled=true;
		float panAmount;
		panAmount=transform.position.x -(screenWidth/2);
		panAmount/=(screenWidth*1.05f/2);
		audioSourceOther.panStereo=panAmount;
		audioSourceOther.PlayOneShot(wallHit);
	}
	//---------------------------------------------------------------------------------------------------------------------
	void playBallHitBrickSound(GameObject other)
	{
		int screenWidth=Global.getScreenWidth();
		float panAmount=other.transform.position.x -(screenWidth/2);
		panAmount/=(screenWidth*1.01f/2);
		audioSourceBrick.panStereo=panAmount;
		audioSourceBrick.PlayOneShot(brickHit);
		audioSourceBrick.pitch+=.01f;
	}

	//---------------------------------------------------------------------------------------------------------------------
	void createTailPowerups()
	{
		int screenWidth=Global.getScreenWidth();
		int screenHeight=Global.getScreenHeight();
		int x=Random.Range(100,screenWidth-100);
		int y=Random.Range(100,screenHeight-100);

		float panAmount=x -(screenWidth/2);
		panAmount/=(screenWidth*1.05f/2);
		audioSourceOther.enabled=true;
		audioSourceOther.panStereo=panAmount;
		audioSourceOther.PlayOneShot(powerupTailAppear);
		audioSourceOther.panStereo=0;

		GameObject powerupTail=GameObject.FindGameObjectWithTag("powerupTail");

		//IF THE GAME IS NOT FINISHED
		if (powerupTail!=null)
		{
			GameObject newPowerupTail=(GameObject) Instantiate(powerupTail,new Vector3(x,y,-100),Quaternion.identity);
			newPowerupTail.transform.rotation=powerupTail.transform.rotation;
			newPowerupTail.transform.localScale=powerupTail.transform.localScale;
			newPowerupTail.tag="newPowerupTail";
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void createStickyPowerups()
	{
		int screenWidth=Global.getScreenWidth();
		int screenHeight=Global.getScreenHeight();
		int x=Random.Range(100,screenWidth-100);
		int y=Random.Range(100,screenHeight-100);
		
		float panAmount=x -(screenWidth/2);
		panAmount/=(screenWidth*1.05f/2);
		audioSourceOther.enabled=true;
		audioSourceOther.panStereo=panAmount;
		audioSourceOther.PlayOneShot(powerupStickyAppear);
		audioSourceOther.panStereo=0;
		
		GameObject powerupSticky=GameObject.FindGameObjectWithTag("powerupSticky");
		
		//IF THE GAME IS NOT FINISHED
		if (powerupSticky!=null)
		{
			GameObject newPowerupSticky=(GameObject) Instantiate(powerupSticky,new Vector3(x,y,-100),Quaternion.identity);
			newPowerupSticky.transform.rotation=powerupSticky.transform.rotation;
			newPowerupSticky.transform.localScale=powerupSticky.transform.localScale;
			newPowerupSticky.tag="newPowerupSticky";
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void OnTriggerEnter2D(Collider2D theCollider)
	{
		if (theCollider.gameObject.tag=="Brick")
		{
			if (lastPlayersHead!=null && lastPlayersTail!=null)
			{
				createScoreUpText();
			}
			playBallHitBrickSound(theCollider.gameObject);
			colorBricksInScoreBar();
			
			if (GameObject.FindGameObjectsWithTag("Brick").Count()<=2)
			{
				finishGame();
			}
			Vector3 pos=theCollider.transform.position;
			newDirection = transform.position-pos;
			newDirection.z=0;
			GameObject.Destroy(theCollider.gameObject);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void OnCollisionEnter2D(Collision2D other)
	{
		if (ballIsSticky==false)
		{
			if (other.gameObject.tag=="Player" || other.gameObject.tag=="PlayerLongTail")
			{
				audioSourceOther.enabled=true;
				audioSourceOther.PlayOneShot(playerHit);
				Vector3 pos=other.transform.position;
				newDirection = transform.position-pos;
				newDirection.z=0;
				this.GetComponent<SpriteRenderer>().color=other.gameObject.GetComponent<SpriteRenderer>().color;
				if (backgroundMusicIsPlaying==false)
				{
						audioSourceBall.Play();
						backgroundMusicIsPlaying=true;
				}

				setLastPlayersHead();
				setLastPlayersTail();
			}
			else if (other.gameObject.tag=="PlayerSticky")
			{
				ballIsSticky=true;
					this.GetComponent<SpriteRenderer>().color=other.gameObject.GetComponent<SpriteRenderer>().color;

				setStickyPlayersHeadPosition();
				stickyPlayer=setStickyPlayersTailEndPosition(other.gameObject);
				stickTime=Time.time;
				gameObject.GetComponent<Collider2D>().isTrigger=true;
				newDirection.x=0;
				newDirection.y=0;
				audioSourceOther.PlayOneShot(ballStick);
				stickBlinkTimer=Time.time;
				stickySoundTimer=Time.time;
			}
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	void createScoreUpText()
	{
		GameObject GuiScoreUpText=GameObject.FindGameObjectWithTag("scoreUPGUIGameObjectMain");
		float angle=Vector3.Angle(lastPlayersTail.transform.position-lastPlayersHead.transform.position,new Vector3(100,0,0));
		Vector3 cross=Vector3.Cross(lastPlayersTail.transform.position-lastPlayersHead.transform.position ,new Vector3(100,0,0));
		if (cross.z>0)
			angle=-angle;
		angle+=90;
		Vector3 newPos=lastPlayersHead.transform.position+(lastPlayersHead.transform.position-lastPlayersTail.transform.position).normalized*120f;
		
		GameObject newGuiText=(GameObject) Instantiate(GuiScoreUpText,newPos,Quaternion.identity);
		newGuiText.tag="scoreUPGUIGameObject";
		newGuiText.GetComponent<SpriteRenderer>().color=lastPlayersHead.GetComponent<SpriteRenderer>().color;
		newGuiText.transform.parent=lastPlayersHead.transform;
		newGuiText.transform.rotation=Quaternion.Euler(0,0,angle);
	}
	//---------------------------------------------------------------------------------------------------------------------
	void finishGame()
	{
		GameObject playerManager=GameObject.FindGameObjectWithTag("PlayerManager");
		BeelzeballPlayerManager script=playerManager.gameObject.GetComponent<BeelzeballPlayerManager>();
		
		script.finishGame();
	}
	//---------------------------------------------------------------------------------------------------------------------
	void colorBricksInScoreBar()
	{
		Color ballColor=GetComponent<SpriteRenderer>().color;
		GameObject[] scoreBricks =GameObject.FindGameObjectsWithTag("ScoreBrick");
		if (scoreBricks.Length>0)
		{
			GameObject brick=scoreBricks[0];
			
			brick.GetComponent<SpriteRenderer>().color=new Color(ballColor.r,ballColor.g,ballColor.b,1);
			brick.tag="ScoreBrickColored";
			brick.GetComponent<Renderer>().enabled=true;
			brick.gameObject.name=((float)(ballColor.r)).ToString("F7")+" "+((float)(ballColor.g)).ToString("F7")+" "+((float)(ballColor.b)).ToString("F7");
			brick.transform.position=new Vector3(-1000,-1000,brick.transform.position.z);
			//SORT THE BRICKS AGAIN
			List<GameObject> sortedScoreBricks=sortScoreBricks(GameObject.FindGameObjectsWithTag("ScoreBrickColored"));
			reArrangeSortedBrickBar(sortedScoreBricks);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	GameObject setStickyPlayersTailEndPosition(GameObject other)
	{
		GameObject[] allStickyPlayers=GameObject.FindGameObjectsWithTag("PlayerSticky");
		float maxDistance=0;
		GameObject stickyPlayersTailEnd=other;
		if (stickyPlayersHead!=null)
		{
			foreach(GameObject player in allStickyPlayers)
			{
				if (player.GetComponent<SpriteRenderer>().color==other.GetComponent<SpriteRenderer>().color)
				{
					float distance=Vector3.Distance(player.transform.position,stickyPlayersHead.transform.position);
					if (distance>maxDistance)
					{
						maxDistance=distance;
						stickyPlayersTailEnd=player;
					}
				}
			}
		}
		return stickyPlayersTailEnd;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void setLastPlayersTail()
	{
		GameObject[] allPlayers=GameObject.FindGameObjectsWithTag("Player");
		float maxDistance=0;
		foreach(GameObject player in allPlayers)
		{
			if (player.GetComponent<SpriteRenderer>().color==gameObject.GetComponent<SpriteRenderer>().color)
			{
				float distance=Vector3.Distance(player.transform.position,lastPlayersHead.transform.position);
				if (distance>maxDistance)
				{
					maxDistance=distance;
					lastPlayersTail=player;
				}
			}
		}
		return;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void setLastPlayersHead()
	{
		GameObject[] allPlayers=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in allPlayers)
		{
			if (player.GetComponent<SpriteRenderer>().color==gameObject.GetComponent<SpriteRenderer>().color)
			{
				if (player.gameObject.transform.localScale.x>.9f)
				{
					lastPlayersHead=player;
					return;
				}
			}
		}
		return ;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void setStickyPlayersHeadPosition()
	{
		stickyPlayersHead=stickyPlayer;
		GameObject[] allStickyPlayers=GameObject.FindGameObjectsWithTag("PlayerSticky");
		foreach(GameObject player in allStickyPlayers)
		{
			if (player.GetComponent<SpriteRenderer>().color==gameObject.GetComponent<SpriteRenderer>().color)
			{
				if (player.gameObject.transform.localScale.x>.9f)
				{
					stickyPlayersHead=player;
					return;
				}
			}
		}
		return ;
	}
	//---------------------------------------------------------------------------------------------------------------------
	List<GameObject> sortScoreBricks(GameObject[] scoreBricks)
	{
		Dictionary<string, int> NameAndCount = new Dictionary<string, int>();
		foreach (GameObject brick in scoreBricks)
		{
			if (!NameAndCount.ContainsKey(brick.name))
			{
				NameAndCount.Add(brick.name, 1);
			}
			else
			{
				NameAndCount[brick.name]++;
			}
		}
		Dictionary<string, int> sortedNameCount= (from entry in NameAndCount orderby entry.Value descending select entry).ToDictionary(x => x.Key, x => x.Value);
		Dictionary<GameObject, int> sortedBrickCount= new Dictionary<GameObject,int>();
		List<GameObject> bricksSorted = new List<GameObject>();
		
		foreach (string name in sortedNameCount.Keys)
		{
			GameObject brick = null;
			foreach (GameObject b in scoreBricks)
			{
				if (b.name == name)
				{
					brick = b;
					break;
				}
			}
			sortedBrickCount.Add(brick, sortedNameCount[name]);
		}

		//HOLD THE TOP 3 WINNERS FOR THE ENDGAME
		for (int i=0;i<3;i++)
		{
			if (sortedBrickCount.Count()>i)
			{
				Color color=sortedBrickCount.ElementAt(i).Key.gameObject.GetComponent<SpriteRenderer>().color;
				int brickCount=sortedBrickCount.ElementAt(i).Value;
				if (!top3Winners.ContainsKey(color))
				{
					top3Winners.Add(color,brickCount);
				}
				else
				{
					top3Winners[color]=brickCount;
				}
			}
			top3Winners= (from entry in top3Winners orderby entry.Value descending select entry).ToDictionary(x => x.Key, x => x.Value);
		}

		foreach (GameObject brick in sortedBrickCount.Keys)
		{
			for (int i = 0; i < sortedBrickCount[brick]; i++)
			{
				bricksSorted.Add(brick);
			}
		}
		return bricksSorted;
	}
	//---------------------------------------------------------------------------------------------------------------------
	void reArrangeSortedBrickBar(List <GameObject> sortedBricks)
	{
		//StartGame startGameScri
		int numberOfBricks=StartGame.numberOfBricks;
//		GameObject firstBrick=sortedBricks.First();
		float brickWidth=20f;
		float brickHeight=40f;

		int screenWidth=Global.getScreenWidth();
		int screenHeight=Global.getScreenHeight();
		float brickDistance=2;
		float x=(screenWidth/2)-(brickWidth+brickDistance)*numberOfBricks/2;

		//DESTROY OLD OBJECTS
		GameObject[] oldSortedBricks=GameObject.FindGameObjectsWithTag("scoreBricksSorted");
		foreach(GameObject brick in oldSortedBricks)
			Destroy(brick);

		foreach (GameObject brick in sortedBricks) 
		{
			GameObject scoreBrick=(GameObject) Instantiate(brick,new Vector3(x,screenHeight-(brickHeight/.5f)-10,brick.transform.position.z),Quaternion.identity);
			scoreBrick.tag="scoreBricksSorted";
			x+=brickWidth+brickDistance;
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
}
}




























