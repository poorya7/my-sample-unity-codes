﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Beelezeball{
public class stickyPowerup : MonoBehaviour {

	private float creationTime;
	private float disapperTimer;

	//20
	private int lifeTime=20;
	private bool onDestroyQueue=false;

	public AudioSource audioSourceEat;
	public AudioSource audioSourceDisappear;
	//public AudioSource ballStick;
	
	public AudioClip eatSound;

	//------------------------------------------------------------------------------------------------------------
	void Start () 
	{
		creationTime=Time.time;
	}
	
	//------------------------------------------------------------------------------------------------------------
	void Update () 
	{
		if (this.tag=="newPowerupSticky")
			startDestroyTimer();
	}
	//------------------------------------------------------------------------------------------------------------
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag=="Player")
		{
			if (!audioSourceEat.isPlaying)
				audioSourceEat.PlayOneShot(eatSound);
			other.gameObject.tag="PlayerSticky";
			this.transform.position=new Vector3(-1000,-1000,this.transform.position.z);
			disapperTimer=Time.time;
			this.tag="onDestroyQueue";
			startDisappearTimer();
		}
	}
	//------------------------------------------------------------------------------------------------------------
	void startDisappearTimer()
	{
		if (Time.time-disapperTimer>5)
		{
			Destroy(this.gameObject);
		}
	}
	//------------------------------------------------------------------------------------------------------------

	void startDestroyTimer()
	{
		if (Time.time-creationTime>lifeTime && Time.time-creationTime<60 && onDestroyQueue==false)
		{
			this.transform.position=new Vector3(-1000,-1000,this.transform.position.z);
			if (!audioSourceDisappear.isPlaying)
				audioSourceDisappear.Play();
			onDestroyQueue=true;
		}
		else if (Time.time-creationTime>60)
		{
			Destroy(this.gameObject);
		}
	}
	//------------------------------------------------------------------------------------------------------------
}



}




