﻿using UnityEngine;
using System.Collections;
namespace Beelezeball{
public class fadeGuiScoreUpText : MonoBehaviour {

	// Use this for initialization
	float timer;
	float yIndex=0;
//	Color originalColor;
	float alpha=1;
	float speed=10;
	//---------------------------------------------------------------------------------------------------------------------
	void Start () 
	{
		timer=Time.time;
	}
	//---------------------------------------------------------------------------------------------------------------------
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (this.tag!="scoreUPGUIGameObjectMain")
		{

			GameObject playersHead=findParentsHead();
			GameObject playersEndTail=findParentsEndTail(playersHead);
			if (playersEndTail!=null && playersHead!=null)
			{
				float angle=Vector3.Angle(playersEndTail.transform.position-playersHead.transform.position,new Vector3(100,0,0));
				Vector3 cross=Vector3.Cross(playersEndTail.transform.position-playersHead.transform.position ,new Vector3(100,0,0));
				if (cross.z>0)
					angle=-angle;
				angle+=90;


				Vector3 newPos=playersHead.transform.position+(playersHead.transform.position-playersEndTail.transform.position).normalized*60;
				newPos.y+=yIndex;
				yIndex+=20;
				//transform.position=newPos;
				transform.rotation=Quaternion.Euler(0,0,angle);
				Vector3 direction=transform.TransformDirection(Vector3.up);
				transform.position+=direction*speed;
				speed-=.6f;
				if (speed<1)
					speed=1;
			}
//			originalColor=gameObject.renderer.material.color;
			Color c=gameObject.GetComponent<SpriteRenderer>().color;
			alpha-=.03f;
			c.a=alpha;
			gameObject.GetComponent<SpriteRenderer>().color=c;

			if (Time.time-timer>2)
				GameObject.Destroy(this.gameObject);
		}
	}
	//---------------------------------------------------------------------------------------------------------------------
	GameObject findParentsHead()
	{
		GameObject[] allPlayers=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject p in allPlayers)
		{
			float r1=p.GetComponent<SpriteRenderer>().color.r;
			float g1=p.GetComponent<SpriteRenderer>().color.g;
			float b1=p.GetComponent<SpriteRenderer>().color.b;
			float r2=gameObject.GetComponent<SpriteRenderer>().color.r;
			float g2=gameObject.GetComponent<SpriteRenderer>().color.g;
			float b2=gameObject.GetComponent<SpriteRenderer>().color.b;
			if (r1==r2 && g1==g2 && b1==b2)
			{
				if (p.transform.localScale.x>20)
				{
					return p;
				}
			}
		}
		return null;
	}
	//---------------------------------------------------------------------------------------------------------------------
	GameObject findParentsEndTail(GameObject playersHead)
	{
		GameObject[] allPlayers=GameObject.FindGameObjectsWithTag("Player");
		GameObject endTail=null;
		float maxDistance=0;
		foreach(GameObject p in allPlayers)
		{
			if (playersHead!=null)
			{
				if (p.GetComponent<SpriteRenderer>().color==playersHead.GetComponent<SpriteRenderer>().color)
				{
					float distance=Vector3.Distance(p.transform.position,playersHead.transform.position);
					if (distance>maxDistance)
					{
						maxDistance=distance;
						endTail=p;
					}
				}
			}
		}
		return endTail;
	}
	//---------------------------------------------------------------------------------------------------------------------
}
}