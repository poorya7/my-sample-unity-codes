﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//--------------------------------------------------------------------------------------------------------------------------------------------------
public static class Global 
{
	//private static readonly int m_screenWidth=1920;
	//private static readonly int m_screenHeight=1080;
	//private static readonly float m_screenWidth= TuioScreenManager.TargetResolutionWidth;
	//private static readonly float m_screenHeight=TuioScreenManager.TargetResolutionHeight;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static float m_ballSpeed=750;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static readonly float m_stickySpawnTime=15;
	private static readonly float m_tailSpawnTime=30;
	private static readonly float m_stickDuration=3;
	private static readonly float m_stickBlinkTime=.15f;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int getScreenWidth()  { return 3840; } //Screen.currentResolution.width; }
	public static int getScreenHeight() { return 2160; } //Screen.currentResolution.height; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float getSpeed() { return m_ballSpeed; }
	public static void incSpeed() { m_ballSpeed+=50;Debug.Log("speed: "+m_ballSpeed); }
	public static void decSpeed() { m_ballSpeed-=50;Debug.Log("speed: "+m_ballSpeed); }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float getStickySpawnTime() { return m_stickySpawnTime; }
	public static float getTailSpawnTime() { return m_tailSpawnTime; }
	public static float getStickDuration() { return m_stickDuration; }
	public static float getStickBlinkTime() { return m_stickBlinkTime; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


































































//--------------------------------------------------------------------------------------------------------------------------------------------------