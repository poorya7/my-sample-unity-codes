﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class playersLogic 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static PharusPlayerManager m_playerManagerScript;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static List<Dot> m_eatablePlayers=new List<Dot>();
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int getActivePlayersCount()
	{
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		int count=0;
		foreach(GameObject player in players)
		{
			if (player.GetComponent<playerHandler>().isHarmless()==false)
				count++;
		}
		return count;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void init(PharusPlayerManager playerManagerScript)
	{
		m_playerManagerScript = playerManagerScript;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static GameObject[] getPlayersList()
	{
		return GameObject.FindGameObjectsWithTag ("Player");
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int getPlayerCount()
	{
		return GameObject.FindGameObjectsWithTag("Player").Count();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playerInsideCenter(GameObject player)
	{
		return centerLogic.checkpointInsideCenter(player.transform.position);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static GameObject getClosestDangerousPlayer(Vector3 pos)
	{
		List<GameObject> playersByDistance=getPlayersByNormalDistance();
		foreach(GameObject player in playersByDistance)
		{
			if (!player.GetComponent<playerHandler>().isHarmless())// && playerIsInsideScreen(player.transform.position))
				return player;
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot getClosestEatablePlayer()
	{
		List<Dot> eatablePlayers=getEatablePlayersByDistance();
		foreach(Dot playerDot in eatablePlayers)
		{
			if (playerDot.safePathAvailable())
			{
				return playerDot;
			}
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<GameObject> getPlayersByNormalDistance()
	{
		Dictionary<GameObject, float> playersDic=new Dictionary<GameObject, float>();
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		foreach(GameObject player in players)
		{
			playersDic.Add(player, centerLogic.distanceNormal(player.transform.position, pacman));
		}
		var sortedPlayers = from entry in playersDic orderby entry.Value ascending select entry;
		List<GameObject> playersList=new List<GameObject>();
		for (int i=0; i<sortedPlayers.Count(); i++)
		{
			playersList.Add(sortedPlayers.ElementAt(i).Key);
		}
		return playersList;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<Dot> getEatablePlayersByDistance()
	{
		Dictionary<Dot, float> players=new Dictionary<Dot, float>();
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		foreach(Dot playerDot in m_eatablePlayers)
		{
			players.Add(playerDot, centerLogic.distanceNormal(playerDot.getPosition(), pacman));
		}
        var sortedPlayers = from entry in players orderby entry.Value ascending select entry;
		List<Dot> playersList=new List<Dot>();
		for (int i=0; i<sortedPlayers.Count(); i++)
		{
			playersList.Add(sortedPlayers.ElementAt(i).Key);
		}
		return playersList;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool anyPlayerEatable()
	{
		//if (centerLogic.checkpointInsideCenter(GameObject.FindGameObjectWithTag("PacMan").transform.position))
			//return false;
		updatePlayerPaths();
		foreach(Dot playerDot in m_eatablePlayers)
		{
			if (playerDot.safePathAvailable())
			{
				GameObject player=playerDot.getDot ();
				//Debug.Log(player.GetComponent<PacmanPlayer>().m_isEatenOnce);
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updatePlayerPaths()
	{
		foreach(Dot playerDot in m_eatablePlayers)
			GameObject.Destroy(playerDot.getDot());
		m_eatablePlayers.Clear();
		GameObject[] players=getPlayersList();
		foreach (GameObject player in players)
		{
			/*if (player.GetComponent<PacmanPlayer>().m_isEatenOnce)
				continue;*/
			if (playerIsInsideScreen(player.transform.position) && player.GetComponent<playerHandler>().isEatable())
			{
				Dot playerDot=new Dot(player.transform.position, true);
				playerDot.updateParameters();
				playerDot.setAllPaths();
				m_eatablePlayers.Add(playerDot);
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<Dot> getPlayerDots()
	{
		return m_eatablePlayers;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playerIsInsideScreen(Vector3 player)
	{
		float edgeMarginX = Global.EDGE_MARGIN_X;
		if (player.x>edgeMarginX && player.x<Global.getScreenWidth()-edgeMarginX && player.y>Global.EDGE_MARGIN_Y_BOTTOM && player.y<Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void colorPlayers()
	{
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in players)
		{
			player.GetComponent<playerHandler> ().assignColor (null);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void makePlayersBlue()
	{
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in players)
		{
			player.GetComponent<playerHandler> ().enterBlueMode ();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void resetPlayers()
	{
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in players)
		{
			player.GetComponent<playerHandler> ().assignColor (Color.white);
			player.GetComponent<playerHandler>().init();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void restorePlayersColor()
	{
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in players)
		{
			player.GetComponent<playerHandler> ().restoreOriginjalColor ();
			player.GetComponent<playerHandler>().init();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//------------------------THIS METHOD IS USED IN GETSAFETYINDEX AND CONSIDER ALL THE PLAYERS, EVEN IF THEY'RE IN THE CENTER----------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int getAllPlayersCount()
	{
		GameObject[] players=getPlayersList();
		int count=0;
		foreach (GameObject player in players)
		{
			if (player.transform.tag=="Player")
				count++;
		}
		return count;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}




















































































//--------------------------------------------------------------------------------------------------------------------------------------------------