using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using DG.Tweening;
//--------------------------------------------------------------------------------------------------------------------------------------------------
//TODO: 
//BUG FOUND: SOMETIMES PACMAN EATS ALL THE DOTS BUT THE GAME IS NOT OVER YET
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class PacManAI : MonoBehaviour 
{
	public GameObject			m_dot;
	public TextMesh				m_blueModeTitle;
	public GUIText				m_blueModeTimer;
	public GUIText				m_restartText;
	public PharusPlayerManager	m_playerManagerScript;
	public float				m_attackedCount;
	public float				m_dangerCount;
	public float				m_attackDetectionTime;
	public int 					m_lives=3;
	GameObject					m_currentTrackerParent;
	float						m_trackerCount=0;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	float 						m_trackerFactor;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Behavior 					eatDotBehavior;
	Behavior 					chaseBehavior;
	Behavior 					escapeBehavior;
	Behavior 					eatBlueDotBehavior;
	Behavior 					aiController;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Dot 					m_goal;
	public Vector3 				m_movementVector;
	int 						m_eatenGhostCount;
	int							m_allDotCount;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	float 						m_pathChangedTimer;
	float 						m_blueTimer;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//bool						m_firstTime=true;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public AudioSource 			AudioSourceMunch;
	public AudioSource 			AudioSourceMunchBlue;
	public AudioSource 			AudioSourceTheta;
	public AudioSource 			AudioSourceBackground;
	public AudioSource 			AudioSourcePolice;
	public AudioSource 			AudioSourceEatPlayer;
	float 						m_lastThetaPitch;
	bool 						m_backFromBlue;
	bool						m_initFinished=false;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Vector3 getMovementVector() { return m_movementVector; }
	public Vector3 getGoalPosition() 
	{	
		if (m_goal==null)
			return Vector3.zero;
		else if (m_goal.getDot()!=null)
			return m_goal.getDot().transform.position; 
		else
			return Vector3.zero;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void init()
	{
		m_initFinished = false;
		GetComponent<SpriteRenderer> ().enabled = false;
		countDown ();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void countDown()
	{
		DOTween.defaultEaseType = Ease.Linear;
		float n = Global.getCountDown () + 1;
		DOTween.To (() => n, x => n = x, 1, n - 1
		).OnComplete (
			() =>
			{
				StartCoroutine (begin ());
			}
		).OnUpdate (
			() => 
			{
				m_restartText.text=((int)n).ToString();
			}
		);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void InitGlobalsFromXml()
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load("Assets/pacman.xml");

		XmlNode node = xmlDoc.DocumentElement.SelectSingleNode("speed");
		float speed = float.Parse (node.InnerText);
		Global.setSpeed(speed);

		node = xmlDoc.DocumentElement.SelectSingleNode("blue_timer");
		float blueDuration = float.Parse (node.InnerText);

		Global.setBlueDuration(blueDuration);

		node = xmlDoc.DocumentElement.SelectSingleNode("count_down");
		byte countdownDuration = byte.Parse (node.InnerText);
		Global.setCountDownDuration(countdownDuration);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void Start()
	{
		//AudioSourceTheta.Play ();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	IEnumerator begin () 
	{
		DOTween.KillAll ();
		InitGlobalsFromXml ();
		Global.BLUE_MODE=false;
		Global.GAME_OVER=false;
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		AudioSourceMunch.pitch = 1;
		AudioSourceBackground.pitch = 0.8f;
		//AudioSourceTheta.pitch = 1;

		//AudioSourceTheta.volume = 1;
		m_backFromBlue = false;
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		m_pathChangedTimer=-999;
		m_blueTimer=-999;
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		m_movementVector = Vector3.zero;
		m_eatenGhostCount=0;
		m_restartText.text=" ";
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		centerLogic.init();
		yield return StartCoroutine (GetComponent<DotMaker> ().init (m_dot, gameObject));
		m_allDotCount = Dots.allDotCount ();
		playersLogic.init(m_playerManagerScript);
		ExitLogic.init();
		mapLogic.initInfluenceMap();
		mapLogic.updateMap();
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		//playersLogic.resetPlayers();
		//--------------------------------------------------------------------------------------------------------------------------------------------------
		eatDotBehavior=new EatDot();
		chaseBehavior=new Chase();
		escapeBehavior=new Escape();
		eatBlueDotBehavior=new EatBlueDot();
		aiController=new AIController();
		Global.setInitDotCount(GameObject.FindGameObjectsWithTag("Dot").Length);
		Global.setInitBlueDotCount(GameObject.FindGameObjectsWithTag("BlueDot").Length);
		m_trackerFactor=0;
		//enterBlueMode();
		m_currentTrackerParent=GameObject.Find("points");
		AdaptiveUnit.setStartTime(Time.time);
		Color c = m_blueModeTitle.color;
		m_blueModeTimer.text = "0.0";
		c.a=0.2f;
		m_blueModeTitle.color = c;
		m_blueModeTimer.color = c;
		setRandomStartupPos ();
		GetComponent<SpriteRenderer> ().enabled = true;
		yield return new WaitForSeconds (1);
		m_initFinished = true;
		AudioSourceBackground.Play();
		AudioSourceTheta.Stop ();
		//--------------------------------------------------------------------------------------------------------------------------------------------------
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setRandomStartupPos()
	{
		transform.position = mapLogic.getStartupPosition ();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void Update()
	{
		if (m_initFinished==false)
			return;
		
		updatePacman ();
		updatePlayerCount();
		updateBlueStatus ();
		mapLogic.updateMap();
		//updateDebugObjects();
		updateBullsEye ();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void FixedUpdate () 
	{

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updateBullsEye()
	{
		GameObject bullsEye=GameObject.FindGameObjectWithTag("bullsEye");
		Transform parent=bullsEye.transform.parent;
		if (parent==null || parent.gameObject.transform.tag!="Player" 
			|| parent.gameObject.GetComponent<playerHandler>().isEatable()==false
		)
		{
			bullsEye.transform.parent=null;
			bullsEye.transform.position=new Vector3(-1000,-1000,-1000);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void trackPacMan()
	{
		GameObject point=GameObject.FindGameObjectWithTag("point");
		/*string hex="00ff00";
		float r = float.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		float g = float.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		float b = float.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);*/
		Color c=new Color(0, 1, 0, 1);
		m_trackerFactor+=0.0003f;
		//c=ChangeColorBrightness(c);
		GameObject newPoint=(GameObject) Instantiate(point,transform.position,Quaternion.identity);
		newPoint.GetComponent<Renderer>().material.color=c;
		if (m_movementVector.magnitude > 0) 
		{
			float angle = Vector3.Angle (m_movementVector, Vector3.right);
			if (m_movementVector.y<0)
			{
				angle = -90;
			}
			newPoint.transform.rotation = Quaternion.Euler (0, 0, angle);
		}
		m_trackerCount++;
		if (m_trackerCount>40)
		{
			m_currentTrackerParent=new GameObject();
			m_trackerCount=0;
		}
		//else 
		//	currentTrackerParent=GameObject.Find("points");
		newPoint.transform.parent=m_currentTrackerParent.transform;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Color ChangeColorBrightness(Color color)
	{
		float factor=m_trackerFactor;
		float red = color.r;
		float green = color.g;
		float blue = color.b;
		red -=factor;
		green -= factor;
		blue -= factor;
		red=(red<0)?0:red;
		green=(green<0)?0:green;
		blue=(blue<0)?0:blue;
		color=new Color(red, green, blue, color.a);
		return color;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updatePlayerCount()
	{
		GameObject playerCount=GameObject.FindGameObjectWithTag("playerCount");
		string count=playersLogic.getAllPlayersCount().ToString();
		//count=AdaptiveUnit.m_totalAttackCount.ToString();
		playerCount.GetComponent<Renderer>().GetComponent<GUIText>().text=count;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updateBlueStatus()
	{
		if (Global.BLUE_MODE) 
		{
			float blueTimePassed = Time.time - m_blueTimer;
			float blueTimer = Global.getBlueDuration () - blueTimePassed;
			m_blueModeTimer.text = blueTimer.ToString ("F1");
			Color c = m_blueModeTitle.color;
			c.a=1;
			m_blueModeTitle.color = c;
			m_blueModeTimer.color = c;
			if (Time.time - m_blueTimer > Global.getBlueDuration ()) 
			{
				//END BLUE MODE
				endBlueMode();
			} 
			else if (!AudioSourcePolice.isPlaying) {
				AudioSourcePolice.volume = 0.8f;
				AudioSourcePolice.Play ();
			}
			AudioSourceTheta.pitch -= 0.2f;
			float finalPitch = Mathf.Abs (m_lastThetaPitch);
			if (AudioSourceTheta.pitch < -finalPitch) {
				AudioSourceTheta.pitch = -finalPitch;
			}
		} 
		else 
		{
			if (AudioSourcePolice.volume>0)
			{
				fadeOutPolice();
			}
			else
			{
				AudioSourcePolice.Stop();
			}
			if (m_backFromBlue == true) {
				AudioSourceTheta.pitch += 0.2f;
				
				if (AudioSourceTheta.pitch > m_lastThetaPitch) {
					AudioSourceTheta.pitch = m_lastThetaPitch;
					m_backFromBlue = false;
				}
			}
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updatePacman()
	{
		aiController.Tick();
		moveOneStep ();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public void handleGoal()
	{
		Debug.Log ("here");
		if (m_goal.isReal())
		{
			//handleRealDot(m_goal);
		}
		if (m_goal.isBlue())
		{
			AudioSourceMunchBlue.Play();
			enterBlueMode();
		}
		else
		{
			AudioSourceMunch.Play();
		}
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void handleCheckpoint()
	{
		if (m_goal.checkpointIsExit())
			transform.position=ExitLogic.getOppositeExit(m_goal.getCheckpoint());
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool goalNotAccessible()
	{
		return m_goal.currentPathUnsafe();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool checkpointNotAccessible()
	{
		return !SafetyLogic.checkpointValid(transform.position, m_goal.getCheckpoint(), m_goal.getPosition()) ;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updateDebugObjects()
	{
		//return;
		if (m_goal!=null)
			m_goal.updateDebugObjects ();
		if(Input.GetKeyUp(KeyCode.W))
		{
			Global.incSpeed();
		}
		else if(Input.GetKeyUp(KeyCode.S))
		{
			Global.decSpeed();
		}
		else if (Input.GetKeyUp(KeyCode.Q))
 		{
			Global.toggleDebugMode();
		}
		else if(Input.GetKeyUp(KeyCode.R))
		{
			Global.GAME_OVER=true;
			resetGame();
		}
		else if (Input.GetKeyUp(KeyCode.F))
		{
			Debugger.deleteTrackingObjects();
		}
		else if (Input.GetKeyUp(KeyCode.Space))
		{
			Time.timeScale=1;
		}
		else if (Input.GetKeyUp(KeyCode.H))
        {
			Time.timeScale=0.1f;
		}
		else if (Input.GetKeyUp(KeyCode.V))
		{
			Global.setSpeed(12);
		}
		else if (Input.GetKeyUp(KeyCode.M))
		{
			mapLogic.m_showMap=!mapLogic.m_showMap;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setGoalToClosestPlayer()
	{
		//PURSUIT
		Dot closestPlayer=playersLogic.getClosestEatablePlayer();//(transform.position);
		if (closestPlayer!=null)
		{
			if (true)//Time.time-m_pathChangedTimer>.1f)
			{
				closestPlayer.chooseSafePath();
				m_goal=closestPlayer;
				GameObject bullsEye=GameObject.FindGameObjectWithTag("bullsEye");
				bullsEye.transform.parent=closestPlayer.getDot().transform;
				Vector3 playerPos=closestPlayer.getPosition();
				bullsEye.transform.position=new Vector3(playerPos.x, playerPos.y+55, -1);
				m_pathChangedTimer=Time.time;
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool reachedPosition(Vector3 position)
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		Vector3 nextPosition=transform.position+m_movementVector*Global.getPacManSpeed()*Time.deltaTime*100;
		float xBefore=transform.position.x;
		float xAfter=nextPosition.x;
		float yBefore=transform.position.y;
		float yAfter=nextPosition.y;
		if (m_movementVector.x==1)
		{
			if (xBefore<=position.x && xAfter>=position.x && yBefore==position.y)
			{
				return true;
			}
		}
		else if (m_movementVector.x==-1)
		{
			if (xBefore>=position.x && xAfter<=position.x && yBefore==position.y)
			{
				return true;
			}
		}
		else if (m_movementVector.y==1)
		{
			if (yBefore<=position.y && yAfter>=position.y && xBefore==position.x)
			{
				return true;
			}
		}
		else if (m_movementVector.y==-1)
		{
			/*if (yBefore<position.y)
				return true;*/
			if (yBefore>=position.y && yAfter<=position.y && xBefore==position.x)
			{
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void moveOneStep()
	{
		if (m_goal!=null)
		{
			if (reachedPosition(m_goal.getCheckpoint()))
			{
				transform.position=m_goal.getCheckpoint();
			}
			else
				transform.position+=m_movementVector*Global.getPacManSpeed()*Time.deltaTime*100;

			Vector3 pos=transform.position;
			pos.z=-3;
			transform.position=pos;
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void resetGame()
	{
		m_initFinished = false;
		endBlueMode ();
		setRandomStartupPos();
		GameObject[] dots=GameObject.FindGameObjectsWithTag("Dot");
		foreach (GameObject dot in dots)
		{
				GameObject.Destroy(dot);
		}
		GameObject[] blueDots=GameObject.FindGameObjectsWithTag("BlueDot");
		foreach (GameObject dot in blueDots)
		{
			GameObject.Destroy(dot);
		}
		GameObject[] dots2=GameObject.FindGameObjectsWithTag("DotNearest");
		foreach (GameObject dot in dots2)
		{
				GameObject.Destroy(dot);
		}
		GameObject[] randomDots=GameObject.FindGameObjectsWithTag("RandomPoint");
		foreach (GameObject dot in randomDots)
		{
			GameObject.Destroy(dot);
		}
		//playersLogic.resetPlayers();
		resetEatenPlayers();
		playersLogic.resetPlayers ();
		Dots.clear();
		GameObject target = GameObject.Find ("onepacman");
		Color c = target.GetComponent<SpriteRenderer> ().color;
		c.a = 1;
		target.GetComponent<SpriteRenderer> ().color = c;
		GameObject.Find ("twopacman").GetComponent<SpriteRenderer> ().color = c;
		m_lives = 3;
		AdaptiveUnit.resetAttackCount();
		AdaptiveUnit.resetTotalAttackCount();
		AudioSourcePolice.Stop();
		AudioSourceBackground.Stop();
		GetComponent<SpriteRenderer> ().enabled = false;
		StartCoroutine (GetComponent<startup> ().reset ());
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void resetEatenPlayers()
	{
		GameObject[] eatenPlayers=GameObject.FindGameObjectsWithTag("EatenPlayer");
		foreach(GameObject eatenPlayer in eatenPlayers)
		{
			eatenPlayer.GetComponent<playerHandler>().restoreGhost();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void adjustRotation()
	{
		//return;
		if (m_movementVector.magnitude > 0) 
		{
			float angle = Vector3.Angle (m_movementVector, Vector3.right);
			if (m_movementVector.y<0)
			{
				angle = -90;
			}
			transform.rotation = Quaternion.Euler (0, 0, angle);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void resetMovementVector()
	{
		m_movementVector=Vector3.zero;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void setMovementVector()
	{
		m_movementVector=(m_goal.getCheckpoint()-transform.position).normalized;
		m_movementVector.x=Mathf.Round (m_movementVector.x);
		m_movementVector.y=Mathf.Round (m_movementVector.y);
		m_movementVector.z=0;//Mathf.Round (m_movementVector.z);
		if (m_movementVector.magnitude == 0)
			;//Debug.Log ("movement vector is zero !");
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void OnTriggerEnter(Collider other)
	{
		if (Global.GAME_OVER || m_initFinished==false)
			return;
		if(other.gameObject.tag=="Player")
		{
			if (Global.BLUE_MODE==true && other.GetComponent<playerHandler>().backFromGhost()==false)
			{
				//EAT PLAYER!
				if (GameObject.FindGameObjectsWithTag("EatenPlayer").Length==0)
					m_eatenGhostCount=0;
				m_eatenGhostCount++;//=GameObject.FindGameObjectsWithTag("EatenPlayer").Length;
				float width=GameObject.FindGameObjectWithTag("Center").transform.localScale.x;
				float height=GameObject.FindGameObjectWithTag("Center").transform.localScale.y;
				float centerX = Global.getScreenWidth () / 2 - width / 2 + 20 + (m_eatenGhostCount % 7) * 50 - 100;
				float centerY = Global.getScreenHeight () / 2 - height / 2 + 20 + (m_eatenGhostCount / 7) * 50 + 80;
				other.GetComponent<playerHandler>().makeCenterAvatar(new Vector3(centerX,centerY,-3));
				other.gameObject.tag="EatenPlayer";
				other.GetComponent<SpriteRenderer>().sprite= Resources.Load("eyes", typeof(Sprite)) as Sprite;
				AudioSourceEatPlayer.Play();
				//Instantiate(GameObject.FindGameObjectWithTag("newGoal"),other.gameObject.transform.position,Quaternion.identity);
			}
			else
			{
				setRandomStartupPos();
			}
		}
		else if (other.gameObject.tag=="exit")
		{
			Vector3 oppositeExit=ExitLogic.getOppositeExit(other.transform.position);
			float distIn=Vector3.Distance(m_goal.getPosition(),other.transform.position);
			float distOut=Vector3.Distance(m_goal.getPosition(),oppositeExit);
			if (distOut<distIn)
				transform.position=oppositeExit;
		}

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void OnTriggerExit(Collider other) 
	{
		if (Global.GAME_OVER || m_initFinished==false)
			return;
		if(other.gameObject.tag=="Player")
		{
			m_lives--;
			m_attackedCount++;
			GameObject target=null;
			if (m_lives==2)
				target=GameObject.Find("onepacman");
			else if (m_lives==1)
				target=GameObject.Find("twopacman");
			if (target!=null)
			{
				Color c=target.GetComponent<SpriteRenderer>().color;
				c.a=0.15f;
				target.GetComponent<SpriteRenderer>().color=c;
			}
			/*float survivedTime=Time.time-m_attackDetectionTime;
			KeyValuePair<string, string> survivedTimee = new KeyValuePair<string, string>("Survival time", survivedTime.ToString());
			KeyValuePair<string, string> dangerCount = new KeyValuePair<string, string>("danger count", (m_dangerCount).ToString());
			KeyValuePair<string, string> escapeCount = new KeyValuePair<string, string>("escape count", (m_dangerCount-m_attackedCount) .ToString());
			TrackingEvaluation.EvaluationManager.Instance.TriggerEvent("got attack.", survivedTimee, dangerCount, escapeCount);*/
			if (m_lives==0)
			{
				Global.GAME_OVER = true;
				resetGame();
				//init ();
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void enterBlueMode()
	{
		AdaptiveUnit.resetAttackCount();
		m_blueTimer = Time.time;
		Global.BLUE_MODE = true;
		playersLogic.makePlayersBlue();
		m_lastThetaPitch=AudioSourceTheta.pitch;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void endBlueMode()
	{
		Global.BLUE_MODE = false;
		playersLogic.restorePlayersColor();
		m_eatenGhostCount = 0;
		m_backFromBlue = true;
		AudioSourceTheta.pitch = -10;
		m_blueModeTimer.text = "0.0";
		Color c = m_blueModeTitle.color;
		c.a=0.2f;
		m_blueModeTitle.color = c;
		m_blueModeTimer.color = c;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void handleRealDot(Dot dot)
	{
		if (dot!=null)
		{
			handleRealDot(dot.getDot(), false);
			dot=null;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void handleRealDot(GameObject dot, bool blue)
	{
		if (dot!=null)
		{
			if (blue)
				AudioSourceMunchBlue.Play ();
			else
				AudioSourceMunch.Play ();

			Vector3 pos=dot.transform.position;
			pos.z=transform.position.z;
			dot.tag="DotEaten";
			Dots.removeDot(dot);
			GameObject.Destroy(dot);
			if (Dots.ateBatch ())
				AudioSourceBackground.pitch += (3f / m_allDotCount);
			if (Dots.ateAll ()) {
				Global.GAME_OVER = true;
				AdaptiveUnit.resetAttackCount ();
				AdaptiveUnit.resetTotalAttackCount ();
				resetGame ();
			} else if (blue)
				enterBlueMode ();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void fadeOutPolice()
	{
		AudioSourcePolice.volume -= Time.deltaTime;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool mostDotsEaten()
	{
		bool state;
		int initCount=Global.getInitDotCount();
		int currentCount=GameObject.FindGameObjectsWithTag("Dot").Length;
		if (currentCount<(initCount/4))
			state=true;
		else
			state=false;
		return state;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool manyBlueDotsLeft()
	{
		int initCount=Global.getInitBlueDotCount();
		int currentCount=GameObject.FindGameObjectsWithTag("BlueDot").Length;
		if (currentCount>(initCount*3/4))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool mostPlayersUnblue()
	{
		int normal=0;
		int blue=0;
		GameObject[] players=GameObject.FindGameObjectsWithTag("Player");
		foreach(GameObject player in players)
		{
			bool isEatable=player.GetComponent<playerHandler>().isEatable();
			if (isEatable)
				blue++;
			else
				normal++;
		}
		normal+=GameObject.FindGameObjectsWithTag("EatenPlayer").Length;
		if (blue<normal)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------------------------------------














