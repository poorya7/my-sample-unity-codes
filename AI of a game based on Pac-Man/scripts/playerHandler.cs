using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerHandler : TrackingEntity 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	GameObject currentTrackerParent;
	int trackerCount=0;
	List<float> m_speedArray=new List<float>();
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public float		m_averageSpeed;
	public float		m_speed;
	public bool			m_isInCenter;
	public bool			m_autoMove;
	public bool 		m_chase;
	public bool			m_lookAhead=false;
	public bool			m_isHrmLess;
	public bool			m_isEtable;
	public GameObject	m_centerAvatar;
	public int			m_movementType;
	float				m_trackSpeedTime;
	bool		 		m_isChasingPacman;
	bool 				m_backFromGhost=false;
	int 				m_direction;
	public Color		m_originalColor;
	Vector3 			m_movementVector;
	Vector3				m_startPosition;
	float				m_lastTimeSpeedChanged=-10;
	Vector3 			m_lastPos;
	float				m_lastPosTimer;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	float				m_currentAliveTimer;
	List<float>			m_aliveTimers;
	public float		m_averageAliveTime;
	float				m_rebornTime;
	public bool			m_isEatenOnce=false;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool backFromGhost() { return m_backFromGhost; }
	public bool isChasingPacMan() { return m_isChasingPacman; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void init()
	{
		m_backFromGhost = false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void Start()
	{
		m_trackSpeedTime=Time.time;
		m_averageSpeed=0;
		m_isChasingPacman=false;
		m_direction=1;
		gameObject.GetComponent<SpriteRenderer>().sprite= Resources.Load("ghost", typeof(Sprite)) as Sprite;
		if (Global.GAME_STARTED) 
		{
			assignColor ();
			if (Global.BLUE_MODE)
				enterBlueMode ();
		}
		m_startPosition=transform.position;
		init();
		m_lastPos=transform.position;
		m_lastPosTimer=Time.time;
		m_currentAliveTimer=Time.time;
		m_aliveTimers=new List<float>();
		m_rebornTime=Time.time;
		currentTrackerParent=GameObject.Find("points");
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void resetAverageAliveTime()
	{
		m_aliveTimers.Clear();
		m_rebornTime=Time.time;
		m_currentAliveTimer=Time.time;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public float getAverageAliveTime()
	{
		float sum=0;
		int count=0;

		foreach(float aliveTime in m_aliveTimers)
		{
			sum+=aliveTime;
		}
		if (m_aliveTimers.Count>0)
			count=m_aliveTimers.Count;
		if (tag=="Player")
		{
			sum+=m_currentAliveTimer;
			count++;
		}
		if (count>0)
			sum/=count;
		return sum;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void enterBlueMode()
	{
		assignColor (Global.BLUE_COLOR);
		m_backFromGhost = false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void assignColor(Color? color=null)
	{
		Color c;
		if (color == null) 
		{
			c = Global.getNextColor ();
			m_originalColor = c;
		}
		else
			c = color.Value;
		gameObject.GetComponent<SpriteRenderer> ().color = c;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void restoreOriginjalColor()
	{
		assignColor (m_originalColor);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void trackPlayer()
	{
		GameObject point=GameObject.FindGameObjectWithTag("point");
		
		/*string hex="00ff00";
		float r = float.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		float g = float.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		float b = float.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);*/
		Color c=new Color(1, 0, 0, 1);
		//m_trackerFactor+=0.0003f;
		//c=ChangeColorBrightness(c);
		GameObject newPoint=(GameObject) Instantiate(point,transform.position,Quaternion.identity);
		newPoint.GetComponent<Renderer>().material.color=c;
		if (m_movementVector.magnitude > 0) 
		{
			float angle = Vector3.Angle (m_movementVector, Vector3.right);
			if (m_movementVector.y<0)
			{
				angle = -90;
			}
			newPoint.transform.rotation = Quaternion.Euler (0, 0, angle);
		}
		trackerCount++;
		if (trackerCount>40)
		{
			currentTrackerParent=new GameObject();
			trackerCount=0;
		}
		newPoint.transform.parent=currentTrackerParent.transform;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void FixedUpdate()
	{
		if (Time.time-m_trackSpeedTime>0.5f)
		{
			m_trackSpeedTime=Time.time;
			//transform.Find("text").GetComponent<TextMesh>().text=Speed.ToString();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void Update()
	{
		updateChaseStatus();
		//trackPlayer();
		if (tag=="Player")
			m_currentAliveTimer=Time.time-m_rebornTime;
		m_averageAliveTime=getAverageAliveTime();
		if (TrackID==0)
		{
			Vector3 orientation=(transform.position-m_lastPos).normalized;
			Vector3 futurePos=transform.position+ new Vector3(orientation.x, orientation.y, 0)*300;
			//Debug.DrawLine(transform.position, futurePos);
			if (Time.time-m_lastPosTimer>0.5f)
			{
				m_lastPos=transform.position;
				m_lastPosTimer=Time.time;
			}
		}
		m_isEtable=isEatable();
		m_isHrmLess=isHarmless();
		if (m_autoMove==true)
		{
			if (transform.tag.Contains("Eaten"))
			{
				moveToCenter();
			}
			else if (m_chase)//transform.tag.Contains("Eaten"))
			{
				setTargetToPacman();
			}
			else
			{
				setTargetToTick();
				/*if (Global.BLUE_MODE==false)
				{
					setTargetToPacman();
				}
				else
				{
					setTargetToEscapePoint();
				}*/
			}
			moveOneStep();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void updateChaseStatus()
	{
		Vector3 orientation;
		if (TrackID==0)
			orientation=(transform.position-m_lastPos).normalized;
		else
			orientation=Orientation;
		int layerMask=1<<12;
		RaycastHit hit;
		if (Physics.Raycast(transform.position, orientation, out hit, Mathf.Infinity , layerMask))
		{
			if (hit.transform.tag=="PacMan")
			{
				//MOVING TO PACMAN
				m_isChasingPacman=true;
			}
			else
			{
				m_isChasingPacman=false;
			}
		}
		else
		{
			m_isChasingPacman=false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void moveOneStep()
	{
		if (Time.time-m_lastTimeSpeedChanged>2)
		{
			float pacmanSpeed = Global.getPacManSpeed ();
			float zeroCheck = (pacmanSpeed == 0) ? 10 : pacmanSpeed;
			//m_speed=Random.Range(pacmanSpeed-10,pacmanSpeed-5);
			//m_speed=pacmanSpeed-5;
			if (tag=="EatenPlayer")
				//m_speed=1;
			m_lastTimeSpeedChanged=Time.time;
		}
		if (Global.GAME_OVER==false)
		{
			transform.position += m_movementVector * m_speed * Time.deltaTime * 100;
		}
		else
		{
			transform.position=m_startPosition;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setTargetToTick()
	{
		Vector3 center=new Vector3(Global.getScreenWidth()/2, Global.getScreenHeight()/2, transform.position.z);
		if (Vector3.Distance(transform.position, center)>700 && !directionTowardCenter())
		{
			m_direction=-m_direction;
		}
		if (m_movementType==1)
			m_movementVector=new Vector3(m_direction,0,0);
		else if (m_movementType==2)
			m_movementVector=new Vector3(-m_direction,0,0);
		else if (m_movementType==3)
			m_movementVector=new Vector3(0,m_direction,0);
		else if (m_movementType==4)
			m_movementVector=new Vector3(0,-m_direction,0);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool directionTowardCenter()
	{
		float x=transform.position.x;
		float y=transform.position.y;
		Vector3 center=new Vector3(Global.getScreenWidth()/2, Global.getScreenHeight()/2, transform.position.z);
		if (x>center.x && m_movementVector.x<0)
			return true;
		else if (x<center.x && m_movementVector.x>0)
			return true;
		else if (y>center.y && m_movementVector.y<0)
			return true;
		else if (y<center.y && m_movementVector.y>0)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setTargetToPacman()
	{
		GameObject pacman=GameObject.FindGameObjectWithTag("PacMan");
		if (m_lookAhead==false)
			m_movementVector=(pacman.transform.position-transform.position).normalized;
		else
		{
			Vector3 pacmanVector=pacman.GetComponent<PacManAI>().getMovementVector();
			Vector3 pacmanFuturePos=pacman.transform.position+(pacmanVector*500);
			m_movementVector=(pacmanFuturePos-transform.position).normalized;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setTargetToEscapePoint()
	{
		m_movementVector=-(GameObject.FindGameObjectWithTag("PacMan").transform.position-transform.position).normalized;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void moveToCenter()
	{
		m_movementVector=(new Vector3(Global.getScreenWidth()/2, Global.getScreenHeight()/2, transform.position.z)-transform.position).normalized;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void restoreGhost()
	{
		gameObject.GetComponent<SpriteRenderer>().sprite= Resources.Load("ghost", typeof(Sprite)) as Sprite;
		assignColor(m_originalColor);
		gameObject.tag="Player";
		GameObject.Destroy(m_centerAvatar);
		m_backFromGhost=true;
		m_rebornTime=Time.time;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void makeCenterAvatar(Vector3 position)
	{
		m_centerAvatar= new GameObject();// (GameObject) GameObject.Instantiate(gameObject, position, Quaternion.identity);
		m_centerAvatar.transform.position=position;
		SpriteRenderer spriteRenderer = m_centerAvatar.AddComponent<SpriteRenderer>();
		m_centerAvatar.GetComponent<SpriteRenderer>().sprite= Resources.Load("ghost", typeof(Sprite)) as Sprite;
		m_centerAvatar.GetComponent<SpriteRenderer>().color=m_originalColor;
		m_centerAvatar.transform.localScale=transform.localScale/2;
		m_centerAvatar.transform.tag="EatenCenterAvatar";
		m_currentAliveTimer=Time.time-m_rebornTime;
		m_aliveTimers.Add(m_currentAliveTimer);
		m_isEatenOnce=true;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool isHarmless()
	{
		if (Global.BLUE_MODE && m_backFromGhost==false)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool isEatable()
	{
		if (Global.BLUE_MODE && gameObject.tag!="EatenPlayer" && m_backFromGhost==false && !centerLogic.checkpointInsideCenter(transform.position))
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.name.Contains ("centerCollider")) 
		{
			m_isInCenter = true;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void OnCollisionExit(Collision other)
	{
		if (other.gameObject.name.Contains ("centerCollider")) 
		{
			m_isInCenter = false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public bool isBlue()
	{
		Color blueColor=new Color(41/255f,44/255f,250/255f);
		
		if (gameObject.GetComponent<SpriteRenderer>().color==blueColor)
			return true;
		else
			return false;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------

}
