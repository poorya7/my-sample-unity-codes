﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityPharus;
using UnityTracking;


public class PacmanPlayerManager : PharusPlayerManager
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public override void AddPlayer (PharusTransmission.TrackRecord theTrackRecord)
	{
		//base.AddPlayer (theTrackRecord);
		Vector2 position = UnityPharusManager.GetScreenPositionFromRelativePosition(theTrackRecord.relPos)*2;
		TrackingEntity aPlayer = (GameObject.Instantiate(_playerPrefab, new Vector3(position.x,position.y,0), Quaternion.identity) as GameObject).GetComponent<TrackingEntity>();
		aPlayer.TrackID = theTrackRecord.trackID;
		aPlayer.AbsolutePosition = new Vector2(theTrackRecord.currentPos.x,theTrackRecord.currentPos.y);
		aPlayer.NextExpectedAbsolutePosition = new Vector2(theTrackRecord.expectPos.x,theTrackRecord.expectPos.y);
		aPlayer.RelativePosition = new Vector2(theTrackRecord.relPos.x,theTrackRecord.relPos.y);
		aPlayer.Orientation = new Vector2(theTrackRecord.orientation.x,theTrackRecord.orientation.y);
		aPlayer.Speed = theTrackRecord.speed;
		aPlayer.gameObject.name = string.Format("PharusPlayer_{0}", aPlayer.TrackID);
		_playerList.Add(aPlayer);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public override void UpdatePlayerPosition (PharusTransmission.TrackRecord trackRecord)
	{
		foreach (TrackingEntity aPlayer in _playerList)
		{
			if (aPlayer.TrackID == trackRecord.trackID)
			{
				aPlayer.AbsolutePosition = new Vector2 (trackRecord.currentPos.x - gridOffset.x, trackRecord.currentPos.y - gridOffset.y);
				aPlayer.NextExpectedAbsolutePosition = new Vector2(trackRecord.expectPos.x - gridOffset.x, trackRecord.expectPos.y - gridOffset.y);
				aPlayer.RelativePosition = new Vector2 (trackRecord.relPos.x, trackRecord.relPos.y);
				aPlayer.Orientation = new Vector2(trackRecord.orientation.x, trackRecord.orientation.y);
				aPlayer.Speed = trackRecord.speed;
				// use AddToVector2List() instead of ToVector2List() as it is more performant
				aPlayer.Echoes.Clear();
				trackRecord.echoes.AddToVector2List(aPlayer.Echoes);
				//aPlayer.SetPosition(new Vector3(aPlayer.AbsolutePosition.x, 0f, aPlayer.AbsolutePosition.y));
				aPlayer.SetPosition(TrackingAdapter.GetScreenPositionFromRelativePosition(trackRecord.relPos.x, trackRecord.relPos.y)*2);
				return;
			}
		}
		if (_addUnknownPlayerOnUpdate)
		{
			AddPlayer(trackRecord);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public override void RemovePlayer (int trackID)
	{
		//base.RemovePlayer (trackID);

		foreach (TrackingEntity player in _playerList.ToArray()) 
		{
			if(player.TrackID.Equals(trackID))
			{
				GameObject.Destroy(player.GetComponent<playerHandler>().m_centerAvatar);
				GameObject.Destroy(player.gameObject);
				_playerList.Remove(player);
				// return here in case you are really really sure the trackID is in our list only once!
				//				return;
			}	
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*Color getNewColor()
	{
		Color[] colors=new Color[20];
		for (int i=0;i<20;i++)
		{
			float r=(float)(Random.Range(0,255))/255;
			float g=(float)(Random.Range(0,255))/255;
			float b=(float)(Random.Range(0,255))/255;
			colors[i]=new Color(r,g,b);
			
		}
		int j=Random.Range(0,colors.Length);
		return colors[j];
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------

}
