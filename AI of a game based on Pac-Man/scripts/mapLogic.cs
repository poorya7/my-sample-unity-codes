using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class mapLogic 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//static string [,] m_influenceMap=new string[200,200];
	static Dictionary<Vector3, float> m_map=new Dictionary<Vector3, float>();
	public static bool m_showMap=false;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void initInfluenceMap()
	{
		float marginX=Global.EDGE_MARGIN_X;
		float space=260;
		int i=0;
		int j=0;
		for (float x=marginX;x<Global.getScreenWidth()-marginX;x+=space)
		{
			j=0;
			i++;
			for (float y=space+Global.EDGE_MARGIN_Y_BOTTOM;y<Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP;y+=space)
			{
				j++;
				Vector3 pos=new Vector3(x, y, -3);
				if (centerLogic.checkpointInsideCenter(pos))
				    continue;
				float value=SafetyLogic.getSafetyIndex(pos);
				m_map[pos]=value;
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updateMap()
	{
		GameObject[] tests=GameObject.FindGameObjectsWithTag("goal");
		foreach(GameObject goal in tests)
			GameObject.Destroy(goal);
		for (int i=0; i<m_map.Count; i++)
		{
			Vector3 pos=m_map.ElementAt(i).Key;
			float value=SafetyLogic.getSafetyIndex(m_map.ElementAt(i).Key);
			m_map[pos]=value;
			if (m_showMap==true)
			{
				GameObject test=(GameObject) GameObject.Instantiate(GameObject.FindGameObjectWithTag("testText"),pos,Quaternion.identity);
				test.transform.tag="goal";
				test.GetComponent<TextMesh>().text=Mathf.Round(value).ToString();
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static List<Vector3> getSafePositionsFromMap(Vector3 pacmanPos)
	{
		//return m_map.Keys.ToList();
		var sortedPos = from entry in m_map orderby entry.Value ascending select entry;
		List<Vector3> positions=new List<Vector3>();
		for (int i=0; i<sortedPos.Count(); i++)
		{
			positions.Add(sortedPos.ElementAt(i).Key);
		}

		return positions;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getStartupPosition()
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		List<Vector3> sortedSafePositions = getSafePositionsFromMap (pacman);
		int randIndex = Random.Range (0, sortedSafePositions.Count);
		return sortedSafePositions.ElementAt (randIndex);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot findEscapePoint(bool escaping=true, float margin=0)
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		Vector3 safePosition=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		List<Vector3> sortedSafePositions=getSafePositionsFromMap(pacman);
		Dot dot=getEscapePointEx(sortedSafePositions, pacman, MOVEMENT_TYPE.NONE, escaping, margin);
		if (dot==null)
		{
			dot=getEscapePointEx(sortedSafePositions, pacman, MOVEMENT_TYPE.NORMAL, escaping, margin);
		}
		return dot;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static Dot getEscapePointEx(List<Vector3> sortedDots, Vector3 pacman, MOVEMENT_TYPE movementType, bool escaping=true, float margin=0)
	{
		for (int i=0; i<sortedDots.Count; i++)
		{
			Vector3 pos=sortedDots[i];
			if (!centerLogic.checkpointInsideCenter(pos))
			{
				Dot dot=new Dot(pos);
				dot.updateParameters();
				dot.setAllPaths();
				bool excludeCond;
				if (movementType==MOVEMENT_TYPE.NORMAL)
					excludeCond=dot.getSelectedMovementType()!=MOVEMENT_TYPE.NORMAL;
				else
					excludeCond=dot.getSelectedMovementType()==MOVEMENT_TYPE.NORMAL;
				if (escaping==false)
					excludeCond=false;
				else if (excludeCond)
					continue;
				if (!isInScreenEnough(pos, margin))
					continue;
				if (dot.chooseSafePath(true))
				{
					float dist=Vector3.Distance(dot.getCheckpoint(),pacman);
					if (true)//dist>=Global.getMinCheckDist())
					{
						return dot;
					}
				}
				else
				{
					if (movementType==MOVEMENT_TYPE.NONE)
						;//Debug.Log(dot.getSelectedMovementType());
				}
			}
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool isInScreenEnough(Vector3 position, float margin=0)
	{
		bool cond1=position.x>=margin && position.x<=Global.getScreenWidth()-margin;
		bool cond2=position.y>=margin && position.y<=Global.getScreenHeight()-margin;
		if (cond1 && cond2)
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot getRandomPoint()
	{
		List<Vector3> safePositions=new List<Vector3>();
		int playerCount=playersLogic.getPlayerCount();
		foreach(KeyValuePair<Vector3, float> entry in m_map)
		{
			float index=Global.HIGHEST_SAFE_VAL_CHECKPOINT/playerCount;
			if (entry.Value<=index)
				safePositions.Add(entry.Key);
		}
		Dot dot=getRandomPointRegular(safePositions);
		if (dot==null)
			dot=moveToSaferPosition(safePositions);
		return dot;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot getRandomPointRegular(List<Vector3> safePositions)
	{
		List<Vector3> tempList=new List<Vector3>(safePositions);
		while(tempList.Count>0)
		{
			int i=Random.Range(0,tempList.Count);
			Vector3 position=tempList.ElementAt(i);
			Dot dot=new Dot(position);
			dot.updateParameters();
			dot.setAllPaths();
			if (false)//dot.getSelectedMovementType()==MOVEMENT_TYPE.NORMAL)
				continue;
			if (dot.chooseSafePath(true))
			{
				return dot;
			}
			tempList.Remove(position);
			dot.Dispose();
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot moveToSaferPosition(List<Vector3> safePositions)
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		Vector3 player=playersLogic.getClosestDangerousPlayer(pacman).transform.position;
		float xDiff=pacman.x-player.x;
		float yDiff=pacman.y-player.y;

		Vector3 position;
		if (Mathf.Abs(xDiff)<Mathf.Abs(yDiff))
		{
			//ESCAPE VERTICALLY
			float newY=pacman.y+200*Mathf.Sign(yDiff);
			position=new Vector3(pacman.x, newY, pacman.z);
		}
		else
		{
			//ESCAPE HORIZONTALLY
			float newX=pacman.x+200*Mathf.Sign(xDiff);
			position=new Vector3(newX, pacman.y, pacman.z);
		}
		if (CheckpointLogic.checkpointInScreen(position) && !centerLogic.checkpointInsideCenter(position))
		{
			Dot dot=new Dot(position);
			dot.updateParameters();
			dot.setAllPaths();
			dot.chooseSafePath();
			return dot;
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}



































































//--------------------------------------------------------------------------------------------------------------------------------------------------