using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//--------------------------------------------------------------------------------------------------------------------------------------------------
public static class ExitLogic
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static List<Vector3> m_verticalExits=new List<Vector3>();
	static List<Vector3> m_allExits=new List<Vector3>();
	static List<Vector3> m_horizontalExits=new List<Vector3>();
	static Vector3 m_exitLeft;
	static Vector3 m_exitRight;
	static Vector3 m_exitUpLeft;
	static Vector3 m_exitUpRight;
	static Vector3 m_exitDownLeft;
	static Vector3 m_exitDownRight;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void init()
	{
		m_exitLeft=GameObject.Find ("exitLeft").transform.position;
		m_exitRight=GameObject.Find ("exitRight").transform.position;
		m_exitUpLeft=GameObject.Find ("exitUpLeft").transform.position;
		m_exitUpRight=GameObject.Find ("exitUpRight").transform.position;
		m_exitDownLeft=GameObject.Find ("exitDownLeft").transform.position;
		m_exitDownRight=GameObject.Find ("exitDownRight").transform.position;

		m_verticalExits.Add(m_exitUpLeft);
		m_verticalExits.Add(m_exitUpRight);
		m_verticalExits.Add(m_exitDownLeft);
		m_verticalExits.Add(m_exitDownRight);

		m_horizontalExits.Add(m_exitLeft);
		m_horizontalExits.Add (m_exitRight);

		m_allExits.Add(m_exitLeft);
		m_allExits.Add(m_exitRight);
		m_allExits.Add(m_exitUpLeft);
		m_allExits.Add(m_exitUpRight);
		m_allExits.Add(m_exitDownLeft);
		m_allExits.Add(m_exitDownRight);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getOppositeExit(Vector3 exitIn)
	{
		if (exitIn==m_exitLeft)
			return m_exitRight;
		else if (exitIn==m_exitRight)
			return m_exitLeft;
		else if (exitIn==m_exitDownLeft)
			return m_exitUpLeft;
		else if (exitIn==m_exitDownRight)
			return m_exitUpRight;
		else if (exitIn==m_exitUpLeft)
			return m_exitDownLeft;
		else
			return m_exitDownRight;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getDotCheckpointOnOppositeSide(Vector3 exitOut, Vector3 dot)
	{
		if (m_horizontalExits.Contains(exitOut))
			return new Vector3(dot.x, exitOut.y, exitOut.z);
		else
			return new Vector3(exitOut.x, dot.y, exitOut.z);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getExitOutCheckpoint(Vector3 exitOut)
	{
		float distanceToMoveIn=70;
		if (exitOut==m_exitLeft)
			return new Vector3(distanceToMoveIn, exitOut.y, exitOut.z);
		else if (exitOut==m_exitRight)
			return new Vector3(exitOut.x-distanceToMoveIn, exitOut.y, exitOut.z);
		else if (exitOut==m_exitDownLeft)
			return new Vector3(exitOut.x, exitOut.y+distanceToMoveIn, exitOut.z);
		else if (exitOut==m_exitDownRight)
			return new Vector3(exitOut.x, exitOut.y+distanceToMoveIn, exitOut.z);
		else if (exitOut==m_exitUpLeft)
			return new Vector3(exitOut.x, exitOut.y-distanceToMoveIn, exitOut.z);
		else// if (exitIn==m_exitUpRight)
			return new Vector3(exitOut.x, exitOut.y-distanceToMoveIn, exitOut.z);
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getExitIn(Vector3 pacman, MOVEMENT_TYPE movementType)
	{
		if (movementType==MOVEMENT_TYPE.WARP_H)
		{
			//HORIZONTAL
			if (pacman.x<Global.getScreenWidth()/2)
				return m_exitLeft;
			else
				return m_exitRight;
		}
		else
		{
			//VERTICAL
			if (pacman.x<Global.getScreenWidth()/2)
			{
				//LEFT SIDE
				if (pacman.y<Global.getScreenHeight()/2)
					return m_exitDownLeft;
				else
					return m_exitUpLeft;
			}
			else
			{
				//RIGHT SIDE
				if (pacman.y<Global.getScreenHeight()/2)
					return m_exitDownRight;
				else
					return m_exitUpRight;
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool align(Vector3 pacman, Vector3 exitIn)
	{
		if (m_horizontalExits.Contains(exitIn))
		{
			return pacman.y==exitIn.y;
		}
		else
		{
			return pacman.x==exitIn.x;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getExitInCheckpoint(Vector3 pacman, Vector3 exitIn)
	{
		if (m_horizontalExits.Contains(exitIn))
	    {
			if (Mathf.Abs(pacman.y-exitIn.y)<5)
				return exitIn;
			else
				return new Vector3(pacman.x, exitIn.y, exitIn.z);
		}
		else
		{
			if (Mathf.Abs(pacman.x-exitIn.x)<5)
				return exitIn;
			else
				return new Vector3(exitIn.x, pacman.y, pacman.z);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getCheckpointIn(Vector3 pacman, MOVEMENT_TYPE movementType)
	{
		if (movementType==MOVEMENT_TYPE.WARP_H)
			return getHorizontalCheckpoint(pacman);
		else
			return getVerticalCheckpoint(pacman);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getHorizontalCheckpoint(Vector3 pacman)
	{
		if (pacman.x<Global.getScreenWidth()/2)
		{
			if (pacman.y==m_exitLeft.y)
				return m_exitLeft;
			else
				return new Vector3(pacman.x, m_exitLeft.y, pacman.z);
		}
		else
		{
			if (pacman.y==m_exitRight.y)
				return m_exitRight;
			else
				return new Vector3(pacman.x, m_exitRight.y, pacman.z);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static Vector3 getVerticalCheckpoint(Vector3 pacman)
	{
		Vector3 exit=getClosestVerticalExit(pacman);
		if (pacman.x==exit.x)
			return exit;
		else
			return new Vector3(exit.x, pacman.y, pacman.z);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool checkpointOnExit(Vector3 checkpoint)
	{
		foreach(Vector3 exit in m_horizontalExits)
		{
			if (Mathf.Abs(checkpoint.y-exit.y)<10)
				if (checkpoint.x<=0 || checkpoint.x>=Global.getScreenWidth())
					return true;
		}
		foreach(Vector3 exit in m_verticalExits)
		{
			if (Mathf.Abs(checkpoint.x-exit.x)<10)
			{
				if (checkpoint.y<=0 || checkpoint.y>=Global.getScreenHeight())
					return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static Vector3 getClosestVerticalExit(Vector3 pacman)
	{
		float minimum=100000;
		Vector3 closestExit=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		foreach(Vector3 exit in m_verticalExits)
		{
			float distance=Vector3.Distance(pacman, exit);
			if (distance<minimum)
			{
				minimum=distance;
				closestExit=exit;
			}
		}
		return closestExit;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getClosestExit(Vector3 pacman)
	{
		float minimum=100000;
		Vector3 closestExit=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		foreach(Vector3 exit in m_allExits)
		{
			float distance=Vector3.Distance(pacman, exit);
			if (distance<minimum)
			{
				minimum=distance;
				closestExit=exit;
			}
		}
		return closestExit;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool nodeIsExit(Vector3 node)
	{
		if (m_allExits.Contains(node))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public static bool enteredExaitGate(Vector3 pacman)
	{
		Vector3 exitUpLeft=GameObject.Find("exitUpLeft").transform.position;
		Vector3 exitUpRight=GameObject.Find("exitUpRight").transform.position;
		Vector3 exitLeft=GameObject.Find("exitLeft").transform.position;
		bool horizontalExit=((pacman.x >= Global.getScreenWidth() || pacman.x <= 0 ) && (pacman.y==exitLeft.y));
		bool verticalExit=(pacman.y >= Global.getScreenHeight() || pacman.y <= 0) && (pacman.x==exitUpLeft.x || pacman.x==exitUpRight.x);
		if (horizontalExit || verticalExit)
			return true;
		else
			return false;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}




















































































//--------------------------------------------------------------------------------------------------------------------------------------------------