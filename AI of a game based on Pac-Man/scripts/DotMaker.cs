using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class DotMaker : MonoBehaviour
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static GameObject			m_dot;
	static GameObject 			m_pacman;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public IEnumerator init (GameObject dot, GameObject pacman) 
	{
		m_dot = dot;
		m_pacman = pacman;
		int[,] dotMatrix = initDotMatrix ();
		yield return StartCoroutine (createDots (dotMatrix));
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	int[,] initDotMatrix()
	{
		int[,] dotMatrix= new int[,] {
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1},
			{ 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
			{ 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 1, 0, 0, 0, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
			{ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
			{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
		};

		/*dotMatrix= new int[,] {
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}

		};*/

		return dotMatrix;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	IEnumerator createDots(int[,] dotMatrix)
	{
		//return ;
		Dots.init(m_pacman);
		float hSpace=110;//34
		float vSpace=110;//34
		float size=m_dot.transform.localScale.x;
		int counter = 0;
		for (int i=0;i<dotMatrix.GetLength(0);i++)
		{
			for (int j=0;j<dotMatrix.GetLength(1);j++)
			{
				if (dotMatrix[i,j]==1 || dotMatrix[i,j]==2)
				{
					float x=Global.EDGE_MARGIN_X+(hSpace+size)*j;
					float y=Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP-(vSpace+size)*i;
					if (x>0 && x<Global.getScreenWidth() && y>0 && y<Global.getScreenHeight())
					{
						Vector3 pos=new Vector3(x,y,m_dot.transform.position.z);
						if (centerLogic.checkpointInsideCenter(pos) || dotOutsideScreen(pos))
							continue;
						GameObject newDot = GameObject.Instantiate (m_dot, pos, Quaternion.identity);
						newDot.transform.parent = m_dot.transform.parent;
						newDot.tag="Dot";
						newDot.GetComponent<Collider>().enabled=true;
						newDot.GetComponent<Renderer>().enabled=true;
						Dot d;
						if (dotMatrix[i,j]==1)
						{
							//NORMAL DOT
							Color yellowColor=new Color(254/255f,203/255f,0/255f);
							newDot.GetComponent<SpriteRenderer>().color=yellowColor;
							d=new Dot(newDot,m_pacman, false);
						}
						else 
						{
							//BLUE DOT
							newDot.transform.localScale*=3;	
							Color blueColor=new Color(41/255f,44/255f,250/255f);
							newDot.GetComponent<SpriteRenderer>().color=blueColor;
							newDot.transform.tag="BlueDot";
							d=new Dot(newDot,m_pacman, true);
						}
						Dots.addDot(d);
						counter++;
						if (counter%3==0)
							yield return null;
					}
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool dotOutsideScreen(Vector3 dot)
	{
		float marginX=Global.EDGE_MARGIN_X;
		if (dot.x<marginX || dot.x>Global.getScreenWidth()-marginX || dot.y<Global.EDGE_MARGIN_Y_BOTTOM || dot.y>Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}




















































//--------------------------------------------------------------------------------------------------------------------------------------------------
