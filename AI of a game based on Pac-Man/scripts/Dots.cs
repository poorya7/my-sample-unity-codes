﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Dots 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static List<Dot> m_dots=new List<Dot>();
	static GameObject 		 m_pacman;
	static int 				 m_dotCount;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void init(GameObject pacman)
	{
		m_pacman = pacman;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void clear()
	{
		m_dots.Clear();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public static List<Dot> getDots()
	{
		return m_dots;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int allDotCount() { return m_dotCount; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool ateAll()
	{
		Debugger.updateText(m_dots.Count().ToString());
		return m_dots.Count()==0;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void addDot(Dot dot)
	{
		if (dot.getPosition().x<1)
			Debug.Log("added zero");
		m_dots.Add(dot);
		m_dotCount++;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void removeDot(GameObject dotObject)
	{
		foreach(Dot dot in m_dots)
		{
			if (dot.getDot()==dotObject)
			{
				m_dots.Remove(dot);
				return;
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool ateBatch()
	{
		if (m_dotCount - m_dots.Count >= 10) 
		{
			m_dotCount = m_dots.Count;
			return true;
		} 
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<Dot> getSortedDots(bool blue, bool cornerDot=false)
	{
		List<Dot> dots1=getCalculatedDots(blue, cornerDot);
		List<Dot> dots2=getCalculatedDots(blue, cornerDot);
		dots1=dots1.OrderBy(o=>o.getDistanceNormal()).ToList();
		dots2=dots2.OrderBy(o=>o.getDistanceWarp()).ToList();
		List<Dot> dots=mergeLists(dots1, dots2);
		return dots;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static List<Dot> getCalculatedDots(bool blue, bool cornerDot=false)
	{
		List<Dot> dots=new List<Dot>();
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		GameObject[] dotObjects;
		if (blue==true)
			dotObjects=GameObject.FindGameObjectsWithTag("BlueDot");
		else
			dotObjects=GameObject.FindGameObjectsWithTag("Dot");
		foreach(GameObject dotObject in dotObjects)
		{
			if (cornerDot==true && !isCornerDot(dotObject.transform.position))
				continue;
			Dot dot = new Dot (dotObject, m_pacman, blue);
			dot.setDistanceNormal();
			dot.setDistanceWarp();
			dots.Add(dot);
		}
		return dots;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool isCornerDot(Vector3 position)
	{
		float xmargin=500;
		float ymargin=600;
		bool cond1=position.x<xmargin || position.x>=Global.getScreenWidth()-xmargin;
		bool cond2=position.y<ymargin || position.y>=Global.getScreenHeight()-ymargin;
		if (cond1 && cond2)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static List<Dot> mergeLists(List<Dot> list1, List<Dot> list2)
	{
		List<Dot> dots=new List<Dot>();
		IEnumerator it1=list1.GetEnumerator();
		IEnumerator it2=list2.GetEnumerator();
		bool hasNext1=it1.MoveNext();
		bool hasNext2=it2.MoveNext();
		while (true)
		{
			if (!hasNext1 || !hasNext2)
				break;
			Dot dot1=(Dot) it1.Current;
			Dot dot2=(Dot) it2.Current;
			if (dot1.getDistanceNormal()<=dot2.getDistanceWarp())
			{
				dot1.setSelectedMovementType(MOVEMENT_TYPE.NORMAL);
				dots.Add(dot1);
				hasNext1=it1.MoveNext();
				if (!hasNext1)
					break;
			}
			else
			{
				dot2.setSelectedMovementType(dot2.getMovementTypeWarp());
				dots.Add(dot2);
				hasNext2=it2.MoveNext();
				if (!hasNext2)
					break;
			}
		}
		while (hasNext1)
		{
			Dot dot1=(Dot) it1.Current;
			dot1.setSelectedMovementType(MOVEMENT_TYPE.NORMAL);
			dots.Add (dot1);
			hasNext1=it1.MoveNext();
		}
		while (hasNext2)
		{
			Dot dot2=(Dot) it2.Current;
			dot2.setSelectedMovementType(dot2.getMovementTypeWarp());
			dots.Add (dot2);
			hasNext2=it2.MoveNext();
		}
		return dots;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static void printList(List<Dot> dots)
	{
		Debug.Log("-----------");
		foreach(Dot dot in dots)
		{
			Debug.Log(dot.getPosition()+" "+dot.getSelectedMovementType());
		}
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------

}