using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//--------------------------------------------------------------------------------------------------------------------------------------------------
public static class PathLogic
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static MOVEMENT_TYPE m_movementTypeWarp;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float calcWarpDistanceH(Vector3 pacmanPos,Vector3 dotPos)
	{
		//Vector3 pos=safePos;
		Vector3 exit1;
		Vector3 exit2;
		if (pacmanPos.x<Global.getScreenWidth()/2)
		{
			//PACMAN IS NEAR THE LEFT SIDE OF THE SCREEN
			exit1=GameObject.Find("exitLeft").transform.position;
			exit2=GameObject.Find("exitRight").transform.position;
		}
		else
		{
			exit1=GameObject.Find("exitRight").transform.position;
			exit2=GameObject.Find("exitLeft").transform.position;
		}
		//float distanceVertical=Mathf.Abs(pacmanPos.x-exit1.x);
		float distance1=centerLogic.distanceNormal(pacmanPos,exit1);
		float distance2=centerLogic.distanceNormal(exit2,dotPos);
		return distance1+distance2;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float calcWarpDistanceToExitToDot(Vector3 pos,Vector3 exit1, Vector3 exit2,Vector3 dotPos)
	{
		float distance1=centerLogic.distanceNormal(pos,exit1);
		float distance2=centerLogic.distanceNormal(exit2,dotPos);
		return distance1+distance2;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float calcWarpDistanceV(Vector3 safePos, Vector3 dotPos)
	{
		Vector3 pos=safePos;
		Vector3 exit1;
		Vector3 exit2;
		if (pos.x<Global.getScreenWidth()/2)
		{
			//PACMAN IS NEAR LEFT
			exit1=GameObject.Find("exitDownLeft").transform.position;
			exit2=GameObject.Find("exitUpLeft").transform.position;
		}
		else
		{
			//PACMAN IS NEAR RIGHT
			exit1=GameObject.Find("exitDownRight").transform.position;
			exit2=GameObject.Find("exitUpRight").transform.position;
		}
		if (pos.y<Global.getScreenHeight()/2)
		{
			//PACMAN IS NEAR BOTTOM
			return calcWarpDistanceToExitToDot(pos,exit1,exit2,dotPos);
		}
		else
		{
			//PACMAN IS NEAR TOP
			return calcWarpDistanceToExitToDot(pos,exit2,exit1,dotPos);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float calcWarpDistance(Vector3 pacmanPos,Vector3 dotPos)
	{
		float distH=calcWarpDistanceH(pacmanPos,dotPos);
		float distV=calcWarpDistanceV(pacmanPos,dotPos);
		float min;
		if (distH<distV)
		{
			//WARP HORIZONTALLY
			min=distH;
			m_movementTypeWarp=MOVEMENT_TYPE.WARP_H;
		}
		else
		{
			//WARP VERTICALLY
			min=distV;
			m_movementTypeWarp=MOVEMENT_TYPE.WARP_V;
		}
		return min;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot findNearestDot(bool blue, bool cornerDot=false)
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		List<Dot> dots=Dots.getSortedDots(blue, cornerDot);
		foreach(Dot dot in dots)
		{
			if (SafetyLogic.playersNearDot(dot.getDot().transform.position))
			{
				continue;
			}
			dot.setAllPaths();
			if (dot.chooseSafePath())
			{
				return dot;
			}
		}
		return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool outOfScreen(Vector3 point)
	{
		float edgeMarginX=Global.EDGE_MARGIN_X;
		if (point.x<edgeMarginX || point.x>Global.getScreenWidth()-edgeMarginX || point.y<Global.EDGE_MARGIN_Y_BOTTOM || point.y>Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Dot getNextBlueDot(Dot currentGoal)
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		if (currentGoal!=null && !currentGoal.isReal())
			currentGoal.Dispose();
		Dot nearestDot=findNearestDot(true);
		return nearestDot;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}

















































//--------------------------------------------------------------------------------------------------------------------------------------------------