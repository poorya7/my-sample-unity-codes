using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
//--------------------------------------------------------------------------------------------------------------------------------------------------
public static class SafetyLogic 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float getSafetyIndex(Vector3 position)
	{
		int closenessDangerFactor=25;
		float maxDistanceToEachPlayer=Mathf.Sqrt(Global.getScreenWidth()*Global.getScreenWidth()+Global.getScreenHeight()*Global.getScreenHeight());
		GameObject[] players=playersLogic.getPlayersList();
		float totalDistance = 0;
		foreach(GameObject player in players)
		{
			if (player==null || player.GetComponent<playerHandler>().isHarmless())
				continue;
			float distanceToPlayer=Vector3.Distance(player.transform.position,position)-140;
			float playerCloseness=(maxDistanceToEachPlayer-distanceToPlayer);
			playerCloseness=playerCloseness/maxDistanceToEachPlayer;
			playerCloseness=100 * Mathf.Pow(playerCloseness, closenessDangerFactor);
			totalDistance+=playerCloseness;
		}
		//TODO
		//REMOVE
		if (totalDistance>100)
			totalDistance=100;
		return totalDistance;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float getSafetyIndexOnePlayer(Vector3 position)
	{
		int closenessDangerFactor=1;
		float maxDistanceToEachPlayer=Mathf.Sqrt(Global.getScreenWidth()*Global.getScreenWidth()+Global.getScreenHeight()*Global.getScreenHeight());
		int playerCount=1;//playerLogic.getAllPlayersCount();
		if (playerCount==0)
		{
			return 0;
		}
		GameObject player=playersLogic.getClosestDangerousPlayer(position);
		if (player==null)
			return 0;

		//140 IS THE RADIUS OF PACMAN AND PLAYER ADDED TOGETHER
		float distanceToPlayer=Vector3.Distance(player.transform.position , position)-140;
		float playerCloseness=(maxDistanceToEachPlayer-distanceToPlayer);
		playerCloseness = 100 * playerCloseness / maxDistanceToEachPlayer;
		return playerCloseness;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playersNearDot(Vector3 dot)
	{
		int playerCount=playersLogic.getPlayerCount();
		if (playerCount==0)
			return false;
		GameObject closestPlayer=playersLogic.getClosestDangerousPlayer(dot);
		if (closestPlayer==null)
			return false;
		float f=Vector3.Distance(closestPlayer.transform.position, dot);
		if (f<100)
			return true;
		else
			return false;
		//DOTO : is this the best method?
		//bool dotSafe=getSafetyIndexOnePlayer(dot)<=(Global.HIGHEST_SAFE_VAL_CHECKPOINT/playerCount);
		//return !dotSafe;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool checkpointValid(Vector3 pacman, Vector3 checkpoint, Vector3 dot)
	{

		if (checkpointOnEdge(checkpoint) || checkpointOutsideScreen(checkpoint))
		{
			return false;
		}
		int playerCount=playersLogic.getPlayerCount();
		bool checkpointSafe=getSafetyIndexOnePlayer(checkpoint)<=(Global.HIGHEST_SAFE_VAL_CHECKPOINT/playerCount);
		if (!checkpointSafe || playerCutsConnection(pacman, checkpoint) || playerCutsConnection(checkpoint, dot))
			return false;
		else
		{
			return true;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool checkpointOutsideScreen(Vector3 checkpoint)
	{
		float margin=Global.EDGE_MARGIN_X;
		bool cond1=(checkpoint.x<0 || (checkpoint.x>Global.getScreenWidth() || checkpoint.y<0 || checkpoint.y>Global.getScreenHeight()));
    	cond1=cond1 && !ExitLogic.checkpointOnExit(checkpoint);
		return cond1;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool checkpointOnEdge(Vector3 checkpoint)
	{
		float marginX=Global.EDGE_MARGIN_X;
		bool cond1=(checkpoint.x>0 && checkpoint.x<marginX) || (checkpoint.x<Global.getScreenWidth() && checkpoint.x>Global.getScreenWidth()-marginX);
		bool cond2=(checkpoint.y>0 && checkpoint.y<Global.EDGE_MARGIN_Y_BOTTOM) || (checkpoint.y<Global.getScreenHeight() && checkpoint.y>Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP);
		if (cond1 || cond2) 
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playerCutsConnection(Vector3 pointA, Vector3 pointB, bool cutOnly=false)
	{
		Vector3 diff=pointB-pointA;
		float  h=70f;
		float startDistance=100;
		if (cutOnly==true)
		{
			startDistance=-100;
			h=90;
		}
		Vector3 diff1=pointB-pointA;
		float distance1=diff1.magnitude;
		Vector3 direction1=diff1/distance1;
		
		Vector3 newv=Vector3.Cross(pointA-pointB,Vector3.forward);
		newv.Normalize();
		
		Vector3 P3 = h*newv+pointB;
		Vector3 P4 = -h*newv+pointB;
		
		Vector3 P5 = h*newv+pointA;
		Vector3 P6 = -h*newv+pointA;
		
		Vector3 diff2=P3-P5;
		float distance2=diff2.magnitude;
		Vector3 direction2=diff2/distance2;
		
		Vector3 diff3=P4-P6;
		float distance3=diff3.magnitude;
		Vector3 direction3=diff3/distance3;

		/*Debug.DrawLine (pointA+direction1*startDistance, pointB);
		Debug.DrawLine (P5+direction2*startDistance, P3);
		Debug.DrawLine (P6+direction3*startDistance, P4);*/

		int layerMask = 1 << 11;
		if (hitPositive (pointA + direction1 * startDistance, direction1, distance1, layerMask) ||
		    hitPositive (P5 + direction2 * startDistance, direction2, distance2, layerMask) ||
		    hitPositive (P6 + direction3 * startDistance, direction3, distance3, layerMask))
			return true;
		else if (cutOnly==false && playersNearLine(pointA, pointB))
		{
			return true;
		}
		else
			return false;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool playersNearLine(Vector3 pointA, Vector3 pointB)
	{
		GameObject[] players=playersLogic.getPlayersList();
		foreach (GameObject player in players) 
		{
			if (player.GetComponent<playerHandler>().isHarmless())
				continue;
			if (playerInLineRange(pointA, pointB, player.transform.position))
			{
				if (playerDistanceToLine(pointA, pointB, player.transform.position)<Global.Max_DISTANCE_PLAYER_TO_LINE)
				{
					return true;
				}
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool playerInLineRange(Vector3 pointA, Vector3 pointB, Vector3 player)
	{
		if (pointA.x == pointB.x) 
		{
			//LINE IS VERTICAL
			float yMin=Mathf.Min(pointA.y, pointB.y);
			float yMax=Mathf.Max(pointA.y, pointB.y);
			return (player.y>=yMin && player.y<=yMax);
		}
		else
		{
			//LINE IS HORIZONTAL
			float xMin=Mathf.Min(pointA.x, pointB.x);
			float xMax=Mathf.Max(pointA.x, pointB.x);
			return (player.x>=xMin && player.x<=xMax);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float playerDistanceToLine(Vector3 pointA, Vector3 pointB, Vector3 player)
	{
		float xa = pointA.x;
		float ya = pointA.y;
		float xb = pointB.x;
		float yb = pointB.y;
		float xc = player.x;
		float yc = player.y;
		float xdiff = Mathf.Pow ((xb - xa), 2);
		float ydiff = Mathf.Pow ((yb - ya), 2);
		float f = Mathf.Pow ((yb - ya) * (xc - xa) + (xb - xa) * (yc - ya), 2);
		float distanceToLine=Mathf.Sqrt (f / (xdiff + ydiff));
		return distanceToLine;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool hitPositive(Vector3 pointA, Vector3 direction, float distance, int layerMask)
	{
		var hits=Physics.RaycastAll(pointA-(direction)*20,direction,distance, layerMask);
		foreach(var hit in hits)
		{
			if (hit.transform.tag=="Player" && !hit.transform.gameObject.GetComponent<playerHandler>().isHarmless())
			{
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playersNearPacman()
	{
		Vector3 pacman = GameObject.FindGameObjectWithTag ("PacMan").transform.position;
		if (getSafetyIndexOnePlayer(pacman)>Global.HIGHEST_SAFE_VAL_PACMAN)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool playersNearPacman(float minimumSafeIndex)
	{
		Vector3 pacman = GameObject.FindGameObjectWithTag ("PacMan").transform.position;
		if (getSafetyIndexOnePlayer(pacman)>minimumSafeIndex)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}






































//--------------------------------------------------------------------------------------------------------------------------------------------------