﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//--------------------------------------------------------------------------------------------------------------------------------------------------
public partial class Dot: IDisposable
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool currentPathUnsafe(bool cutOnly=false)
	{
		bool cond=!pathSafe(m_path, cutOnly);
		return cond;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool pathSafe(PacmanPath path, bool cutOnly=false)
	{
		if (path==null)
			return false;
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		if (!path.hasNext())
		{
			bool cond1=!SafetyLogic.playerCutsConnection(pacman, getPosition(), cutOnly);
			bool cond3=!SafetyLogic.playersNearDot(getPosition());
			return (cond1 && cond3);
		}
		else
		{
			Vector3 firstNode=path.getNodes().First();
			if (path==m_path)
			{
				bool cond1;
				if (firstNode==m_nextCheckpoint)
				{
					cond1=SafetyLogic.playerCutsConnection(pacman, m_nextCheckpoint, cutOnly);
				}
				else
				{
					if (!(ExitLogic.nodeIsExit(m_nextCheckpoint) && ExitLogic.nodeIsExit(firstNode)))
					{
						cond1=SafetyLogic.playerCutsConnection(pacman, m_nextCheckpoint, cutOnly) || SafetyLogic.playerCutsConnection(m_nextCheckpoint, firstNode, cutOnly);
					}
					else if (ExitLogic.nodeIsExit(m_nextCheckpoint))
					{
						cond1=SafetyLogic.playerCutsConnection(pacman, m_nextCheckpoint, cutOnly);
					}
					else
					{
						cond1=false;
					}
				}
				bool cond2=SafetyLogic.playersNearDot(m_nextCheckpoint) || SafetyLogic.playersNearDot(firstNode);
				if (cond1 || cond2)
					return false;
				pacman=m_nextCheckpoint;
			}
			else
			{
				bool cond1=SafetyLogic.playerCutsConnection(pacman, firstNode, cutOnly);
				bool cond2=SafetyLogic.playersNearDot(firstNode);
				if (cond1 || cond2)
					return false;

			}
			foreach(Vector3 node in path.getNodes())
			{
				if (!(ExitLogic.nodeIsExit(pacman) && ExitLogic.nodeIsExit(node)))
				{
					if (SafetyLogic.playersNearDot(node))
						return false;
					if (SafetyLogic.playerCutsConnection(pacman, node, cutOnly))
						return false;
				}
				pacman=node;
			}

			return true;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------------------------------------




































































































