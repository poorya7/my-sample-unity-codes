﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//--------------------------------------------------------------------------------------------------------------------------------------------------
public partial class Dot: IDisposable
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private List<PacmanPath> m_paths;
	private PacmanPath m_path;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool pathAvailable()
	{
		if (m_paths.Count > 0)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void setAllPaths()
	{
		Global.resetRandIndex();
		m_paths.Clear();
		if (m_selectedMovementType==MOVEMENT_TYPE.NORMAL)
		{
			setNormalPaths();
		}
		else
		{
			setWarpPaths();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool safePathAvailable()
	{
		foreach (PacmanPath path in m_paths)
		{
			if (pathSafe(path))
			{
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void moveNewDebuggers()
	{
		foreach (PacmanPath path in m_paths)
		{
			if (path==m_path)
			{
				GameObject.Find("debuggers").transform.Find("main_goal").transform.position=getPosition();
				GameObject.Find("debuggers").transform.Find("main_checkpoint").transform.position=getCheckpoint();
			}
			else
			{
				return;
				if (path.getNodes().Count>0)
					GameObject.Find("debuggers").transform.Find("alt_goal").transform.position=path.getNodes()[0];
				if (path.getNodes().Count>0)
					GameObject.Find("debuggers").transform.Find("alt_checkpoint").transform.position=path.getNodes()[0];
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool chooseSafePath(bool cutOnly=false)
	{
		bool b=chooseLessMovementPath(cutOnly);
		return b;
		//m_path.print();
		if (m_isPlayer==false)
		{
			foreach (PacmanPath path in m_paths)
			{
				if (pathSafe(path, cutOnly))
			    {
					m_path=path;
					m_nextCheckpoint=m_path.checkNext();
					updateDebugObjects();
					makeBoxCheckpoint(m_nextCheckpoint);
					return true;
				}
			}
			return false;
		}
		else
		{
			return chooseLessMovementPath(cutOnly);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool chooseLessMovementPath(bool cutOnly)
	{
		List<PacmanPath> safepaths=new List<PacmanPath>();
		foreach (PacmanPath path in m_paths)
		{
			if (pathSafe(path, cutOnly))
			{
				if (hasSameMovement(path))
			    {
					m_path=path;
					m_nextCheckpoint=m_path.checkNext();
					updateDebugObjects();
					makeBoxCheckpoint(m_nextCheckpoint);
					return true;
				}
				else
				{
					safepaths.Add(path);
				}
			}
		}
		if (safepaths.Count>0)
		{
			m_path=safepaths[0];
			float maxDistance=Vector3.Distance(m_pacman.transform.position,m_path.checkNext());
			for (int i=1;i<safepaths.Count();i++)
			{
				float distanceToFirstCheckpoint=Vector3.Distance(m_pacman.transform.position,safepaths[i].checkNext());
				if ((distanceToFirstCheckpoint-maxDistance)>500)
				{
					m_path=safepaths[i];
					maxDistance=distanceToFirstCheckpoint;
				}
			}
			m_nextCheckpoint=m_path.checkNext();
			updateDebugObjects();
			makeBoxCheckpoint(m_nextCheckpoint);
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool hasSameMovement(PacmanPath path)
	{
		Vector3 movementVector=m_pacman.GetComponent<PacManAI>().m_movementVector;
		bool isHorizontal=movementVector.x!=0;
		bool pathHorizontal=path.checkNext().y==m_pacman.transform.position.y;
		if ((isHorizontal && pathHorizontal) || (!isHorizontal && !pathHorizontal))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setWarpPaths()
	{
		Vector3 exitIn=ExitLogic.getExitIn(m_pacman.transform.position, m_selectedMovementType);
		Vector3 checkpointIn=ExitLogic.getExitInCheckpoint(m_pacman.transform.position, exitIn);
		Vector3 exitOut=ExitLogic.getOppositeExit(exitIn);
		Vector3 checkpointOut = ExitLogic.getDotCheckpointOnOppositeSide (exitOut, getPosition ());
		if (!align(m_pacman.transform.position, exitIn))
		{
			if (!CheckpointLogic.checkpointInScreen(checkpointIn) && checkpointIn!=exitIn)
				return;
			if (centerLogic.checkpointInvalid(m_pacman.transform.position, checkpointIn, exitIn))
			{
				Vector3 altCheckpointIn=centerLogic.getAlternateCheckpoint(m_pacman.transform.position, exitIn);
				setAltPathsNormal(m_pacman, altCheckpointIn, exitIn, exitOut);
				return;
			}
		}
		if (centerLogic.checkpointInvalid(exitOut, checkpointOut, getPosition()))
		{
			Vector3 altCheckpointOut=centerLogic.getAlternateCheckpoint(exitOut, getPosition());
			setAltPathsOut(m_pacman.transform.position, altCheckpointOut, exitIn, exitOut);
			return;
		}
		PacmanPath path=new PacmanPath();
		path.add (checkpointIn);
		path.add (exitIn);
		path.add (exitOut);
		path.add (checkpointOut);
		path.add (getPosition());
		m_paths.Add (path);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setNormalPaths()
	{
		if (m_pacman == null)
			m_pacman = GameObject.FindGameObjectWithTag ("PacMan");
		Vector3 checkpointH=CheckpointLogic.getCheckpointH(m_pacman.transform.position, getPosition());
		Vector3 checkpointV=CheckpointLogic.getCheckpointV(m_pacman.transform.position, getPosition());
		bool cond1=Vector3.Distance(m_pacman.transform.position, checkpointH)>100;
		bool cond2=Vector3.Distance(m_pacman.transform.position, checkpointV)>100;
		if (m_isPlayer)
		{
			if (cond1)
				setNormalPaths(m_pacman, checkpointH);
			if (cond2)
				setNormalPaths(m_pacman, checkpointV);
		}
		else
		{
			if (m_pacman.transform.position!=checkpointH)
				setNormalPaths(m_pacman, checkpointH);
			if (m_pacman.transform.position!=checkpointV)
				setNormalPaths(m_pacman, checkpointV);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*bool reachedPos(Vector3 pos)
	{
		return GameObject.FindGameObjectWithTag("PacMan").GetComponent<PacManAI>().reachedPosition(pos);
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setNormalPaths(GameObject pacman, Vector3 checkpoint)
	{
		Vector3 pos = pacman.transform.position;
		if (!CheckpointLogic.checkpointInScreen(checkpoint))
			return;
		else if (centerLogic.checkpointInvalid(pos, checkpoint, getPosition()))
		{
			Vector3 altCheckpoint=centerLogic.getAlternateCheckpoint(pos, getPosition());
			setAltPathsNormal(pos, altCheckpoint);
			return;
		}
		PacmanPath path=new PacmanPath();
		path.add (checkpoint);
		path.add (getPosition());
		m_paths.Add (path);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setAltPathsNormal(GameObject pacman, Vector3 altCheckpointIn, Vector3 exitIn, Vector3 exitOut)
	{
		//USED FOR WARP MOVEMENT
		List<Vector3> checkpointsPacmanToAlt=new List<Vector3>();
		List<Vector3> checkpointsAltToExitIn=new List<Vector3>();
		checkpointsPacmanToAlt.Add (CheckpointLogic.getCheckpointH(pacman.transform.position, altCheckpointIn));
		checkpointsPacmanToAlt.Add (CheckpointLogic.getCheckpointV(pacman.transform.position, altCheckpointIn));
		Vector3 exitInCheckpoint=ExitLogic.getExitOutCheckpoint(exitIn);
		checkpointsAltToExitIn.Add (CheckpointLogic.getCheckpointH(altCheckpointIn, exitInCheckpoint));
		checkpointsAltToExitIn.Add (CheckpointLogic.getCheckpointV(altCheckpointIn, exitInCheckpoint));
		foreach(Vector3 checkpointPacmanToAlt in checkpointsPacmanToAlt)
		{
			foreach(Vector3 checkpointAltToExitIn in checkpointsAltToExitIn)
			{
				if (checkpointPacmanToAlt==altCheckpointIn || altCheckpointIn==checkpointAltToExitIn)
					continue;
				bool endValid=!centerLogic.checkpointInvalid(altCheckpointIn, checkpointAltToExitIn, exitIn);
				if (endValid)
				{
					PacmanPath path=new PacmanPath();
					if (align(checkpointPacmanToAlt, altCheckpointIn, checkpointAltToExitIn))
					{
						path.add(checkpointPacmanToAlt);
						if (checkpointAltToExitIn!=getPosition())
						{
							path.add (checkpointAltToExitIn);
						}
					}
					else
					{
						path.add(checkpointPacmanToAlt);
						path.add(altCheckpointIn);
						if (checkpointAltToExitIn!=getPosition())
						{
							path.add (checkpointAltToExitIn);
						}
					}
					path.add (exitInCheckpoint);
					path.add (exitIn);
					path.add (exitOut);
					path.add (ExitLogic.getDotCheckpointOnOppositeSide(exitOut, getPosition()));
					path.add (getPosition());
					m_paths.Add (path);
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setAltPathsOut(Vector3 pacman, Vector3 altCheckpointOut, Vector3 exitIn, Vector3 exitOut)
	{
		//USED FOR WARP MOVEMENT
		Vector3 checkpointIn=ExitLogic.getExitInCheckpoint(pacman, exitIn);

		List<Vector3> checkpointsExitOutToAlt=new List<Vector3>();
		List<Vector3> checkpointsAltToDot=new List<Vector3>();

		checkpointsExitOutToAlt.Add (CheckpointLogic.getCheckpointH(exitOut, altCheckpointOut));
		checkpointsExitOutToAlt.Add (CheckpointLogic.getCheckpointV(exitOut, altCheckpointOut));

		checkpointsAltToDot.Add (CheckpointLogic.getCheckpointH(altCheckpointOut, getPosition()));
		checkpointsAltToDot.Add (CheckpointLogic.getCheckpointV(altCheckpointOut, getPosition()));
		foreach(Vector3 checkpointExitOutToAlt in checkpointsExitOutToAlt)
		{
			foreach(Vector3 checkpointAltToDot in checkpointsAltToDot)
			{
				if (checkpointExitOutToAlt==altCheckpointOut || altCheckpointOut==checkpointAltToDot)
					continue;
				bool endValid=!centerLogic.checkpointInvalid(exitOut, checkpointExitOutToAlt, altCheckpointOut);
				if (endValid)
				{
					PacmanPath path=new PacmanPath();
					path.add (checkpointIn);
					path.add (exitIn);
					path.add (exitOut);
					if (align(checkpointExitOutToAlt, altCheckpointOut, checkpointAltToDot))
					{
						path.add(checkpointExitOutToAlt);
						if (checkpointAltToDot!=getPosition())
						{
							path.add (checkpointAltToDot);
						}
					}
					else
					{
						path.add(checkpointExitOutToAlt);
						path.add(altCheckpointOut);
						if (checkpointAltToDot!=getPosition())
						{
							path.add (checkpointAltToDot);
						}
					}
					path.add (getPosition());
					m_paths.Add (path);
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setAltPathsNormal(Vector3 pacman, Vector3 altCheckpoint)
	{
		List<Vector3> checkpointsPacmanToAlt=new List<Vector3>();
		List<Vector3> checkpointsAltToDot=new List<Vector3>();
		if (true)//align(pacman, altCheckpoint))
	    {
			Vector3 checkpointh1=CheckpointLogic.getCheckpointH(pacman, altCheckpoint);
			if (pacman!=checkpointh1)
				checkpointsPacmanToAlt.Add (checkpointh1);
			Vector3 checkpointv1=CheckpointLogic.getCheckpointV(pacman, altCheckpoint);
			if (pacman!=checkpointv1)
				checkpointsPacmanToAlt.Add (checkpointv1);
		}
		Vector3 checkpointh2=CheckpointLogic.getCheckpointH(altCheckpoint, getPosition());
		if (pacman!=checkpointh2)
			checkpointsAltToDot.Add (checkpointh2);
		Vector3 checkpointv2=CheckpointLogic.getCheckpointV(altCheckpoint, getPosition());
		if (pacman!=checkpointv2)
  			checkpointsAltToDot.Add (checkpointv2);
		foreach(Vector3 checkpointPacmanToAlt in checkpointsPacmanToAlt)
		{
			foreach(Vector3 checkpointAltToDot in checkpointsAltToDot)
			{
				/*if (checkpointPacmanToAlt==altCheckpoint || altCheckpoint==checkpointAltToDot || Vector3.Distance(checkpointPacmanToAlt,pacman)<5)
					continue;*/
				bool checkpointAltToDotValid=!centerLogic.checkpointInvalid(altCheckpoint, checkpointAltToDot, getPosition());
				if (checkpointAltToDotValid)
				{
					PacmanPath path=new PacmanPath();
					/*if (align(checkpointPacmanToAlt, altCheckpoint, checkpointAltToDot))
					{
						path.add(checkpointPacmanToAlt);
						if (checkpointAltToDot!=getPosition())
						{
							path.add (checkpointAltToDot);
						}
					}
					else*/
					{
						if (!align(pacman, altCheckpoint))
							path.add(checkpointPacmanToAlt);
						if (pacman!=altCheckpoint)
							path.add(altCheckpoint);
						if (checkpointAltToDot!=getPosition())
						{
							path.add (checkpointAltToDot);
						}
					}
					path.add (getPosition());
					m_paths.Add (path);
				}
			}
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool equal(Vector3 a, Vector3 b)
	{
		if (Vector3.Distance(a,b)==0)
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool align(Vector3 a, Vector3 b)
	{
		if (Math.Abs(a.x-b.x)<1)
			return true;
		else if (Math.Abs(a.y-b.y)<1)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool align(Vector3 a, Vector3 b, Vector3 c)
	{
		if (Math.Abs(a.x-b.x)<1 && Math.Abs(b.x-c.x)<1 && Math.Abs(a.x- c.x)<1)
			return true;
		else if (Math.Abs(a.y-b.y)<1 && Math.Abs(b.y-c.y)<1 && Math.Abs(a.y- c.y)<1)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void updateDebugObjects()
	{
		return;
		GameObject[] oldCheckpoints=GameObject.FindGameObjectsWithTag("checkpointNew");
		foreach(GameObject c in oldCheckpoints)
			GameObject.Destroy(c);
		GameObject[] oldGoals=GameObject.FindGameObjectsWithTag("goalNew");
		foreach(GameObject c in oldGoals)
			GameObject.Destroy(c);
		if (m_path==null)
			return;
		PacmanPath path=m_path;
		Vector3 firstNode=m_pacman.transform.position;
		drawMiddlePoints(firstNode, m_nextCheckpoint, path);
		firstNode=m_nextCheckpoint;
		foreach(Vector3 node in path.getNodes())
		{
			if (!(ExitLogic.nodeIsExit(firstNode) && ExitLogic.nodeIsExit(node)))
				drawMiddlePoints(firstNode, node, path);
			firstNode=node;
		}	
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void makeBox(Vector3 pos, PacmanPath path)
	{
		GameObject checkpoint1=GameObject.FindGameObjectWithTag("cubePoint");
		if (checkpoint1 == null)
			return;
		GameObject checkpointNew = GameObject.Instantiate (checkpoint1, pos, Quaternion.identity);
		checkpointNew.tag="checkpointNew";
		checkpointNew.GetComponent<Renderer>().material.color=path.getColor();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void makeBoxCheckpoint(Vector3 pos)
	{
		return;
		GameObject goal=GameObject.FindGameObjectWithTag("goal");
		GameObject goalNew= (GameObject) GameObject.Instantiate(goal, pos, Quaternion.identity);
		goalNew.tag="goalNew";
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void drawMiddlePoints(Vector3 start, Vector3 end, PacmanPath path)
	{
		int counter=0;
		if (path == null)
			return;
		List<Vector3>middlePoints=new List<Vector3>();
		float step=path.getStep();
		float startX=start.x;
		float startY=start.y;
		float endX=end.x;
		float endY=end.y;
		Vector3 diff=(end-start).normalized;
		float loopStart,loopEnd, loopStep;
		if (startX<=endX)
		{
			loopStart=startX;
			loopEnd=endX;
			loopStep=diff.x*step;
		}
		else
		{
			loopStart=endX;
			loopEnd=startX;
			loopStep=diff.x*step*-1;
		}
		if (startX==endX)
		{
			if (startY<=endY)
			{
				loopStart=startY;
				loopEnd=endY;
				loopStep=diff.y*step;
			}
			else
			{
				loopStart=endY;
				loopEnd=startY;
				loopStep=diff.y*step*-1;
			}
		}
		int k=1;
		for (float i=loopStart;i<loopEnd;i+=loopStep)
		{
			float x=startX+(diff.x)*step*k;
			float y=startY+(diff.y)*step*k;
			if ((x>=startX && x<=endX) || (x<=startX && x>=endX))
			{
				if ((y>=startY && y<=endY) || (y<=startY && y>=endY)) 
				{
					Vector3 newPos=new Vector3(x,y,start.z);
					makeBox(newPos, path);
					if (centerLogic.checkpointInsideCenter(newPos))
					{
						counter++;
					}
				}
			}
			k++;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void printPaths()
	{
		Debug.Log("--------------PATHS---------------------");
		foreach(PacmanPath path in m_paths)
			path.print();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}





































































