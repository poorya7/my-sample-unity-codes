using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

//--------------------------------------------------------------------------------------------------------------------------------------------------
public partial class Dot:IDisposable
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private readonly GameObject m_dot;
	private readonly bool m_isPlayer;
	private readonly bool m_dotIsReal;
	private float m_distanceNormal;
	private float m_distanceWarp;
	private bool m_openNormal;
	private bool m_openWarp;
	private MOVEMENT_TYPE m_movementTypeWarp;
	private MOVEMENT_TYPE m_selectedMovementType;
	private bool m_isBlue;
	GameObject	m_pacman;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Dot(GameObject dot,GameObject pacman, bool isBlue)
	{
		m_dot=dot;
		m_pacman = pacman;
		//m_dot.transform.localScale=new Vector3(1,1,1);
		m_dotIsReal=true;
		m_paths=new List<PacmanPath>();
		m_isBlue=isBlue;
		m_isPlayer=false;
	}
	public Dot(Vector3 dot)
	{
		m_pacman = GameObject.FindGameObjectWithTag ("PacMan");
		m_dot=new GameObject();
		GameObject previousPoint=GameObject.FindGameObjectWithTag ("RandomPoint");
		m_dot.tag="RandomPoint";
		GameObject.Destroy (previousPoint);
		m_dot.name="Random Point";
		m_dot.transform.position=dot;
		m_dotIsReal=false;
		m_paths=new List<PacmanPath>();
		m_isBlue=false;
		m_isPlayer=false;
	}
	public Dot(Vector3 dot, bool isPlayer)
	{
		m_pacman = GameObject.FindGameObjectWithTag ("PacMan");
		m_isPlayer=isPlayer;
		m_dot=new GameObject();
		GameObject previousPoint=GameObject.FindGameObjectWithTag ("RandomPoint");
		m_dot.tag="RandomPoint";
		GameObject.Destroy (previousPoint);
		m_dot.name="Random Point";
		m_dot.transform.position=dot;
		m_dotIsReal=false;
		m_paths=new List<PacmanPath>();
		m_isBlue=false;
	}
	public void Dispose()
	{
		GameObject.Destroy(m_dot);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool isBlue() { return m_isBlue; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public GameObject getDot() 
	{ 
		if (m_dot!=null)
			return m_dot; 
		else
			return null;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool isReal() { return m_dotIsReal; }
	public float getDistanceNormal() { return m_distanceNormal; }
	public float getDistanceWarp() { return m_distanceWarp; }
	public MOVEMENT_TYPE getMovementTypeWarp() { return m_movementTypeWarp; }
	public bool isOpenNormal() { return m_openNormal; }
	public bool isOpenWarp() { return m_openWarp; }
	public MOVEMENT_TYPE getSelectedMovementType() { return m_selectedMovementType; }
	public void setSelectedMovementType(MOVEMENT_TYPE type) { m_selectedMovementType=type; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Vector3 getPosition() 
	{ 
		return m_dot.transform.position; 
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void updateParameters()
	{
		setDistanceNormal();
		setDistanceWarp();
		setMovementType();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setMovementType()
	{
		if (m_distanceNormal<m_distanceWarp)
			setSelectedMovementType(MOVEMENT_TYPE.NORMAL);
		else
			setSelectedMovementType(m_movementTypeWarp);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void setDistanceNormal() 
	{ 
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		m_distanceNormal=centerLogic.distanceNormal(pacman, m_dot.transform.position);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void setDistanceWarp() 
	{ 
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		m_distanceWarp=PathLogic.calcWarpDistance(pacman, m_dot.transform.position);
		m_movementTypeWarp=PathLogic.m_movementTypeWarp;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public void setPathSafety() 
	{
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		m_openNormal=SafetyLogic.pathSafeNormal(pacman, this);
		m_openWarp=SafetyLogic.pathSafeWarp(pacman, this);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public bool pathSafe()
	{
		if (m_selectedMovementType==MOVEMENT_TYPE.NORMAL)
		{
			if (m_openNormal)
				return true;
			else
				return false;
		}
		else
		{
			if (m_openWarp)
				return true;
			else
				return false;
		}
		if (getBestMovementType()==m_selectedMovementType)
			return true;
		else
			return false;
		if (m_openNormal || m_openWarp)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public MOVEMENT_TYPE getBestMovementType()
	{
		if (m_openNormal && m_openWarp)
		{
			if (m_distanceNormal<=m_distanceWarp)
				return MOVEMENT_TYPE.NORMAL;
			else
				return m_movementTypeWarp;
		}
		else if (m_openNormal)
		{
			return MOVEMENT_TYPE.NORMAL;
		}
		else if (m_openWarp)
			return m_movementTypeWarp;
		else
		{
			//FIND ALTERNATIVE WARP
			return MOVEMENT_TYPE.NONE;//  alternateWarpPath();
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	MOVEMENT_TYPE alternateWarpPath()
	{
		Vector3 exitLeft=GameObject.Find ("exitLeft").transform.position;
		Vector3 exitRight=GameObject.Find ("exitRight").transform.position;
		Vector3 exitUpLeft=GameObject.Find ("exitUpLeft").transform.position;
		Vector3 exitUpRight=GameObject.Find ("exitUpRight").transform.position;
		Vector3 exitDownLeft=GameObject.Find ("exitDownLeft").transform.position;
		Vector3 exitDownRight=GameObject.Find ("exitDownRight").transform.position;
		Vector3 pacman=GameObject.FindGameObjectWithTag("PacMan").transform.position;
		if (m_movementTypeWarp==MOVEMENT_TYPE.WARP_H)
		{
			//EXCLUDE HORIZONTAL
			bool cond1=SafetyLogic.pathSafeNormal(pacman, new Dot(exitUpLeft)) && SafetyLogic.pathSafeWarp(exitUpLeft, this);
			bool cond2=SafetyLogic.pathSafeNormal(pacman, new Dot(exitUpRight)) && SafetyLogic.pathSafeWarp(exitUpRight, this);
			                                      bool cond3=SafetyLogic.pathSafeNormal(pacman, new Dot(exitDownLeft)) && SafetyLogic.pathSafeWarp(exitDownLeft, this);
			                                      bool cond4=SafetyLogic.pathSafeNormal(pacman, new Dot(exitDownRight)) && SafetyLogic.pathSafeWarp(exitDownRight, this);
			if (cond1 || cond2 || cond3 || cond4)
				return MOVEMENT_TYPE.WARP_V;
		}
		else
		{
			//EXCLUDE VERTICAL
				bool cond1=SafetyLogic.pathSafeNormal(pacman, new Dot(exitLeft)) && SafetyLogic.pathSafeWarp(exitLeft, this);
				                                      bool cond2=SafetyLogic.pathSafeNormal(pacman, new Dot(exitRight)) && SafetyLogic.pathSafeWarp(exitRight, this);
			if (cond1 || cond2)
				return MOVEMENT_TYPE.WARP_H;
		}
		return MOVEMENT_TYPE.NONE;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}



























































































//--------------------------------------------------------------------------------------------------------------------------------------------------