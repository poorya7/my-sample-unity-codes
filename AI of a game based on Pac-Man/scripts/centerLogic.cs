﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class centerLogic 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float m_centerWidth;
	static float m_centerHeight;
	static Vector3 m_centerPos;
	static Vector3 m_topLeft;
	static Vector3 m_topRight;
	static Vector3 m_bottomLeft;
	static Vector3 m_bottomRight;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void init()
	{
		GameObject centerBounds=GameObject.FindGameObjectWithTag("CheckpointCollider");
		m_centerWidth=centerBounds.transform.localScale.x+200;
		m_centerHeight=centerBounds.transform.localScale.y+200;
		m_centerPos=centerBounds.transform.position;
		m_topLeft=new Vector3(m_centerPos.x-m_centerWidth/2,m_centerPos.y+m_centerHeight/2,m_centerPos.z);
		m_topRight=new Vector3(m_centerPos.x+m_centerWidth/2,m_centerPos.y+m_centerHeight/2,m_centerPos.z);
		m_bottomLeft=new Vector3(m_centerPos.x-m_centerWidth/2,m_centerPos.y-m_centerHeight/2,m_centerPos.z);
		m_bottomRight=new Vector3(m_centerPos.x+m_centerWidth/2,m_centerPos.y-m_centerHeight/2,m_centerPos.z);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void drawBorders()
	{
		Debug.DrawLine(m_topLeft,m_topRight);
		Debug.DrawLine(m_topRight,m_bottomRight);
		Debug.DrawLine(m_bottomRight,m_bottomLeft);
		Debug.DrawLine(m_bottomLeft,m_topLeft);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool pacmanInsideCenter(Vector3 pacman)
	{
		float minX=m_topLeft.x;
		float maxX=m_bottomRight.x;
		float minY=m_bottomLeft.y;
		float maxY=m_topRight.y;
		if (pacman.x>minX && pacman.x<maxX && pacman.y>minY && pacman.y<maxY)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool checkpointInsideCenter(Vector3 checkpoint)
	{
		GameObject centerBounds=GameObject.FindGameObjectWithTag("CheckpointCollider");
		float minx=centerBounds.transform.position.x-centerBounds.transform.localScale.x/2;
		float maxx=centerBounds.transform.position.x+centerBounds.transform.localScale.x/2;
		float miny=centerBounds.transform.position.y-centerBounds.transform.localScale.y/2;
		float maxy=centerBounds.transform.position.y+centerBounds.transform.localScale.y/2;
		if (checkpoint.x>minx && checkpoint.x<maxx && checkpoint.y>miny && checkpoint.y<maxy)
			return true;
		else
			return false;
		/*float minX=m_topLeft.x;
		float maxX=m_bottomRight.x;
		float minY=m_bottomLeft.y;
		float maxY=m_topRight.y;*/

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float distanceNormal(Vector3 pointA, Vector3 pointB)
	{
		float distance;
		Vector3 checkpointH=new Vector3 (pointB.x, pointA.y, pointA.z);
		Vector3 checkpointV=new Vector3 (pointA.x, pointB.y, pointA.z);
		if (!centerCutsConnection(pointA, checkpointH) && !centerCutsConnection(checkpointH, pointB))
		{
			distance=Mathf.Abs(pointA.x-pointB.x)+Mathf.Abs(pointA.y-pointB.y);
		}
		else if (!centerCutsConnection(pointA, checkpointV) && !centerCutsConnection(checkpointV, pointB))
		{
			distance=Mathf.Abs(pointA.x-pointB.x)+Mathf.Abs(pointA.y-pointB.y);
		}
		else
		{
			Vector3 alternateCheckpoint=getAlternateCheckpoint(pointA, pointB);
			distance=Mathf.Abs(pointA.x-alternateCheckpoint.x)+Mathf.Abs(pointA.y-alternateCheckpoint.y);
			distance+=Mathf.Abs(pointB.x-alternateCheckpoint.x)+Mathf.Abs(pointB.y-alternateCheckpoint.y);
		}
		return distance;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getAlternateCheckpoint(Vector3 pacman, Vector3 goal)
	{
		float minDistance=1000000;
		Vector3 selectedCorner=pacman;
		List<Vector3> closestCorners=centerLogic.getClosestCorners(pacman);
		foreach(Vector3 corner in closestCorners)
		{
			float distance=getManhattanDistance(pacman, corner) + getManhattanDistance(corner, goal);
			if (distance<minDistance)
			{
				minDistance=distance;
				selectedCorner=corner;
			}
		}
		return selectedCorner;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static List<Vector3> getClosestCorners(Vector3 pacman)
	{
		Dictionary<Vector3, float> sortedCorners=new Dictionary<Vector3, float>();
		//if (Vector3.Distance(pacman,m_topLeft)>10)
			sortedCorners.Add(m_topLeft, Vector3.Distance(pacman, m_topLeft));
		//if (Vector3.Distance(pacman,m_topRight)>10)
			sortedCorners.Add(m_topRight, Vector3.Distance(pacman, m_topRight));
		//if (Vector3.Distance(pacman,m_bottomLeft)>10)
			sortedCorners.Add(m_bottomLeft, Vector3.Distance(pacman, m_bottomLeft));
		//if (Vector3.Distance(pacman,m_bottomRight)>10)
			sortedCorners.Add(m_bottomRight, Vector3.Distance(pacman, m_bottomRight));
		var sortedDict = from entry in sortedCorners orderby entry.Value ascending select entry;
		Vector3 c1= sortedDict.ElementAt(0).Key;
		Vector3 c2= sortedDict.ElementAt(1).Key;
		Vector3 c3= sortedDict.ElementAt(2).Key;
		List<Vector3> closestCorners=new List<Vector3>();
		closestCorners.Add(c1);
		closestCorners.Add(c2);
		/*closestCorners.Add(c3);
		closestCorners.Add(c4);
		return closestCorners;*/
		if (!lineSegmentsIntersec(c1,c2,pacman,c3) && !pacmanInsideCenter(pacman))
			closestCorners.Add (c3);
		//WE REVERSE THE CORNERS SO THE CLOSEST CORNER 
		// GETS PICKED LAST IN THE FNAL FUNCTION.
		closestCorners.Reverse();
		return closestCorners;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool lineSegmentsIntersec(Vector3 a1, Vector3 a2, Vector3 b1, Vector3 b2)
	{
		a1.z=0;a2.z=0;b2.z=0;b2.z=0;
		float s1_x, s1_y, s2_x, s2_y;
		s1_x = a2.x - a1.x;     s1_y = a2.y - a1.y;
		s2_x = b2.x - b1.x;     s2_y = b2.y - b1.y;
		float s, t;
		s = (-s1_y * (a1.x - b1.x) + s1_x * (a1.y - b1.y)) / (-s2_x * s1_y + s1_x * s2_y);
		t = ( s2_x * (a1.y - b1.y) - s2_y * (a1.x - b1.x)) / (-s2_x * s1_y + s1_x * s2_y);
		if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		{
			float ix = a1.x + (t * s1_x);
			float iy = a1.y + (t * s1_y);
			Vector3 intersect=new Vector3(ix,iy,0);
			return true;
		}
		return false; 
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float getManhattanDistance(Vector3 pointA, Vector3 pointB)
	{
		return Mathf.Abs(pointA.x-pointB.x)+Mathf.Abs(pointA.y-pointB.y);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static float getCornerRerouteDistance(Vector3 corner, Vector3 goal, List<Vector3> excludedCorners)
	{
		return Mathf.Abs(corner.x-goal.x)+Mathf.Abs(corner.y-goal.y);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool centerCutsConnection(Vector3 pointA, Vector3 pointB)
	{
		/*if (checkpointInsideCenter(pointA) || checkpointInsideCenter(pointB))
			return true;*/
		//pointB=m_topLeft;
		//Debug.DrawLine(pointA, pointB);
		GameObject centerBounds=GameObject.FindGameObjectWithTag("CheckpointCollider");
		float minx=centerBounds.transform.position.x-centerBounds.transform.localScale.x/2;
		float maxx=centerBounds.transform.position.x+centerBounds.transform.localScale.x/2;
		float miny=centerBounds.transform.position.y-centerBounds.transform.localScale.y/2;
		float maxy=centerBounds.transform.position.y+centerBounds.transform.localScale.y/2;
		if (pointA.x==pointB.x)
		{
			float x=pointA.x;
			Vector3 min, max;
			if (pointA.y<pointB.y)
			{
				min=pointA;
				max=pointB;
			}
			else
			{
				min=pointB;
				max=pointA;
			}
			if (x>minx && x<maxx)
			{
				if (min.y<=Global.getScreenHeight()/2 && max.y>Global.getScreenHeight()/2)
					return true;
			}
			return false;
		}
		else if (pointA.y==pointB.y)
		{
			float y=pointA.y;
			Vector3 min, max;
			if (pointA.x<pointB.x)
			{
				min=pointA;
				max=pointB;
			}
			else
			{
				min=pointB;
				max=pointA;
			}
			if (y>miny && y<maxy)
			{
				if (min.x<=Global.getScreenWidth()/2 && max.x>Global.getScreenWidth()/2)
					return true;
			}
			return false;
		}
		else
		{
			return raycastHitPositive(pointA, pointB);
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool raycastHitPositive(Vector3 pointA, Vector3 pointB)
	{
		Vector3 difference=pointB-pointA;
		float distance=difference.magnitude;
		Vector3 direction=difference/distance;
		RaycastHit hit;
		int layerMask = 1 << 8;
		if (Physics.Raycast (pointA, direction, out hit, distance, layerMask)) 
		{
			if (hit.transform.tag == "CheckpointCollider")
			{
				return true;
			}
		} 
		else if (Physics.Raycast (pointB, -direction, out hit, distance, layerMask)) 
		{
			if (hit.transform.tag == "CheckpointCollider")
			{
				return true;
			}
		} 
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*public static float lineDistanceToCenter(Vector3 pointA, Vector3 pointB)
	{
		Debug.DrawLine(pointA, pointB);
		float xa = pointA.x;
		float ya = pointA.y;
		float xb = pointB.x;
		float yb = pointB.y;
		float xc = Global.getScreenWidth()/2;
		float yc = Global.getScreenHeight()/2;
		float xdiff = Mathf.Pow ((xb - xa), 2);
		float ydiff = Mathf.Pow ((yb - ya), 2);
		float f = Mathf.Pow ((yb - ya) * (xc - xa) + (xb - xa) * (yc - ya), 2);
		float distanceToLine=Mathf.Sqrt (f / (xdiff + ydiff));
		return distanceToLine;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool hitPositive(Vector3 pointA, Vector3 direction, float distance, int layerMask)
	{
		var hits1=Physics2D.RaycastAll(pointA ,direction ,distance, layerMask);
		foreach(var hit in hits1)
		{
			if (hit.transform.tag=="CheckpointCollider")
			{
				return true;
			}
		}
		var hits2=Physics.RaycastAll(pointA ,-direction ,distance, layerMask);
		foreach(var hit in hits2)
		{
			if (hit.transform.tag=="CheckpointCollider")
			{
				return true;
			}
		}
		return false;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool checkpointInvalid(Vector3 pacman, Vector3 checkpoint, Vector3 goal)
	{
		bool cond=centerLogic.checkpointInsideCenter(checkpoint) || centerLogic.centerCutsConnection(pacman, checkpoint) || centerLogic.centerCutsConnection(checkpoint, goal);
		return cond;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}





































































//--------------------------------------------------------------------------------------------------------------------------------------------------