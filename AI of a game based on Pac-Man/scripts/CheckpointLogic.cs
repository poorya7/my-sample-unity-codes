﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//--------------------------------------------------------------------------------------------------------------------------------------------------
public class CheckpointLogic
{

	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getCheckpointH(Vector3 pacman, Vector3 dot)
	{
		return new Vector3 (dot.x, pacman.y, pacman.z);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 getCheckpointV(Vector3 pacman, Vector3 dot)
	{
		return new Vector3 (pacman.x, dot.y, pacman.z);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool checkpointInScreen(Vector3 checkpoint)
	{
		bool cond1=checkpoint.x>=Global.EDGE_MARGIN_X && checkpoint.x<=Global.getScreenWidth()-Global.EDGE_MARGIN_X;
		bool cond2=checkpoint.y>=Global.EDGE_MARGIN_Y_BOTTOM && checkpoint.y<=Global.getScreenHeight()-Global.EDGE_MARGIN_Y_TOP;
		if (cond1 && cond2)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}










































































//--------------------------------------------------------------------------------------------------------------------------------------------------