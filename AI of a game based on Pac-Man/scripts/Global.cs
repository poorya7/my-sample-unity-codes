using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//--------------------------------------------------------------------------------------------------------------------------------------------------
public enum MOVEMENT_TYPE
{
	NORMAL,
	WARP_H,
	WARP_V, 
	NONE
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public static class Global 
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static readonly float minDistBetweenDotNCheckpoint=40;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static int m_initDotCount;
	public static void setInitDotCount(int count) { m_initDotCount=count; }
	public static int getInitDotCount() { return m_initDotCount; }
	private static int m_initBlueDotCount;
	public static void setInitBlueDotCount(int count) { m_initBlueDotCount=count; }
	public static int getInitBlueDotCount() { return m_initBlueDotCount; }
	public static readonly Color BLUE_COLOR = new Color (41 / 255f, 44 / 255f, 250 / 255f);
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static readonly float EDGE_MARGIN_X=140;
	public static readonly float EDGE_MARGIN_Y_BOTTOM=450;
	public static readonly float EDGE_MARGIN_Y_TOP=140;
	private static byte m_countDownDuration=3;
	public static void setCountDownDuration(byte duration) { m_countDownDuration=duration; }
	public static byte getCountDown() { return m_countDownDuration;	}
	public static bool BLUE_MODE;
	public static bool GAME_OVER;
	public static bool GAME_STARTED;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float HIGHEST_SAFE_VAL_DOT=95;
	public static float HIGHEST_SAFE_VAL_PATH=80;
	public static float HIGHEST_SAFE_VAL_RANDOM=90;
	public static float HIGHEST_SAFE_VAL_PLAYER=70;
	public static float HIGHEST_SAFE_VAL_CHECKPOINT=80;
	public static float HIGHEST_SAFE_VAL_PACMAN=95;
	public static float Max_DISTANCE_PLAYER_TO_LINE = 210;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static bool m_debugMode=false;
	private static float m_speed=4;
	public static void setSpeed(float speed) {		m_speed = speed;	}
	private static readonly float m_screenWidth=3840;
	private static readonly float m_screenHeight=2160;
	private static float m_blueDuration=6;
	public static void setBlueDuration(float duration) {m_blueDuration = duration;	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	private static float m_minCheckpointDist=400;
	public static float getMinCheckDist() { return m_minCheckpointDist; }
	public static void decMinCheckDist() {		m_minCheckpointDist -= 40;	}
	public static void resetMinCheckDist() {		m_minCheckpointDist = 200;	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool isDebugMode() { return m_debugMode;}
	public static void toggleDebugMode() { m_debugMode=!m_debugMode;}
	public static float getPacManSpeed() { return m_speed;}
	public static void incSpeed() { m_speed+=.1f; }
	public static void decSpeed() { m_speed-=.1f; }
	public static float getScreenWidth() { return m_screenWidth; }
	public static float getScreenHeight() { return m_screenHeight; }
	public static float getBlueDuration() { return m_blueDuration; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static string[] m_colors={"FF0000" ,"00FF00" , "00FFFF" ,"FF00FF" ,"808080" ,"FF8080" ,"8080FF" ,"808000" ,"9a7461" ,"baa7ca", "8A21D9" ,"8C5D00" ,"93bb29",
		"ebd4b0",
		"f87218",
		"00619B",
		"6d3b43"
	};
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static int[] rands={70,85,100,115,130,145};
	static int randIndex=0;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static int m_colorIndex=0;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//public static void resetColorIndex() { m_colorIndex=0; }
	public static void resetRandIndex() { randIndex=0; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Color getNextColor()
	{
		string hex=m_colors[m_colorIndex];
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		m_colorIndex++;
		if (m_colorIndex>=m_colors.Length)
			m_colorIndex=0;
		return new Color32(r,g,b, 255);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static int getRand()
	{
		int rand=rands[randIndex];
		randIndex++;
		if (randIndex==rands.Length)
			randIndex=0;
		return rand;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}






















































































