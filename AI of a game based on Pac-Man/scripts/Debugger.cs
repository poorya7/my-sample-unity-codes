﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Debugger 
{
	static bool m_disabled=true;
	static float m_lastTime=0;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void trackMovement(Vector3 position)
	{
		GameObject bullseys=GameObject.FindGameObjectWithTag("bullsEye");
		position.z=0;
		GameObject bb=(GameObject) GameObject.Instantiate(bullseys , position, Quaternion.identity);
		bb.tag="Tracker";
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void deleteTrackingObjects()
	{
		GameObject[] trackers=GameObject.FindGameObjectsWithTag("Tracker");
		foreach(GameObject t in trackers)
			GameObject.Destroy(t);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updatePositions(Vector3 position1, Vector3 position2, Vector3 position3)
	{
		if (m_disabled)
			return;
		clearPositions();
		GameObject.FindGameObjectWithTag("checkpoint1").transform.position=position1;
		GameObject.FindGameObjectWithTag("checkpoint2").transform.position=position2;
		GameObject.FindGameObjectWithTag("goal").transform.position=position3;

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updatePositions(Vector3 position1, Vector3 position2)
	{
		if (m_disabled)
			return;
		clearPositions();
		GameObject.FindGameObjectWithTag("checkpoint1").transform.position=position1;
		GameObject.FindGameObjectWithTag("checkpoint2").transform.position=position2;
		if (Time.time-m_lastTime>.2f)
		{
			//GameObject.Instantiate(GameObject.FindGameObjectWithTag("mid"), GameObject.FindGameObjectWithTag("PacMan").transform.position, Quaternion.identity);
			m_lastTime=Time.time;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updatePositions(Vector3 position)
	{
		if (m_disabled)
			return;
		clearPositions();
		GameObject.FindGameObjectWithTag("checkpoint1").transform.position=position;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void updateText(string text)
	{
		if (m_disabled)
			return;
		clearTexts();
		GameObject.FindGameObjectWithTag("restartText").GetComponent<Renderer>().GetComponent<GUIText>().text=text;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static void clearPositions()
	{
		if (m_disabled)
			return;
		GameObject.FindGameObjectWithTag("checkpoint1").transform.position=new Vector3(-100,-100,-100);
		GameObject.FindGameObjectWithTag("checkpoint2").transform.position=new Vector3(-100,-100,-100);
		GameObject.FindGameObjectWithTag("goal").transform.position=new Vector3(-100,-100,-100);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static void clearTexts()
	{
		if (m_disabled)
			return;
		GameObject.FindGameObjectWithTag("restartText").GetComponent<Renderer>().GetComponent<GUIText>().text="";
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void drawMiddlePoints(Vector3 point)
	{
		if (m_disabled)
			return;
		clearMiddlePoints();
		GameObject mid=GameObject.FindGameObjectWithTag("mid");
		GameObject newMid=(GameObject) GameObject.Instantiate(mid , point, Quaternion.identity);
		newMid.tag="mid1";

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static void clearMiddlePoints()
	{
		if (m_disabled)
			return;
		GameObject[] mid1s=GameObject.FindGameObjectsWithTag("mid1");
		foreach(GameObject mid1 in mid1s)
			GameObject.Destroy(mid1);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------------------------------------









































































//--------------------------------------------------------------------------------------------------------------------------------------------------