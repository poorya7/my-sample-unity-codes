﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotFactory :MonoBehaviour
{
	//--------------------------------------------------------------
	public static GameObject 	_entity;
	static int 					_factoryListSize;
	//--------------------------------------------------------------
	public static GameObject makeRobot()
	{
		List<float> values=new List<float>();

		for (int i=0; i<Global.LEG_COUNT; i++)
		{
			Leg leg=new Leg();
			leg.init();
			values.AddRange(leg.getValues());
		}
		return makeRobot(values);
	}
	//--------------------------------------------------------------
	public static GameObject makeRobot(List<float> values)
	{
		GameObject robot = Instantiate (Resources.Load ("Robot"), Vector3.zero, Quaternion.identity) as GameObject;
		robot.transform.eulerAngles=new Vector3(0,90,0);
		int counter=0;
		for (int i=0; i<Global.LEG_COUNT; i++)
		{
			GameObject leg=robot.GetComponent<RobotHandler>()._legs[i];
			Leg legScript=leg.GetComponent<Leg>();
			legScript.init(values.GetRange(counter,6));
			counter+=6;
			legScript.activate();
		}
		robot.GetComponent<RobotHandler>().setValues(values);
		robot.GetComponent<RobotHandler>().enabled=true;
		return robot;
	}
	//--------------------------------------------------------------
}
