﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//-----------------------------------------------------------------------------
public class Global 
{
	//-----------------------------------------------------------------------------
	public static readonly int 			LEG_COUNT=2;
	public static readonly Vector2 		FORCE=new Vector2(2100,40000);
	public static readonly Vector2 		FORCE_DURATION =new Vector2(0.6f,1.3f);
	public static readonly Vector2 		FORCE_INTERVAL=new Vector2(0.6f,1.3f);
	//-----------------------------------------------------------------------------
	static List<Vector2>				_ranges;
	//-----------------------------------------------------------------------------
	public static List<Vector2> getRanges()
	{
		if (_ranges==null)
		{
			_ranges=new List<Vector2>();
			
			for (int i=0; i<LEG_COUNT; i++)
			{
				_ranges.Add(FORCE);
				_ranges.Add(new Vector2(-FORCE.y, -FORCE.x));
				_ranges.Add(FORCE_DURATION);
				_ranges.Add(FORCE_DURATION);
				_ranges.Add(FORCE_INTERVAL);
				_ranges.Add(FORCE_INTERVAL);
			}
		}
		return _ranges;
	}
	//-----------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
















































































