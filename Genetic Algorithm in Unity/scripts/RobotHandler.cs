﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotHandler : MonoBehaviour 
{
	//--------------------------------------------------------------
	public GameObject 			_body;
	public GameObject			_line;
	public List<GameObject>		_legs;
	public bool					_stillStanding;
	public init 				_initScript;
	List<float>					_values;
	Vector3						_initialPos;
	float						_aliveTimer;
	float						_totalTime;
	float						_initX;
	float						_initZ;
	bool						_fell;

	public float				_fitnesss;
	//--------------------------------------------------------------
	void Start () 
	{
	}
	//--------------------------------------------------------------
	public void disableLine()
	{
		_line.GetComponent<MeshRenderer> ().enabled = false;
	}
	//--------------------------------------------------------------
	public void enableLine()
	{
		//_line.GetComponent<MeshRenderer> ().enabled = true;
	}
	//--------------------------------------------------------------
	public void setValues(List<float> values)
	{
		_values=values;
	}
	//--------------------------------------------------------------
	public List<float> getValues()
	{
		return _values;
	}
	//--------------------------------------------------------------
	public void setInitScript(init script)
	{
		_initScript=script;
	}
	//--------------------------------------------------------------
	public void init(Vector3 initialPos)
	{
		_fell = false;
		_initialPos = initialPos;
		_initX = _body.transform.position.x;
		_initZ = _body.transform.position.z;
		_stillStanding=true;
		_aliveTimer = Time.time;
	}
	//--------------------------------------------------------------
	void Update () 
	{
		//_fitnessText.text=calcFitness().ToString("F2");

		_fitnesss=calcFitness();

		float time = Time.time - _aliveTimer;
		if (time > _initScript.getAliveTime ())
			die ();

		if (Input.GetKeyUp(KeyCode.N))
			die();
	}
	//--------------------------------------------------------------
	public void setFell()
	{
		_fell = true;
	}
	//--------------------------------------------------------------
	public void die()
	{
		if (_stillStanding)
		{
			_stillStanding = false;
			float fitness = calcFitness ();
			_initScript.setDeathTime (gameObject, _initialPos, fitness, _values);
			GameObject.Destroy (gameObject);
		}
	}
	//--------------------------------------------------------------
	public float calcFitness()
	{
		float currentX = _body.transform.position.x;
		float currentZ = _body.transform.position.z;
		float distance = Mathf.Sqrt (Mathf.Pow (currentX - _initX, 2) + Mathf.Pow (currentZ - _initZ, 2));
		if ( _fell || _body.transform.position.y > 6 || currentX<_initX) // || distance > 90 
			distance = 0.0001f;
		return distance;
	}
	//--------------------------------------------------------------
}
