﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamHandler : MonoBehaviour {

	// Use this for initialization
	float _vAmount = 10;
	float _amount = 50;
	float _rotateAmount = 10;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyUp(KeyCode.P))
			GetComponent<Animator>().enabled=true;
		else if (Input.GetKeyUp(KeyCode.O))
			GetComponent<Animator>().enabled=false;


		float vAmount = Time.deltaTime * _vAmount;
		float amount = Time.deltaTime * _amount;
		float rotateAmount = Time.deltaTime * _rotateAmount;

		if (Input.GetKey (KeyCode.Space))
		{
			if (Input.GetKey(KeyCode.A))
				gameObject.transform.Rotate (0, rotateAmount,0);
			else if (Input.GetKey(KeyCode.D))
				gameObject.transform.Rotate (0, -rotateAmount,0);
			//else
				gameObject.transform.Translate (0, 0, -amount);
		}

		if (Input.GetMouseButton(1))
		{
			if (Input.GetKey(KeyCode.A))
				gameObject.transform.Rotate (0, rotateAmount,0);
			else if (Input.GetKey(KeyCode.D))
				gameObject.transform.Rotate (0, -rotateAmount,0);
			//else
				gameObject.transform.Translate (0, 0, amount);
		}

		if (Input.GetKey (KeyCode.A))
		gameObject.transform.Translate (-amount, 0, 0);

		if (Input.GetKey (KeyCode.D))
		gameObject.transform.Translate (amount,0,0);

		if (Input.GetKey (KeyCode.R))
		gameObject.transform.Translate (0,vAmount,0);

		if (Input.GetKey (KeyCode.T))
		gameObject.transform.Translate (0,-vAmount,0);
		
	}
}
