﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leg :MonoBehaviour
{
	//-----------------------------------------------------------------------------
	public TextMesh		_label;
	HingeJoint			_joint;
	Rigidbody			_legBody;
	bool				_active;
	bool				_lastForceForward;
	bool				_onGround;
	public float				_forwardTorque;
	public float				_backwardTorque;
	float				_forwardTorqueDuration;
	float				_backwardTorqueDuration;
	float				_forwardInterval;
	float				_backwardInterval;
	float				_forceTimer;
	float				_durationTimer;
	bool				_forwardDir;
	bool				_backwardDir;
	float				_vForce=100;
	//-----------------------------------------------------------------------------
	public void init()
	{
		_active=false;
		_lastForceForward=false;
		_onGround=false;
		_forwardTorque = -Random.Range (Global.FORCE.x, Global.FORCE.y);
		_backwardTorque = Random.Range (Global.FORCE.x, Global.FORCE.y);
		_forwardTorqueDuration=	Random.Range(Global.FORCE_DURATION.x, Global.FORCE_DURATION.y);
		_backwardTorqueDuration= Random.Range(Global.FORCE_DURATION.x, Global.FORCE_DURATION.y);
		_forwardInterval = Random.Range (Global.FORCE_INTERVAL.x, Global.FORCE_INTERVAL.y);
		_backwardInterval = Random.Range (Global.FORCE_INTERVAL.x, Global.FORCE_INTERVAL.y);
	}
	//-----------------------------------------------------------------------------
	public void init(List<float> values)
	{
		_active=false;
		_lastForceForward=false;
		_onGround=false;
		int counter=0;
		_forwardTorque = values[counter++];
		_backwardTorque = values[counter++];
		_forwardTorqueDuration = values[counter++];
		_backwardTorqueDuration = values[counter++];
		_forwardInterval = values[counter++];
		_backwardInterval = values[counter++];
	}
	//-----------------------------------------------------------------------------
	public float[] getValues() 
	{
		List<float> vals = new List<float> ();
		vals.Add(_forwardTorque);
		vals.Add(_backwardTorque);
		vals.Add(_forwardTorqueDuration);
		vals.Add(_backwardTorqueDuration);
		vals.Add(_forwardInterval);
		vals.Add(_backwardInterval);
		return vals.ToArray ();
	}
	//-----------------------------------------------------------------------------
	public void setGameobject(GameObject gameObject) 
	{
		
	}
	//-----------------------------------------------------------------------------
	public void activate()
	{
		_legBody = gameObject.GetComponent<Rigidbody>();
		_joint=GetComponent<HingeJoint>();
		_forceTimer=Time.time;
		_active=true;
		float[] f=getValues();
		string output="		";
		foreach (float ff in f)
		{
			output+=ff.ToString("F1")+"  ";
		}
		_label.GetComponent<TextMesh>().text=output;
	}
	//-----------------------------------------------------------------------------
	void OnCollisionEnter (Collision col)
    {
       _onGround=true;
	   //transform.Find("mesh").GetComponent<MeshRenderer>().material.color=Color.red;
    }
	//-----------------------------------------------------------------------------
	void OnCollisionExit (Collision col)
    {
       _onGround=false;
	   //transform.Find("mesh").GetComponent<MeshRenderer>().material.color=Color.white;
    }
	//-----------------------------------------------------------------------------
	void Start()
	{

	}
	//-----------------------------------------------------------------------------
	void Update()
	{
		if (_active )
			applyForces2();
	}
	//-----------------------------------------------------------------------------
	void applyForces()
	{
		if (_lastForceForward==false)
		{
			if (Time.time - _forceTimer > _forwardInterval)
			{
				_forceTimer=Time.time;
				_lastForceForward=true;
				_legBody.AddTorque (_forwardTorque,0,0,ForceMode.Acceleration);
			}
		}
		else
		{
			if (Time.time - _forceTimer > _backwardInterval)
			{
				_forceTimer=Time.time;
				_lastForceForward=false;
				_legBody.AddForce(0,_vForce,0);
				_legBody.AddTorque (_backwardTorque,0,0,ForceMode.Acceleration);
			}
		}
	}
	//-----------------------------------------------------------------------------
	void applyForces2()
	{
		if (_lastForceForward==false)
		{
			if (Time.time - _forceTimer > _forwardInterval)
			{
				_durationTimer=Time.time;
				_forceTimer=Time.time;
				_lastForceForward=true;
				_forwardDir=true;
				_backwardDir=false;
			}
		}
		else
		{
			if (Time.time - _forceTimer > _backwardInterval)
			{
				_durationTimer=Time.time;
				_forceTimer=Time.time;
				_lastForceForward=false;
				_forwardDir=false;
				_backwardDir=true;
				_legBody.AddForce(0,_vForce,0);
			}
		}
		if (_forwardDir && _joint.angle<_joint.limits.max && _onGround)
		{
			if (Time.time-_durationTimer<_forwardTorqueDuration)
			{
				_legBody.AddTorque (_forwardTorque,0,0,ForceMode.Acceleration);
				//transform.Find("mesh").GetComponent<MeshRenderer>().material.color=Color.white;
			}
		}
		else if (_backwardDir && _joint.angle>_joint.limits.min)
		{
			if (Time.time-_durationTimer<_backwardTorqueDuration)
			{
				_legBody.AddTorque (_backwardTorque,0,0,ForceMode.Acceleration);
				//transform.Find("mesh").GetComponent<MeshRenderer>().material.color=Color.red;
			}
		}
	}
	//-----------------------------------------------------------------------------
}
