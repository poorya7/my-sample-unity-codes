﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
//-----------------------------------------------------------------------------
public class GA :MonoBehaviour
{
	//-----------------------------------------------------------------------------
	public delegate void 						ScalingMode(List<Chromosome> list);
	public delegate void 						MutationMode(Chromosome c);
	public delegate List<Chromosome> 			CrossoverMode(Chromosome a, Chromosome b);
	public delegate List<Chromosome>			SelectionMode(int size);
    //-----------------------------------------------------------------------------
	int                       GENERATION_COUNT=70;
	float                     CROSSOVER_RATE=0.7f;
	float                     MUTATION_RATE=0.03f;
	int						  SELECTED_ELITE_COUNT=0;
	int						  TOTAL_ELITE_POP=5;
	//-----------------------------------------------------------------------------
	int                       _popSize;
	int 					  _genNumber = 1;
	init					  _initScript;
    //-----------------------------------------------------------------------------
	MutationMode 	 		  _mutate;
	CrossoverMode 	  		  _crossover;
	SelectionMode	 		  _selection;
	ScalingMode		  		  _scale;
	//-----------------------------------------------------------------------------
	public Text				  _infoText;
	public Text				  _gaText;
	public Text				  _tmpText;
	public Text				  _genSummaryText1;
	public Text				  _genSummaryText2;
	public Text 			  _fullInfoText;
	public Text				  _legsText;

	public TextMesh			  _gaNumtext;
	public TextMesh			  _maxDistText;
    //-----------------------------------------------------------------------------
	public IEnumerator init(init initScript, Dictionary<float, List<float>> allValues)
    {
		_popSize = allValues.Count;
		_initScript = initScript;
		TOTAL_ELITE_POP = Mathf.Min (_popSize, 5);
		//------------------------------------------------------SET MODES----------
		_crossover=Crossover.SinglePoint;
		_mutate=Mutation.customMutation;
		_selection = Selection.RW;
		_scale = Scaling.RankScaling;
		//-------------------------------------------------------------------------
        Crossover.setCrossoverRate(CROSSOVER_RATE);
        Mutation.setMutationRate(MUTATION_RATE);
		Population.init (allValues);
		Scaling.init (allValues.Count * 2);
		countUniques();
		_gaNumtext.text="Creating next generation...";
		_maxDistText.GetComponent<MeshRenderer>().enabled=false;
		yield return StartCoroutine (makeNextGenChromosomes ());
		
		makeNextGenRobots ();
		_gaNumtext.text="GENERATION "+_genNumber;
		if (_gaNumtext.GetComponent<MeshRenderer>().enabled==true)
			_maxDistText.GetComponent<MeshRenderer>().enabled=true;
    }
	//-----------------------------------------------------------------------------
	void countUniques()
	{
		List<Chromosome> chromosomes= Population.getPopulation();
		Dictionary<List<float>, int> countByValues=new Dictionary<List<float>, int>();
		for (int i=0; i<chromosomes.Count; i++)
		{
			List<float> values1=chromosomes[i].getValues();
			int count=0;
			List<float> values2=null;
			foreach(KeyValuePair<List<float>, int> pair in countByValues)
			{
				values2=pair.Key;
				if (valsEqual(values1, values2))
				{
					count=pair.Value+1;
					break;
				}
			}
			if (count>1)
			{
				countByValues.Remove(values2);
				countByValues.Add(new List<float>(values1), count);
			}
			else
				countByValues.Add(new List<float>(values1),1);
		}

		string output="";
		foreach(KeyValuePair<List<float>, int> pair in countByValues)
			output+=pair.Value+" ";
		Debug.Log(output);
	}
	//-----------------------------------------------------------------------------
	string printV(List<float> vals)
	{
		string output="";
		foreach(float f in vals)
			output+=f+" ";
		return output;
	}
	//-----------------------------------------------------------------------------
	bool valsEqual(List<float> values1, List<float> values2)
	{
		for (int i=0; i<values1.Count; i++)
			if (values1[i]!=values2[i])
			 	return false;
		return true;
	}
	//-----------------------------------------------------------------------------
	public int getGenNumber()
	{
		return _genNumber;
	}
	//-----------------------------------------------------------------------------
	void makeNextGenRobots()
	{
		List<List<float>> populationValues = Population.getPopulationValues ();
		_initScript.createNewRobots (populationValues);
	}
    //-----------------------------------------------------------------------------
    IEnumerator makeNextGenChromosomes()
    {
		//_tmpText.text = "Creating next generation...";
		updateUI ();
		_scale (Population.getPopulation ());
		// DON'T SELECT ELITES FOR THIS PROJECT
		//List<Chromosome> elits = Population.getElites (SELECTED_ELITE_COUNT, TOTAL_ELITE_POP);
		List<Chromosome> newPopulation = _selection (_popSize);// - elits.Count);

		//newPopulation.AddRange (elits);
		yield return StartCoroutine (makeNewBabies (newPopulation));
		Population.reset (newPopulation);
        newPopulation.Clear();
		_genNumber++;
		//_tmpText.text = "";
    }
	//-----------------------------------------------------------------------------
	void Update()
	{
		if (Input.GetKeyUp(KeyCode.I))
			_gaNumtext.GetComponent<MeshRenderer>().enabled=true;
		/*if (_genNumber > 1)
			_gaText.text = "Generation : " + _genNumber;*/
	}
	//-----------------------------------------------------------------------------
	void updateUI()
	{
		float sum = 0;
		float min = float.MaxValue;
		float max = float.MinValue;
		List<Chromosome> population = Population.getPopulation ();
		for (int i=0; i<population.Count; i++)
		{
			Chromosome c = population [i];
			if (c.getFitness () > max)
				max = c.getFitness ();
			if (c.getFitness () < min)
				min = c.getFitness ();
			sum += c.getFitness ();
		}
		sum /= Population.getPopulation ().Count;

		/*if (_genSummaryText1.text.Length > 130)
		{
			_genSummaryText1.text = "";
			_genSummaryText2.text = "";
		}
		_genSummaryText1.text += "\nGeneration " + _genNumber.ToString (); 
		_genSummaryText2.text += "\nAverage : " + sum.ToString ("F2");*/
	}
	//-----------------------------------------------------------------------------
	IEnumerator makeNewBabies(List<Chromosome> population)
	{
		List<Chromosome> newBabies = new List<Chromosome> ();
		for (int i = 0; i < population.Count; i += 2)
		{
			Chromosome a = population.ElementAt (i);
			Chromosome b = population.ElementAt (i+1);
			List<Chromosome> children = _crossover(a, b);
			foreach (Chromosome child in children)
			{
				_mutate(child);
				newBabies.Add(child);
				yield return null;
			}
		}
		population.Clear ();
		population.AddRange (newBabies);
	}
    //-----------------------------------------------------------------------------

}
