﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
//-----------------------------------------------------------------------------
public class Scaling : MonoBehaviour 
{
	//-----------------------------------------------------------------------------
	static readonly float		BOLTZMANN_DEC_RATE=0.05f;
	static readonly float 		BOLTZMANN_MIN = 1;
	static float 				_boltzmannTemperature;
	//-----------------------------------------------------------------------------
	public static void init(float boltzmannTemperature)
	{
		_boltzmannTemperature = boltzmannTemperature;
	}
	//-----------------------------------------------------------------------------
	public static void RankScaling(List<Chromosome> population)
	{
		population.Sort ();

		float fitness = 1;
		foreach (Chromosome c in population) 
		{
			c.setFitness (fitness);
			fitness += 1f;
		}
	}
	//-----------------------------------------------------------------------------
	public static void SigmaScaling(List<Chromosome> population)
	{
		float average = 0;
		foreach (Chromosome c in population)
			average += c.getFitness ();
		average /= population.Count;
		float deviation = calcStandardDeviation (population, average);
		foreach (Chromosome c in population)
			c.setFitness ((c.getFitness () - average) / (2 * deviation));
		Population.positifyFitnesses ();
	}
	//-----------------------------------------------------------------------------
	public static void BoltzmannScaling(List<Chromosome> population)
	{
		_boltzmannTemperature -= BOLTZMANN_DEC_RATE;
		if (_boltzmannTemperature < BOLTZMANN_MIN)
			_boltzmannTemperature = BOLTZMANN_MIN;
		float averageFit = calcAverageFitness (population);
		float divider = calcAverageFitness (population) / _boltzmannTemperature;
		foreach (Chromosome c in population)
		{
			float oldFitness = c.getFitness ();
			float newFitness = (oldFitness / _boltzmannTemperature) / divider;
			c.setFitness (newFitness);
		}
	}
	//-----------------------------------------------------------------------------
	static float calcStandardDeviation(List<Chromosome> population, float average)
	{
		float variance = 0;
		foreach (Chromosome c in population)
		{
			variance += Mathf.Pow (((float) (c.getFitness () - average)), 2);
		}
		variance /= population.Count;
		return (float) Mathf.Sqrt ((float) variance);
	}
	//-----------------------------------------------------------------------------
	static float calcAverageFitness(List<Chromosome> population)
	{
		float average = 0;
		foreach (Chromosome c in population)
		{
			average += c.getFitness ();
		}
		return average / population.Count;
	}
	//-----------------------------------------------------------------------------
}
