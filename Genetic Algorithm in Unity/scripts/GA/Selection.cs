﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//-----------------------------------------------------------------------------
public class Selection : MonoBehaviour 
{
	//-----------------------------------------------------------------------------
	public static List<Chromosome> RW(int size)
	{
		//ROULETTE WHEEL
		List<Chromosome> newPopulation = new List<Chromosome>();
		for (int i = 0; i < size; i++) 
		{
			Chromosome c = chooseOneRW (Population.getPopulation ());
			newPopulation.Add (c);
		}
		return newPopulation;
	}
	//-----------------------------------------------------------------------------
	static Chromosome chooseOneRW(List<Chromosome> population)
	{
		double r = Random.Range (0, (float) calcTotalFitness (population));
		double slice = 0;
		int selectedChromosome = 0;
		for (int i = 0; i <population.Count; i++)
		{
			slice += population [i].getFitness ();
			if (slice>r)
			{
				selectedChromosome = i;
				break;
			}
		}
		return population [selectedChromosome];
	}
	//-----------------------------------------------------------------------------
	public static List<Chromosome> SUS(int size)
	{
		//STOCHASTIC UNIVERSAL SAMPLING
		List<Chromosome> population=Population.getPopulation();
		List<Chromosome> newPopulation = new List<Chromosome> ();
		double totalFitness = calcTotalFitness (population);
		double sectionSize = totalFitness / size;
		double pointer = Random.Range (0, (float) sectionSize);
		int index = 0;
		double sum = population.ElementAt (0).getFitness ();
		while (newPopulation.Count < size) 
		{
			while (sum < pointer) 
			{
				index++;
				sum += population.ElementAt (index).getFitness ();
			}
			newPopulation.Add (population.ElementAt (index));
			pointer += sectionSize;
		}
		return newPopulation;
	}
	//-----------------------------------------------------------------------------
	public static List<Chromosome> Tournament(int size)
	{
		List<Chromosome> population = Population.getPopulation ();
		List<Chromosome> newPopulation = new List<Chromosome> ();
		while (newPopulation.Count < size) 
		{
			double bestFitnessSoFar = 0;
			Chromosome chosenOne = null;
			int n = 10;
			for (int i = 0; i < n; i++) 
			{
				int index = Random.Range (0, population.Count);
				if (population.ElementAt (index).getFitness () >= bestFitnessSoFar) 
				{
					chosenOne = population.ElementAt (index);
					bestFitnessSoFar = chosenOne.getFitness ();
				} 
			}
			newPopulation.Add (chosenOne);
		}
		return newPopulation;
	}
	//-----------------------------------------------------------------------------
	static double calcTotalFitness(List<Chromosome> population)
	{
		double total=0;
		foreach (Chromosome c in population)
		{
			total+=c.getFitness();
		}
		return total;
	}
	//-----------------------------------------------------------------------------
}
