﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//-----------------------------------------------------------------------------
public class Mutation : MonoBehaviour 
{
    //-----------------------------------------------------------------------------
    static float                _mutationRate;
    //-----------------------------------------------------------------------------
    public static void setMutationRate(float rate)
    {
        _mutationRate=rate;
    }
    //-----------------------------------------------------------------------------
    public static void Mutate(Chromosome c)
    {
        //NORMAL SWAPPING OF TWO BITS
        float r = Random.Range(0, 1f);
        if (r < _mutationRate)
        {
            List<float> values = c.getValues();
            Vector2 randoms=getRandomRange(0,values.Count,-1);
			float token = values[(int)randoms.x];
            values[(int)randoms.x] = values[(int)randoms.y];
            values[(int)randoms.y] = token;
        }
    }
    //-----------------------------------------------------------------------------
    public static void SM(Chromosome c)
    {
		//SCRAMBLE MUTATION
        float r = Random.Range(0, 1f);
        if (r < _mutationRate)
        {
            List<float> values = c.getValues();
            Vector2 randoms=getRandomRange(0,values.Count,3);
            shuffleBits(values,(int)randoms.x,(int)randoms.y);
        }
    }
	//-----------------------------------------------------------------------------
	public static void customMutation(Chromosome c)
	{
		float r = Random.Range(0, 1f);
		if (r < _mutationRate)
		{
			List<float> values = c.getValues();
			int pos = Random.Range (0, values.Count);
            Vector2 range=Global.getRanges()[pos];
			float newValue = Random.Range(range.x, range.y);
            values[pos]=newValue;
		}
	}
    //-----------------------------------------------------------------------------
    public static void IM(Chromosome c)
    {
		//INSERTION MUTATION
        float r = Random.Range(0, 1f);
        if (r < _mutationRate)
        {
            List<float> values = c.getValues();
			Vector2 randoms = getRandomRange (0, values.Count, -1);
            float token = values[(int)randoms.x];
            values.RemoveAt((int)randoms.x);
            values.Insert((int)randoms.y, token);
        }
    }
    //-----------------------------------------------------------------------------
    public static void DIVM(Chromosome c)
    {
		//DISPLACED INVERSION MUTATION
        List<float> values = c.getValues();
        Vector2 randoms = getRandomRange(0, values.Count, 3);
        reverseBits(values, (int)randoms.x, (int)randoms.y);
		List<float> reversed = new List<float>();
        for (int i = (int)randoms.x; i < (int)randoms.y; i++)
            reversed.Add(values[i]);
        int k = Random.Range(0, values.Count - (int)(randoms.y - randoms.x));
        insertBits(values, reversed, k);
    }
    //-----------------------------------------------------------------------------
    public static void IVM(Chromosome c)
    {
		//INVERSION MUTATION
        float r = Random.Range(0, 1f);
        if (r < _mutationRate)
        {
            List<float> values = c.getValues();
            Vector2 randoms = getRandomRange(0, values.Count, 3);
            reverseBits(values, (int)randoms.x, (int)randoms.y);
        }
    }
    //-----------------------------------------------------------------------------
    public static void DM(Chromosome c)
    {
		//DISPLACEMENT MUTATION
        float r = Random.Range(0, 1f);
        if (r < _mutationRate)
        {
            List<float> values = c.getValues();
            Vector2 randoms=getRandomRange(0,values.Count);
			List<float> newList = new List<float>();
            for (int i = (int)randoms.x; i < (int)randoms.y; i++)
            {
                newList.Add(values[i]);
            }
			int k = Random.Range(0,  values.Count - (int)(randoms.y-randoms.x));
            insertBits(values,newList,k);
        }
    }
    //-----------------------------------------------------------------------------
    public static Vector2 getRandomRange(int startIn, int endEx, int maxDistance = 0)
    {
		//CREATES TWO RANDOM NUMBERS BETWEEN STARTIN AND ENDEX,
		//WITH THE DISTANCE OF MAXDISTANCE
        int n1=0;
        int n2=0;
        if (maxDistance == -1)
        {
            while (n1==n2)
            {
                n1 = Random.Range(startIn, endEx);
                n2 = Random.Range(startIn , endEx);
            }
        }
        else
        {
            n2 = -1;
            while (n2 < n1 + maxDistance)
            {
                n1 = Random.Range(startIn, endEx - 1);
                n2 = Random.Range(n1 + 1, endEx);
            }
        }
        return new Vector2(n1,n2);
    }
    //-----------------------------------------------------------------------------
	static void insertBits(List<float> bits, List<float> toInsert, int insertionPoint)
    {
        int removeIndexStart=bits.IndexOf(toInsert[0]);
        bits.RemoveRange(removeIndexStart,toInsert.Count);
        bits.InsertRange(insertionPoint, toInsert);
    }
    //-----------------------------------------------------------------------------
    static void reverseBits(List<float> bits, int startIn, int endEx)
    {
        for (int i = startIn; i < startIn + (endEx - startIn) / 2; i++)
        {
            int upperIndex = endEx + startIn - i - 1;
            float token = bits[upperIndex];
            bits[upperIndex] = bits[i];
            bits[i] = token;
        }
    }
    //-----------------------------------------------------------------------------
    static void shuffleBits(List<float> bits, int startIn, int endEx)
    {
        if (endEx == startIn + 2)
        {
            float token = bits[endEx - 1];
            bits[endEx - 1] = bits[startIn];
            bits[startIn] = token;
        }
        else
        {
            for (int i = startIn; i < endEx; i++)
            {
                int k = Random.Range(startIn, endEx);
                while (k == i)
                    k = Random.Range(startIn, endEx);
                float token = bits[i];
                bits[i] = bits[k];
                bits[k] = token;
            }
        }
    }
    //-----------------------------------------------------------------------------
}
