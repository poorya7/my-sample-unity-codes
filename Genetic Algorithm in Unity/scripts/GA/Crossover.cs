﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//-----------------------------------------------------------------------------
public class Crossover : MonoBehaviour 
{
    //-----------------------------------------------------------------------------
    static float                _crossoverRate;
    //-----------------------------------------------------------------------------
    public static void setCrossoverRate(float rate)
    {
        _crossoverRate=rate;
    }
	//-----------------------------------------------------------------------------
	public static List<Chromosome> SinglePoint(Chromosome a, Chromosome b)
	{
		float rand = Random.Range(0, 1f);
		if (rand > _crossoverRate || equal(a, b))
		{
			return copyInput (a, b);
		}
		else
		{
			List<Chromosome> output = new List<Chromosome>();
			int random = Random.Range (1, a.getValues ().Count - 1);
			List<float> valuesA = a.getValues ();
			List<float> valuesB = b.getValues ();
			List<float> baby1 = new List<float> (valuesA);
			List<float> baby2 = new List<float> (valuesB);
			for (int i = 0; i < random; i++)
			{
				baby1 [i] = valuesB [i];
				baby2 [i] = valuesA [i];
			}
			printValues (valuesA);
			printValues (valuesB);
			printValues (baby1);
			printValues (baby2);
			output.Add(new Chromosome(baby1));
			output.Add(new Chromosome(baby2));
			return output;
		}
	}
	//-----------------------------------------------------------------------------
	static void printValues(List<float> values)
	{
		return;
		string output = "";
		foreach (float val in values)
			output += val.ToString ("0.#") + "  ";
		Debug.Log (output);
	}
    //-----------------------------------------------------------------------------
    public static List<Chromosome> PMX(Chromosome a, Chromosome b)
    {
        //PARTIALLY MAPPED CROSSOVER
        float rand = Random.Range(0, 1f);
        if (rand > _crossoverRate || equal(a, b))
        {
			return copyInput (a, b);
        }
        else
        {
			List<Chromosome> output = new List<Chromosome>();
            Vector2 randoms = Mutation.getRandomRange(0, a.getValues().Count+1);
            int n1 = (int)randoms.x;
            int n2 = (int)randoms.y;
			List<float> baby1 = new List<float>(a.getValues());
			List<float> baby2 = new List<float>(b.getValues());
			List<float> replacement1 = new List<float>();
			List<float> replacement2 = new List<float>();
            for (int i = n1; i < n2; i++)
            {
                replacement1.Add(baby2[i]);
                replacement2.Add(baby1[i]);
            }
            swap(baby1, replacement1, n1);
            swap(baby2, replacement2, n1);
            output.Add(new Chromosome(baby1));
            output.Add(new Chromosome(baby2));
            return output;
        }
        return null;
    }
    //-----------------------------------------------------------------------------
    public static List<Chromosome> OBX(Chromosome a, Chromosome b)
    {
        //ORDER-BASED CROSSOVER
		float rand = Random.Range(0, 1f);
		if (rand > _crossoverRate || equal (a, b)) 
		{
			return copyInput (a, b);
		} 
		else 
		{
			List<Chromosome> output = new List<Chromosome>();
			List<float> values1 = a.getValues ();
			List<float> values2 = b.getValues ();

			List<float> baby1 = new List<float> (values1);
			List<float> baby2 = new List<float> (values2);

			List<int> indices1 = getSeparateRandomsIndices (baby1.Count, 3);
			forceOrder (values1, baby2, indices1);
			forceOrder (values2, baby1, indices1);
			output.Add(new Chromosome(baby1));
			output.Add(new Chromosome(baby2));
			return output;
		}
    }
	//-----------------------------------------------------------------------------
	public static List<Chromosome> PBX(Chromosome a, Chromosome b)
	{
		//POSITION-BASED CROSSOVER
		float rand = Random.Range(0, 1f);
		if (rand > _crossoverRate || equal (a, b)) 
		{
			return copyInput (a, b);
		} 
		else 
		{
			List<Chromosome> output = new List<Chromosome>();
			List<float> values1 = a.getValues ();
			List<float> values2 = b.getValues ();
			List<float> baby1 = new List<float> (values1);
			List<float> baby2 = new List<float> (values2);
			List<int> randomIndices = getSeparateRandomsIndices (baby1.Count, 3);
			forcePosition (values1, baby2, randomIndices);
			forcePosition (values2, baby1, randomIndices);
			output.Add(new Chromosome(baby1));
			output.Add(new Chromosome(baby2));
			return output;
		}
	}
	//-----------------------------------------------------------------------------
	static void forcePosition(List<float> babyFrom, List<float> babyTo, List<int> randomIndices)
	{
		List<float> tmp = new List<float> ();
		for (int i = 0; i < babyTo.Count; i++)
			tmp.Add (-1);
		foreach (int index in randomIndices) 
			tmp [index] = babyFrom [index];
		int tmpCounter = 0;
		foreach(int i in babyTo)
		{
			if (!tmp.Contains (i))
			{
				while (tmpCounter < tmp.Count  && tmp [tmpCounter] != -1)
					tmpCounter++;

				tmp [tmpCounter] = i;
				tmpCounter++;
			}
		}
		for (int i = 0; i < babyTo.Count; i++)
			babyTo [i] = tmp [i];
	}
	//-----------------------------------------------------------------------------
	static void forceOrder(List<float> babyFrom, List<float> babyTo, List<int> indicesInBabyFrom)
	{
		List<float> numbersInBabyFrom = new List<float> ();
		foreach (int indexInBabyFrom in indicesInBabyFrom) 
		{
			numbersInBabyFrom.Add (babyFrom [indexInBabyFrom]);
		}
		indicesInBabyFrom.Sort ();  
		Dictionary<int,float> numbersByIndexInBabyTo = new Dictionary<int, float> ();

		foreach(float numberInBabyFrom in numbersInBabyFrom)
		{
			int indexInBabyTo = babyTo.IndexOf (numberInBabyFrom);
			numbersByIndexInBabyTo [indexInBabyTo] = numberInBabyFrom;
		}
		var sortedByKey = from entry in numbersByIndexInBabyTo orderby entry.Key ascending select entry;
		for (int i = 0; i < numbersInBabyFrom.Count; i++) 
		{
			int index = sortedByKey.ElementAt (i).Key;
			float val = babyFrom [indicesInBabyFrom [i]];
			babyTo [index] = val;
		}
	}
	//-----------------------------------------------------------------------------
	public static List<Chromosome> copyInput(Chromosome a, Chromosome b)
	{
		List<Chromosome> output = new List<Chromosome>();
		output.Add (new Chromosome (new List<float> (a.getValues ())));
		output.Add (new Chromosome (new List<float> (b.getValues ())));
		return output;
	}
	//-----------------------------------------------------------------------------
	static void printList(List<int> lst)
	{	
		string output = "";
		foreach (int n in lst)
			output += n + " ";
		Debug.Log (output);
	}	
    //-----------------------------------------------------------------------------
    static bool equal(Chromosome a, Chromosome b)
    {
		List<float> valuesA = a.getValues ();
		List<float> valuesB = b.getValues ();
		for (int i=0; i<valuesA.Count; i++)
        {
			if (valuesA [i] != valuesB [i])
                return false;
        }
        return true;
    }
    //-----------------------------------------------------------------------------
	static void swap(List<float> baby, List<float> replacements, int startPos)
    {
        for (int i = 0; i < replacements.Count; i++)
        {   
            baby[baby.IndexOf(replacements[i])]=baby[startPos+i];
            baby[startPos + i] = replacements[i];
        }
    }
    //-----------------------------------------------------------------------------
	static List<int> getSeparateRandomsIndices(int listCount, int outputCount)
    {
        List<int> output = new List<int>();
        int counter = 0;
        while (counter < outputCount)
        {
			int selected=Random.Range(0, listCount);
            while (output.Contains(selected))
				selected=Random.Range(0, listCount);
            output.Add(selected);
            counter++;
        }
        return output;
    }
    //-----------------------------------------------------------------------------
}