﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//-----------------------------------------------------------------------------
public class Population : MonoBehaviour 
{
	//-----------------------------------------------------------------------------
	public static List<Chromosome>      	_population;  
	//-----------------------------------------------------------------------------
	public static void init(Dictionary<float, List<float>> allValues)
	{
		_population = new List<Chromosome>();
		foreach(KeyValuePair<float, List<float>> entry in allValues)
		{
			Chromosome c = new Chromosome (entry.Value);
			c.setFitness (entry.Key);
			_population.Add (c);
		}
	}
	//-----------------------------------------------------------------------------
	public static void init(int size, int chromoLen)
	{
		_population = new List<Chromosome>();
		for (int i = 0; i < size; i++)
		{
			Chromosome c = new Chromosome(chromoLen);
			_population.Add(c);
		}
	}
	//-----------------------------------------------------------------------------
	public static void reset(List<Chromosome> newPopulation)
	{
		_population.Clear ();
		_population.AddRange (newPopulation);
	}
	//-----------------------------------------------------------------------------
	public static List<List<float>> getPopulationValues()
	{
		List<List<float>> values = new List<List<float>> ();
		foreach (Chromosome c in _population)
			values.Add (c.getValues ());
		return values;
	}
	//-----------------------------------------------------------------------------
	public static List<Chromosome> getPopulation()
	{
		return _population;
	}
	//-----------------------------------------------------------------------------
	public static List<Chromosome> getElites(int selectedElitesCount, int totalElitesCount)
	{
		List<Chromosome> totalElites = new List<Chromosome> ();
		Dictionary<Chromosome,float> chromoByFitness = new Dictionary<Chromosome, float> ();
		foreach (Chromosome c in _population) 
			chromoByFitness [c] = c.getFitness ();
		var sortedDict = from entry in chromoByFitness orderby entry.Value descending select entry;
		for (int i = 0; i < totalElitesCount; i++) 
			totalElites.Add (sortedDict.ElementAt(i).Key);
		List<Chromosome> selectedElites = new List<Chromosome> ();
		while (selectedElites.Count < selectedElitesCount) 
		{
			int r = Random.Range (0, totalElites.Count);
			Chromosome selected = totalElites.ElementAt (r);
			if (!selectedElites.Contains (selected))
				selectedElites.Add (selected);
		}
		return selectedElites;
	}
	//-----------------------------------------------------------------------------
	public static void positifyFitnesses()
	{
		float smallest = _population.ElementAt (0).getFitness ();
		for (int i=1; i<_population.Count; i++)
		{
			if (_population.ElementAt (i).getFitness () < smallest)
				smallest = _population.ElementAt (i).getFitness ();
		}
		foreach (Chromosome c in _population)
			c.setFitness (c.getFitness ()-smallest);
	}
	//-----------------------------------------------------------------------------
	static Chromosome getBestChromosome()
	{
		float max=-1;
		Chromosome best = null;
		foreach (Chromosome c in _population)
		{
			float fitness = c.getFitness();
			if (fitness > max)
			{
				max = fitness;
				best = c;
			}
		}
		return best;
	}
	//-----------------------------------------------------------------------------
	public static float getBestFitness()
	{
		Chromosome c = getBestChromosome ();
		return c.getFitness ();
	}
	//-----------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------