﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
//-----------------------------------------------------------------------------
public class Chromosome :IComparable<Chromosome>
{
    //-----------------------------------------------------------------------------
	protected List<float>	_values;
	float					_fitness;
    //-----------------------------------------------------------------------------
    public Chromosome()
    {
    }
    //-----------------------------------------------------------------------------
    public Chromosome(int size)
    {
		_values = new List<float> ();
        for (int i = 0; i < size; i++)
        {
			float value=-1;
            do
            {
				value=UnityEngine.Random.Range(0,size);
            } while (_values.Contains(value));
			_values.Add(value);
        }
    }
	//-----------------------------------------------------------------------------
	public Chromosome(List<float> values)
	{
		_values = values;
	}
	//-----------------------------------------------------------------------------
	public Chromosome(float[] values)
	{
		_values = new List<float> (values);
	}
	//-----------------------------------------------------------------------------
	public int CompareTo(Chromosome obj)
	{
		return this._fitness.CompareTo (obj.getFitness ());
	}
	//-----------------------------------------------------------------------------
	public List<float> getValues()
	{
		return _values;
	}
	//-----------------------------------------------------------------------------
	public void setFitness(float fitness)
	{
		this._fitness=fitness;
	}
	//-----------------------------------------------------------------------------
	public float getFitness()
	{
		return _fitness;
	}
	//-----------------------------------------------------------------------------
	public string str()
	{
		string output="";
		foreach(float val in _values)
			output+=val.ToString("F1")+"  ";
		return output;
	}
    //-----------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------