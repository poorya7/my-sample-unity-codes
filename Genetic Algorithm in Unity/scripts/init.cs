﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
//--------------------------------------------------------------
public class init : MonoBehaviour 
{
	//--------------------------------------------------------------
	public GA 						_gaScript;
	public Text						_timerText;
	public TextMesh					_maxDistText;
	public TextMesh					_bestVals;
	public bool						_raceMode;
	//--------------------------------------------------------------
	int                      		POP_SIZE=96;
	public float					ALIVE_TIME=40;
	//--------------------------------------------------------------
	Dictionary<float, List<float>>	_allValuesByFitness;
	List<GameObject>				_entities;
	List<Vector3>					_positions;
	GameObject						_RobotParent;
	List<Vector3>					_freeSlots;
	bool							_initPopCreated;
	int 							_deadCount;
	int 							_aliveCount;
	float							_generationTimer;
	float							_legCount;
	float							_lastMaxDist;
	//--------------------------------------------------------------
	void Start () 
	{
		_lastMaxDist=float.MinValue;
		_freeSlots=new List<Vector3>();
		bool sw=true;
		for (int z=425; z>-75; z-=80)
		{
			for (int x=-120; x<300; x+=30)
			{
				if (false)//sw==true)
					_freeSlots.Add(new Vector3(x,2.5f,z+15));
				else
					_freeSlots.Add(new Vector3(x,2.5f,z));
				sw=!sw;
			}
		}
		_positions = new List<Vector3> ();
		_RobotParent = new GameObject ("Entities");
		_entities = new List<GameObject> ();
		_allValuesByFitness= new Dictionary<float, List<float>>();
		if (!_raceMode)
			createNewRobots ();
		else
			race();
	}
	//--------------------------------------------------------------
	void race()
	{
		String bestVals=_bestVals.text;
		string[] list= bestVals.Split('r');
		foreach (string vals in list)
		{
			if (vals.Trim().Length==0)
				continue;
			string[] row=vals.Split(' ');
			List<float> values=new List<float>();
			int legCount=int.Parse(row[0]);
			for (int i=1; i<row.Length; i++)
			{
				if (row[i].Trim().Length>0)
					values.Add(float.Parse(row[i]));
			}
			makeRobot(values,legCount);
		}
	}
	//--------------------------------------------------------------
	void createNewRobots()
	{
		_lastMaxDist=float.MinValue;
		_entities.Clear ();
		_allValuesByFitness.Clear();
		_positions.Clear();
		_initPopCreated = false;
		_deadCount = 0;
		for (int i = 0; i < POP_SIZE; i++)
			makeRobot ();
	}
	//--------------------------------------------------------------
	public void createNewRobots(List<List<float>> allValues)
	{
		_lastMaxDist=float.MinValue;
		_entities.Clear ();
		_allValuesByFitness.Clear();
		_positions.Clear();
		_initPopCreated = false;
		_deadCount = 0;
		for (int i = 0; i < POP_SIZE; i++)
			makeRobot (allValues.ElementAt(i), Global.LEG_COUNT);
	}
	//-----------------------------------------------------------------------------
	float getMaxDistance()
	{
		float max = float.MinValue;
		foreach (GameObject Robot in _entities)
		{
			float dist = Robot.GetComponent<RobotHandler> ().calcFitness ();
			if (dist > max )//&& dist<200)
				max = dist;
		}
		return max;
	}
	//--------------------------------------------------------------
	public float getAliveTime()
	{
		return ALIVE_TIME;
	}
	//--------------------------------------------------------------
	public void setDeathTime(GameObject Robot, Vector3 initialPosition, float fitness, List<float> values)
	{
		_entities.Remove (Robot);
		_aliveCount--;
		_deadCount++;
		_positions.Remove (initialPosition);
		while (_allValuesByFitness.ContainsKey (fitness))
			fitness += 0.0001f;
		_allValuesByFitness.Add (fitness, values);
	}
	//--------------------------------------------------------------
	void makeRobot()
	{
		Vector3 pos = getNextAvailablePos ();
		if (pos!=new Vector3(-1,-1,-1))
		{
			_positions.Add(pos);
			GameObject Robot = RobotFactory.makeRobot ();
			Robot.transform.SetParent (_RobotParent.transform, false);
			Robot.transform.position = pos;
			Robot.GetComponent<RobotHandler> ().setInitScript (this);
			Robot.GetComponent<RobotHandler> ().init (pos);
			_aliveCount++;
			_entities.Add (Robot);
			_generationTimer=Time.time;
		}
	}
	//--------------------------------------------------------------
	void makeRobot(List<float> values, int legCount)
	{
		Vector3 pos = getNextAvailablePos ();
		if (pos!=new Vector3(-1,-1,-1))
		{
			_positions.Add(pos);
			GameObject Robot = RobotFactory.makeRobot (values);
			Robot.transform.SetParent (_RobotParent.transform, false);
			Robot.transform.position = pos;
			Robot.GetComponent<RobotHandler> ().setInitScript (this);
			Robot.GetComponent<RobotHandler> ().init (pos);
			_aliveCount++;
			_entities.Add (Robot);
			_generationTimer=Time.time;
		}
	}
	//--------------------------------------------------------------
	Vector3 getNextAvailablePos()
	{
		foreach(Vector3 slot in _freeSlots)
			if(!_positions.Contains(slot))
				return slot;
		return new Vector3(-1,-1,-1);
				
	}
	//--------------------------------------------------------------
	void Update () 
	{
		if (_raceMode)
			return;
		if (Input.GetKeyUp(KeyCode.B))
			saveBestChromosome();

		if (Input.GetKeyUp(KeyCode.I))
			_maxDistText.GetComponent<MeshRenderer>().enabled=true;

		if (_initPopCreated == false)
		{
			if (_aliveCount+ _deadCount < POP_SIZE)
			{
				if (_aliveCount < POP_SIZE)
					makeRobot ();
			}
			else 
			{
				if (_deadCount==POP_SIZE)
				{
					_initPopCreated = true;
					StartCoroutine (_gaScript.init (this, _allValuesByFitness));
				}
			}
		}
		if (_entities.Count>0)
			updateTop3 ();
		updateTimer();
		float maxDistance = getMaxDistance ();
		if (maxDistance>0 && maxDistance>_lastMaxDist)
		{
			_lastMaxDist=maxDistance;
			_maxDistText.text = "Max distance : "+maxDistance.ToString ("F2");
		}
	}
	//--------------------------------------------------------------
	void saveBestChromosome()
	{
		var sortedLiveList = _entities.OrderBy (go => go.GetComponent<RobotHandler> ().calcFitness ()).ToList ();
		float bestLiveFitness=sortedLiveList[sortedLiveList.Count-1].GetComponent<RobotHandler>().calcFitness();
		List<float> bestValues=sortedLiveList[sortedLiveList.Count-1].GetComponent<RobotHandler>().getValues();

		var sortedDeadList = _allValuesByFitness.OrderBy (go => go.Key).ToList();
		if (sortedDeadList.Count>0)
		{
			float bestDeadFitness=sortedDeadList[sortedDeadList.Count-1].Key;
			if (bestDeadFitness>bestLiveFitness)
				bestValues=sortedDeadList[sortedDeadList.Count-1].Value;
		}
		String output="";
		foreach(float f in bestValues)
			output+=f.ToString("F2")+" ";
		_bestVals.text+="r"+Global.LEG_COUNT+" "+output;
	}
	//--------------------------------------------------------------
	void updateTimer()
	{
		float c=Time.time - _generationTimer;
		float remainingTime = ALIVE_TIME - (Time.time - _generationTimer);
		if (remainingTime < 0)
			remainingTime = 0;
		_timerText.text = remainingTime.ToString ("F2");
	}
	//--------------------------------------------------------------
	void updateTop3()
	{
		var sortedList = _entities.OrderBy (go => go.GetComponent<RobotHandler> ().calcFitness ()).ToList ();
		foreach (GameObject Robot in sortedList)
			Robot.GetComponent<RobotHandler> ().disableLine ();
		if (sortedList.Count>3)
			for (int i = sortedList.Count - 1; i > sortedList.Count - 4; i--)
			{
				float fitness=sortedList[i].GetComponent<RobotHandler>().calcFitness();
				sortedList [i].GetComponent<RobotHandler> ().enableLine ();
			}
	}
	//--------------------------------------------------------------
}

