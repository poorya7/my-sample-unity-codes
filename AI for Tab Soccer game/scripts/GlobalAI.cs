﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace AI
{
	
public static class GlobalAI  
{
	//------------------------------------------------------------------------------------------------------
	public static readonly float BALL_SLOW_VELOCITY=160;
	static readonly float xLimit=815;
	static readonly float yLimit=512;
	//------------------------------------------------------------------------------------------------------
	static GameObject  		  	m_goalKeeperAlpha;
	static GameObject			m_goalKeeperBeta;

	static GameObject  	 		m_potGoalKeeperAlpha;
	static GameObject		    m_potGoalKeeperBeta;

//	static GameObject		    m_halfBackAlpha1;
//	static GameObject		    m_halfBackAlpha2;
//	static GameObject			m_halfBackBeta1;
//	static GameObject			m_halfBackBeta2;

	static List<GameObject>		m_halfBacksAlpha=new List<GameObject>();
	static List<GameObject>		m_halfBacksBeta=new List<GameObject>();
	//------------------------------------------------------------------------------------------------------
	public static void setGoalKeeperAlpha(GameObject goalKeeper) { m_goalKeeperAlpha=goalKeeper; }
	public static void setGoalKeeperBeta(GameObject goalKeeper) { m_goalKeeperBeta=goalKeeper; }
	//------------------------------------------------------------------------------------------------------
	/*public static void resizeHalfBacks()
	{
		if (m_halfBackAlpha1!=null)
			m_halfBackAlpha1.transform.localScale=new Vector3(1,1,1);
		if (m_halfBackAlpha2!=null)
			m_halfBackAlpha2.transform.localScale=new Vector3(1,1,1);
		if (m_halfBackBeta1!=null)
			m_halfBackBeta1.transform.localScale=new Vector3(1,1,1);
		if (m_halfBackBeta2!=null)
			m_halfBackBeta2.transform.localScale=new Vector3(1,1,1);
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static float setBallOwnTime(float currentBallOwnTime, KickerLogic logic)
	{
		float newTime;
		if (GameManager.Instance.Ball.CurrentOwner==logic)
		{
			if (currentBallOwnTime<0)
				newTime=Time.time;
			else
				newTime=currentBallOwnTime;
		}
		else
		{
			newTime=-1;
		}
		return newTime;
	}
	//------------------------------------------------------------------------------------------------------
	public static float calcShootPower(Vector3 source, Vector3 goal, float maxAllowedPower)
	{
		float maxDistance=550;
		float distance=Vector3.Distance(source, goal);
		float power=(distance/maxDistance)*maxAllowedPower;
		power=(power<Settings.KickerMinShootPower)?Settings.KickerMinShootPower:power;
		power=(power>maxAllowedPower)?maxAllowedPower:power;
		return power;
	}
	//------------------------------------------------------------------------------------------------------
	public static void setMeAsHalfBack(KickerLogic logic) 
	{ 
		if (logic.Team==ETeam.ALPHA)
		{
			if (!m_halfBacksAlpha.Contains(logic.gameObject))
				m_halfBacksAlpha.Add(logic.gameObject);
		}
		else
		{
			if (!m_halfBacksBeta.Contains(logic.gameObject))
				m_halfBacksBeta.Add(logic.gameObject);
		}
	}
	//------------------------------------------------------------------------------------------------------
	public static bool freeHalfBackPosAvailable(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			return m_halfBacksAlpha.Count<2;
		else
			return m_halfBacksBeta.Count<2;
	}
	//------------------------------------------------------------------------------------------------------
	/*static void setHalfBackBeta(GameObject halfBack)
	{
		if (m_halfBackBeta1==null)
		{
			//halfBack.transform.localScale=new Vector3(1.2f ,1.2f ,1.2f);
			m_halfBackBeta1=halfBack; 
		}
		else if (m_halfBackBeta2==null)
		{
			//halfBack.transform.localScale=new Vector3(1.2f ,1.2f ,1.2f);
			m_halfBackBeta2=halfBack;
		}

	}
	//------------------------------------------------------------------------------------------------------
	static void setHalfBackAlpha(GameObject halfBack)
	{
		if (m_halfBackAlpha1==null)
		{
			//halfBack.transform.localScale=new Vector3(1.2f ,1.2f ,1.2f);
			m_halfBackAlpha1=halfBack; 
		}
		else if (m_halfBackAlpha2==null)
		{
			//halfBack.transform.localScale=new Vector3(1.2f ,1.2f ,1.2f);
			m_halfBackAlpha2=halfBack;
		}
	}*/
	//------------------------------------------------------------------------------------------------------
	public static void removeMeAsHalfBack(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			m_halfBacksAlpha.Remove(logic.gameObject);
		else
			m_halfBacksBeta.Remove(logic.gameObject);
		/*if (m_halfBackAlpha1==logic.gameObject)
			m_halfBackAlpha1=null;
		else if (m_halfBackAlpha2==logic.gameObject)
			m_halfBackAlpha2=null;
		if (m_halfBackBeta1==logic.gameObject)
			m_halfBackBeta1=null;
		else if (m_halfBackBeta2==logic.gameObject)
			m_halfBackBeta2=null;*/
		//logic.gameObject.transform.localScale=new Vector3(1,1,1);
	}
	//------------------------------------------------------------------------------------------------------
	public static GameObject getOtherHalfBack(KickerLogic logic) 
	{
		if (logic.Team==ETeam.ALPHA)
		{
			foreach(GameObject kicker in m_halfBacksAlpha)
			{
				if (kicker!=logic.gameObject)
					return kicker;
			}
		}
		else
		{
			foreach(GameObject kicker in m_halfBacksBeta)
			{
				if (kicker!=logic.gameObject)
					return kicker;
			}
		}
		return null;
	}
	//------------------------------------------------------------------------------------------------------
	public static void resetHalfBacks()
	{
		m_halfBacksAlpha.Clear();
		m_halfBacksBeta.Clear();
		//resizeHalfBacks();
//		m_halfBackAlpha1=null;
//		m_halfBackAlpha2=null;
//		m_halfBackBeta1=null;
//		m_halfBackBeta2=null;
	}
	//------------------------------------------------------------------------------------------------------
	public static bool amHalfBack(KickerLogic logic)
	{
//		GameObject kicker=logic.gameObject;
		if (m_halfBacksAlpha.Contains(logic.gameObject) || m_halfBacksBeta.Contains(logic.gameObject))
			return true;
		else
			return false;
		/*if (kicker==m_halfBackAlpha1 || kicker==m_halfBackAlpha2 || kicker==m_halfBackBeta1 || kicker==m_halfBackBeta2)
			return true;
		else
			return false;*/
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static void setHalfBackScales()
	{
		int count=0;
		foreach(KickerLogic logic in KickerManager.Instance.Kickers)
		{
			if (amHalfBack(logic))
			{
				count++;
				logic.transform.localScale=new Vector3(1.3f, 1.3f, 1.3f);
			}
			else
				logic.transform.localScale=new Vector3(1,1,1);
		}
		//Debug.Log(count);
	}
	//------------------------------------------------------------------------------------------------------
	public static void printHalfBackCount()
	{
		Debug.Log(m_halfBacksAlpha.Count+" "+m_halfBacksBeta.Count);
	}
	//------------------------------------------------------------------------------------------------------
	public static void setMeAsPotGoalKeeper(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			m_potGoalKeeperAlpha=logic.gameObject;
		else
			m_potGoalKeeperBeta=logic.gameObject;
	}
	//------------------------------------------------------------------------------------------------------
	public static GameObject getOurPotentialGoalKeeper(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			return m_potGoalKeeperAlpha;
		else
			return m_potGoalKeeperBeta;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 getDefenderPos(KickerLogic logic)
	{
		Vector3 pos=ourGoal(logic)+(otherGoal(logic)-ourGoal(logic)).normalized*300;
		return pos;
	}
	//------------------------------------------------------------------------------------------------------
	public static void reset()
	{
		setGoalKeeperAlpha(null);
		setGoalKeeperBeta(null);
		resetHalfBacks();
	}
	//------------------------------------------------------------------------------------------------------
	public static GameObject ourGoalKeeper(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			return m_goalKeeperAlpha;
		else
			return m_goalKeeperBeta;
	}
	//------------------------------------------------------------------------------------------------------
	public static bool isOurGoalEmpty(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			return m_goalKeeperAlpha==null;
		else
		{
			return m_goalKeeperBeta==null;
		}
	}
	//------------------------------------------------------------------------------------------------------
	public static bool isOtherGoalEmpty(KickerLogic logic)
	{
		if (logic.Team==ETeam.ALPHA)
			return m_goalKeeperBeta==null;
		else
			return m_goalKeeperAlpha==null;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 ourGoal(KickerLogic logic)
	{
		GameObject goal=null;
		if(logic.Team==ETeam.ALPHA)
			goal=GameObject.FindGameObjectWithTag("GoalAlpha");
		else
			goal=GameObject.FindGameObjectWithTag("GoalBeta");
		return goal.transform.position;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 otherGoal(KickerLogic logic)
	{
		GameObject goal=null;
		if(logic.Team==ETeam.ALPHA)
			goal=GameObject.FindGameObjectWithTag("GoalBeta");
		else
			goal=GameObject.FindGameObjectWithTag("GoalAlpha");
		return goal.transform.position;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool outOfField(Vector3 pos)
	{
		if (Mathf.Abs(pos.x)>xLimit || Mathf.Abs (pos.y)>yLimit)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static KickerLogic getClosestKickerToBall(KickerLogic logic, bool teamMemberOnly, bool exactBallPos=false, KickerLogic excludedKicker=null)
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 target;
		Vector3 ballStopPos=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		if (exactBallPos==true || ball.HasOwner || ball.Velocity.magnitude<GlobalAI.BALL_SLOW_VELOCITY || GlobalAI.outOfField(ballStopPos))
			target=ball.transform.position;
		else
			target=ballStopPos;
		float minDist=float.PositiveInfinity;
		KickerLogic closestKicker=logic;
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (teamMemberOnly==true && kicker.Team!=logic.Team)
				continue;
			if (excludedKicker!=null && kicker==excludedKicker)
				continue;
			float distToBallStopPos;
			distToBallStopPos=Vector3.Distance(kicker.transform.position, target);
			if (distToBallStopPos<minDist)
			{
				minDist=distToBallStopPos;
				closestKicker=kicker;
			}
		}
		return closestKicker;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}

}




































































//--------------------------------------------------------------------------------------------------------------------------------------------------