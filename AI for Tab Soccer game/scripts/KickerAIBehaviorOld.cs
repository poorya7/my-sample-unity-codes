using UnityEngine;
using System.Collections;

namespace AI
{
	
[RequireComponent(typeof(KickerLocomotion))]
public class KickerAIBehaviorOld : MonoBehaviour, IKickerAIBehavior
{
	//------------------------------------------------------------------------------------------------------
	private KickerLocomotion itsLocomotion;
	private KickerLogic itsKickerLogic;
	private Ball itsBall;
	private ETeam lastTeamScored;
	private bool itsIsEnabled;
	private bool amInOurGoal;
	float lastXTime;
	float lastX,lastY;
	float teamworkTime;
	bool teamworkStarted;
	float passingLongDistTime;
	bool passingLongStarted;
	float shootToGoalDriblTime;
	bool shootToGoalDriblStarted;
	Vector3 newDirection;
	string lastClosestKickerName;
	//------------------------------------------------------------------------------------------------------
	void OnEnable () 
	{
		itsIsEnabled = true;
	}
	//------------------------------------------------------------------------------------------------------
	void OnDisable () 
	{
		itsIsEnabled = false;
	}
	//------------------------------------------------------------------------------------------------------
	void Awake () 
	{
		itsLocomotion = this.GetComponent<KickerLocomotion>();
		itsKickerLogic = this.GetComponent<KickerLogic>();
		itsBall = GameManager.Instance.Ball;
		amInOurGoal=false;
	}
	//------------------------------------------------------------------------------------------------------
	void Start()
	{
		lastClosestKickerName="";
		newDirection=new Vector3(-10000,0,0);
		teamworkStarted=false;
		passingLongStarted=false;
		shootToGoalDriblStarted=false;
		lastX=0;
		lastY=0;
		lastXTime=Time.time;
	}
	//------------------------------------------------------------------------------------------------------
	void Update () 
	{
		if (Input.GetMouseButtonDown(0))
			Time.timeScale=1;

		if(!itsIsEnabled) return;

		if (iAmClosestToGoal(ourGoal(),null))
		{
			//BE A GOAL KEEPER!
			if (!iAmBehindOurGoal())
			{
				moveToGoal(ourGoal());
			}
			else
			{
				//I AM BEHIND OUR GOAL
				if (!itsBall.HasOwner && iAmClosestToBall(true))
				{
					//BALL IS FREE AND I AM CLOSEST TO IT
					//MOVE TO BALL
					moveToFreeBall();
				}
				else
				{
					float x=otherGoal().transform.position.x;
					Vector3 target=new Vector3(x,0,transform.position.z);
					moveTowards(target,null);
				}
			}
		}
		else
		{
			if (itsBall.HasOwner)
			{
				if (itsBall.CurrentOwner.Team==itsKickerLogic.Team)
				{
					//OUR TEAM HAS THE BALL
					playOffense();
				}
				else
				{
					playDefense();
				}
			}
			else
			{
				//BALL IS FREE!
				if (iAmClosestToBall(true))
				{
					moveToFreeBall();
				}
			}
		}
	}
	//------------------------------------------------------------------------------------------------------
	void moveToFreeBall()
	{
		//MOVE TO THE BALL
		Vector3 velocity=itsBall.Velocity;
		if (iAmClosestToGoal(ourGoal(),null))
		{
			//I AM THE GOAL KEEPER
			itsLocomotion.MoveIntoDirection(itsBall.transform.position-transform.position);
		}
		else if (velocity.magnitude<200)
			moveTowards(itsBall.transform.position,getClosestKicker(null,transform.position,false));
		else
		{
			Vector3 dest=itsBall.transform.position+velocity;
			moveTowards(dest,getClosestKicker(null,transform.position,false));
		}
	}
	//------------------------------------------------------------------------------------------------------
	void playOffense()
	{

		if (itsBall.CurrentOwner==itsKickerLogic)
		{
			//I HAVE THE BALL
			//MOVE TO OTHER GOAL

			if (iAmBehindEnemyGoal() || teamworkStarted)
			{
				//I AM BEHIND THE GOAL, PASS THE BALL
				KickerLogic closestTeamMate=getClosestKicker(null,transform.position,true);//.transform.position;
				moveTowards(closestTeamMate.transform.position,null);
				if (teamworkStarted==false)
				{
					teamworkTime=Time.time;
					teamworkStarted=true;
				}
				if (Time.time-teamworkTime>1f)
				{
					itsLocomotion.PassBall();
					teamworkStarted=false;
				}
			}
			else if (Vector3.Distance(transform.position, otherGoal().transform.position)<500 && iAmFacingEnemyGoal())
			{
				//NEAR ENEMY GOAL
				//IF PLAYER NEAR, PASS, ELSE
				//FACE COMPLETELY TO GOAL AND SHOOT
				KickerLogic teamMate=teamMateNear();
				if (teamMate!=null)
				{
					//TARGET IS NEAR PENALTY POINT
					Vector3 target=new Vector3(otherGoal().transform.position.x-Mathf.Sign(otherGoal().transform.position.x)*200,0,0);
					float teammatedist=Vector3.Distance(teamMate.transform.position,target);
					float mydist=Vector3.Distance(transform.position,target);
					float distanceToTeamMate=Vector3.Distance(transform.position,teamMate.transform.position);
					if (mydist>150 && teammatedist<mydist && distanceToTeamMate>350 && !opponentBetweenMeAndTeammate(teamMate) && kickerIsFree(teamMate))
						passBall(teamMate,0);
					else
						shootToGoal();
				}
				else
				{
					shootToGoal();
				}
			}
			else 
			{
				//NOT NEAR THE ENEMY GOAL YET
				//IF PLAYER AHEAD, PASS THE BALL, ELSE MOVE TOWARDS THE GOAL
				KickerLogic teammateAheadOfMe=teamMateAhead();
				if (teammateAheadOfMe!=null)
				{
					Vector3 target=new Vector3(otherGoal().transform.position.x-Mathf.Sign(otherGoal().transform.position.x)*200,0,0);
					float teammatedist=Vector3.Distance(teammateAheadOfMe.transform.position,target);
					float mydist=Vector3.Distance(transform.position,target);
					float distanceToTeamMate=Vector3.Distance(transform.position,teammateAheadOfMe.transform.position);
					if (teammatedist<mydist && distanceToTeamMate>350 && !opponentBetweenMeAndTeammate(teammateAheadOfMe) && kickerIsFree(teammateAheadOfMe))
					{
						passBall(teammateAheadOfMe,100);
					}
					else
					{
						//MOVE TO OTHER GOAL
						if (opponentBlockingMe())
						{
							KickerLogic freeTimeMate=findFreeTeamMate();
							if (freeTimeMate!=null && !opponentBetweenMeAndTeammate(freeTimeMate))
							{
								passBall(freeTimeMate,0);
							}
							else
							{
								keepMoving();
							}
						}
						else
						{
							keepMoving();
						}
					}
				}
				else
				{
					if (opponentBlockingMe())
					{
						KickerLogic freeTimeMate=findFreeTeamMate();
						if (freeTimeMate!=null && !opponentBetweenMeAndTeammate(freeTimeMate))
						{
							passBall(freeTimeMate,0);
						}
						else
						{
							keepMoving();
						}
					}
					else
					{
						//MOVE TO OTHER GOAL
						keepMoving();
					}
				}
			}
		}
		else
		{
			teamworkStarted=false;
			passingLongStarted=false;
			shootToGoalDriblStarted=false;
			supportOwnerOffense();
		}
	}
	//------------------------------------------------------------------------------------------------------
	void keepMoving()
	{
		float y=Random.Range(-100,100);
		Vector3 targetVar=new Vector3(otherGoal().transform.position.x,y,0);
		moveTowards(targetVar,null);
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic findFreeTeamMate()
	{
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name && aKickerLogic.Team==itsKickerLogic.Team)
			{
				//TEAM MATE
				if (kickerIsFree(aKickerLogic))
				{
					return aKickerLogic;
				}
			}
		}
		return null;
	}
	//------------------------------------------------------------------------------------------------------
	bool kickerIsFree(KickerLogic kicker)
	{
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=kicker.name && aKickerLogic.Team!=itsKickerLogic.Team)
			{
				//OPPONENT
				if (Vector3.Distance(aKickerLogic.transform.position,kicker.transform.position)<90)
					return false;
			}
		}
		return true;
	}
	//------------------------------------------------------------------------------------------------------
	bool opponentBlockingMe()
	{
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name && aKickerLogic.Team!=itsKickerLogic.Team)
			{
				//PLAYER FROM THE OTHER TEAM
				float opponentDistToTheirGoal=Vector3.Distance(aKickerLogic.transform.position,otherGoal().transform.position);
				float myDistToTheirGoal=Vector3.Distance(transform.position,otherGoal().transform.position);
				float opponentDistToMe=Vector3.Distance(aKickerLogic.transform.position,transform.position);
				float yDiff=aKickerLogic.transform.position.y-transform.position.y;
				if (opponentDistToTheirGoal<myDistToTheirGoal && opponentDistToMe<70 && iAmFacingEnemyGoal() && yDiff<25)
				{
					//OPPONENT BETWEEN ME AND THEIR GOAL
					return true;
				}
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool opponentBetweenMeAndTeammate(KickerLogic teammate)
	{
		Vector3 direction=(teammate.transform.position-transform.position).normalized;
		Vector3 startPoint=transform.position+direction*50;
		RaycastHit2D hit = Physics2D.Raycast(startPoint, direction);
		if (hit.collider != null) 
		{
			//			float distance = Mathf.Abs(hit.point.y - transform.position.y);
			if (hit.transform.tag=="Kicker")
			{
				string name=hit.transform.name;
				if ((name.Contains("ALPHA") && itsKickerLogic.Team==ETeam.BETA) || (name.Contains("BETA") && itsKickerLogic.Team==ETeam.ALPHA))
				{
					return true;
				}
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	void shootToGoal()
	{
		if (shootToGoalDriblStarted==false)
		{
			shootToGoalDriblTime=Time.time;
			shootToGoalDriblStarted=true;
		}
		else
		{
			Vector3 driblPoint=transform.position;
			if (itsLocomotion.TargetDirection.x<0)
				driblPoint.x-=100;
			else
				driblPoint.x+=100;
			if (itsLocomotion.TargetDirection.y>0)
				driblPoint.y-=100;
			else
				driblPoint.y+=100;
			moveTowards(driblPoint,null);
			if (Time.time-shootToGoalDriblTime>.2f)
			{
				float y;
				y=Random.Range(-80,80);
				Vector3 dir=new Vector3(otherGoal().transform.position.x-transform.position.x,y-transform.position.y,0);
				itsLocomotion.MoveIntoDirection(dir);
				itsLocomotion.ShootBall(Random.Range(Settings.KickerMinShootPower, Settings.KickerMaxShootPower));
			}
		}
	}
	//------------------------------------------------------------------------------------------------------
	void passBall(KickerLogic teamMate,float distanceAhead)
	{
		Vector3 passPoint=teamMate.transform.position;
		if (itsKickerLogic.Team==ETeam.ALPHA)
			passPoint.x+=distanceAhead;
		else
			passPoint.x-=distanceAhead;
		if (passingLongStarted==false)
		{
			passingLongDistTime=Time.time;
			passingLongStarted=true;
		}
		else
		{
			itsLocomotion.MoveIntoDirection(passPoint-transform.position);
			if (Time.time-passingLongDistTime>.2f)
			{
				float distance=Vector3.Distance(passPoint,transform.position);
				float power = (distance / 1200f) * Settings.KickerMaxShootPower;
				power = (power < Settings.KickerMinShootPower) ? Settings.KickerMinShootPower : power;
				itsLocomotion.ShootBall(power);
				passingLongStarted=false;
			}
		}
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic teamMateNear()
	{
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name && aKickerLogic.Team==itsKickerLogic.Team)
			{
				//THIS IS A TEAM MATE
				if (Mathf.Abs(aKickerLogic.transform.position.x-transform.position.x)<50)
					return aKickerLogic;
			}
		}
		return null;
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic teamMateAhead()
	{
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name && aKickerLogic.Team==itsKickerLogic.Team)
			{
				//THIS IS A TEAM MATE
				if (itsKickerLogic.Team==ETeam.ALPHA)
				{
					if (aKickerLogic.transform.position.x>transform.position.x)
					if (Vector3.Distance(aKickerLogic.transform.position,transform.position)>260)
						return aKickerLogic;
				}
				else
				{
					if (aKickerLogic.transform.position.x<transform.position.x)
					if (Vector3.Distance(aKickerLogic.transform.position,transform.position)>260)
						return aKickerLogic;
				}
			}
		}
		return null;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmFacingEnemyGoal()
	{
		float x=itsLocomotion.TargetDirection.x;
		float y=itsLocomotion.TargetDirection.y;
		//		float goalx=otherGoal().transform.position.x;
		//		float goaly=otherGoal().transform.position.y;
		ETeam team=itsKickerLogic.Team;
		if (team==ETeam.ALPHA)
		{
			if (x>0 &&  !iAmBehindEnemyGoal())
			{
				if ((transform.position.y<0 && y>0) || (transform.position.y>0 && y<0))
				{
					return true;
				}
			}
		}
		else if (team==ETeam.BETA)
		{
			if (x<0 &&  !iAmBehindEnemyGoal())
			{
				if ((transform.position.y<0 && y>0) || (transform.position.y>0 && y<0))
				{
					return true;
				}
			}
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmBehindOurGoal()
	{
		float x=transform.position.x;
		float y=transform.position.y;
		if (y<-140 || y>140)
		{
			if((itsKickerLogic.Team==ETeam.BETA && x>730) || (itsKickerLogic.Team==ETeam.ALPHA && x<-730))
				return true;
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmBehindEnemyGoal()
	{
		float x=transform.position.x;
		float y=transform.position.y;
		if (y<-180 || y>180)
		{
			if((itsKickerLogic.Team==ETeam.ALPHA && x>730) || (itsKickerLogic.Team==ETeam.BETA && x<-730))
				return true;
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	void moveToGoal(GameObject goal)
	{
		Vector3 penaltyPointDirection=(otherGoal().transform.position-goal.transform.position).normalized;
		Vector3 goalkeeperCenterPos=goal.transform.position+penaltyPointDirection*40;
		//MOVE TO OUR GOAL TO PROTECT IT WHILE OUR TEAM IS ATTACKING
		if (amInOurGoal==false)
		{
			if (Vector3.Distance(goalkeeperCenterPos,transform.position)>64)
			{
				//				float x=0-goalkeeperCenterPos.x;
				//				float newx=goalkeeperCenterPos.x+x/10;
				//				Vector3 target=new Vector3(newx,0,0);
				moveTowards(goalkeeperCenterPos,null);
			}
			else
			{
				amInOurGoal=true;
				//FACE THE RIGHT DIRECTION
				itsLocomotion.MoveIntoDirection(goalkeeperCenterPos-transform.position);
				itsLocomotion.MoveIntoDirection(transform.position-goalkeeperCenterPos);
			}
		}
		else
		{

			//I AM ALREADY IN OUR GOAL
			//PROTECT IF ENEMY IS APPROACHING!
			if (itsBall.HasOwner)
			{
				if (itsBall.CurrentOwner.Team!=itsKickerLogic.Team)
				{
					float x=transform.position.x;
					float yDiff= itsBall.CurrentOwner.transform.position.y-transform.position.y;
					float z=transform.position.z;
					float newDistToGoal= Vector3.Distance(goalkeeperCenterPos,new Vector3(x,itsBall.CurrentOwner.transform.position.y,z));
					if (newDistToGoal<180 && Mathf.Abs(yDiff)>5)
						itsLocomotion.MoveIntoDirection(new Vector3(0,yDiff,0));
				}
			}
			else
			{
				if (iAmClosestToBall(true))
				{
					//MOVE TO BALL
					itsLocomotion.MoveIntoDirection(itsBall.transform.position-transform.position);
				}
			}
			if (Vector3.Distance(goalkeeperCenterPos,transform.position)>70 && itsBall.HasOwner)
			{
				amInOurGoal=false;
			}
		}
	}
	//------------------------------------------------------------------------------------------------------
	void playDefense()
	{
		if (iAmClosestToGoal(ourGoal(),ourGoalKeeper()))
		{
			if(iAmClosestToBall(true))
			{
				//MOVE TO BALL OWNER
				moveTowards(itsBall.transform.position,itsBall.CurrentOwner);
			}
			else
			{
				Vector3 midPoint=(itsBall.gameObject.transform.position+ourGoal().transform.position)/2;
				moveTowards(midPoint,null);
			}
		}
		else if (iAmClosestToBall(false))
		{
			//MOVE TO BALL OWNER
			moveTowards(itsBall.transform.position,itsBall.CurrentOwner);
		}
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic ourGoalKeeper()
	{
		float minimumDistance=100000;
		KickerLogic closestPlayer=itsKickerLogic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team==itsKickerLogic.Team && aKickerLogic!=itsBall.CurrentOwner)
			{
				float distanceToOurGoal=Vector3.Distance(aKickerLogic.transform.position,ourGoal().transform.position);
				if (distanceToOurGoal<minimumDistance)
				{
					minimumDistance=distanceToOurGoal;
					closestPlayer=aKickerLogic;
				}
			}
		}
		return closestPlayer;
	}
	//------------------------------------------------------------------------------------------------------
	void supportOwnerOffense()
	{
		if (iAmClosestToGoal(ourGoal(),ourGoalKeeper()))
		{
			//STAY BEHIND
			if (Vector3.Distance(transform.position,ourGoal().transform.position)>300)
			{
				moveTowards(ourGoal().transform.position,null);
			}
			return ;
		}
		float diff=Vector3.Distance(itsBall.CurrentOwner.gameObject.transform.position,transform.position);
		//DON'T MOVE TOO FAR AWAY FROM OWNER
		if (diff<350 || iAmBehindOurPlayer())
		{
			float x=otherGoal().transform.position.x;
			float y=otherGoal().transform.position.y;
			KickerLogic otherOffenseKicker=otherOffense();
			if (otherOffenseKicker!=null && Vector3.Distance(transform.position,itsBall.CurrentOwner.transform.position)>150)
			{
				if (otherOffenseKicker.transform.position.y<0)
					y+=200;
				else
					y-=200;
			}
			else
			{
				if (itsBall.CurrentOwner.transform.position.y<0)
					y+=200;
				else
					y-=200;
			}

			Vector3 target=new Vector3(x,y,transform.position.z);
			float xDiff=transform.position.x-target.x;
			if (Mathf.Abs(xDiff)>250)
			{
				moveTowards(target,null);
			}
			else
			{
				target.x=transform.position.x;
				moveTowards(target,null);
			}
		}
		else if (Vector3.Distance(transform.position,ourGoal().transform.position)>250)
		{
			moveTowards(new Vector3(ourGoal().transform.position.x,transform.position.y,transform.position.z),null);
		}
	}

	//------------------------------------------------------------------------------------------------------
	KickerLogic otherOffense()
	{
		//		KickerLogic otherOffenseKicker=null;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name && aKickerLogic.Team==itsKickerLogic.Team)
			{
				if (aKickerLogic.name!=ourGoalKeeper().name && aKickerLogic.name!=closestToGoal(ourGoal(),ourGoalKeeper()).name)
				{
					if (aKickerLogic.name!=itsBall.CurrentOwner.name)
						return aKickerLogic;
				}
			}
		}
		return null;
	}
	//------------------------------------------------------------------------------------------------------
	void supportOwnerDefense()
	{
		float xDiff=Mathf.Abs(itsBall.CurrentOwner.gameObject.transform.position.x-transform.position.x);
		//		float diff=Vector3.Distance(itsBall.CurrentOwner.gameObject.transform.position,transform.position);
		if (xDiff>300)
		{
			moveTowards(itsBall.CurrentOwner.gameObject.transform.position,null);
		}
		//		else
		//			moveTowards(itsBall.CurrentOwner.gameObject.transform.position,null);
	}
	//------------------------------------------------------------------------------------------------------
	void moveTowards(Vector3 target,KickerLogic excludedKicker)
	{
		float minumumDistance=90;
		if (itsBall.HasOwner && itsBall.CurrentOwner.name==itsKickerLogic.name)
			minumumDistance+=30;
		KickerLogic closestKicker=getClosestKicker(excludedKicker,transform.position,false);
		float distance=Vector3.Distance(transform.position,closestKicker.transform.position);
		float x,y;
		if (Time.time-lastXTime>.6f)
		{
			//CHANGE X IF NECESSARY
			if (distance<minumumDistance && !kickerBehindMe(closestKicker))
			{
				if (newDirection.x==-10000 || lastClosestKickerName!=closestKicker.name)
					setNewDirection(target,closestKicker.transform.position);
				x=newDirection.x;
				y=newDirection.y;
			}
			else
			{
				newDirection.x=-10000;
				x=target.x-transform.position.x;
				y=target.y-transform.position.y;
			}
			lastX=x;
			lastY=y;
			lastXTime=Time.time;
		}
		else
		{
			x=lastX;
			y=lastY;
		}
		if(Vector3.Distance(transform.position,target)>2f)	
			itsLocomotion.MoveIntoDirection(new Vector3(x,y,0));
		lastClosestKickerName=closestKicker.name;
	}
	//------------------------------------------------------------------------------------------------------
	bool kickerBehindMe(KickerLogic kicker)
	{
		if (itsLocomotion.TargetDirection.x>0 && kicker.transform.position.x<transform.position.x)
		{
			if (itsLocomotion.TargetDirection.y>0 && kicker.transform.position.y<transform.position.y)
				return true;
			else if (itsLocomotion.TargetDirection.y<0 && kicker.transform.position.y>transform.position.y)
				return true;
		}
		if (itsLocomotion.TargetDirection.x<0 && kicker.transform.position.x>transform.position.x)
		{
			if (itsLocomotion.TargetDirection.y>0 && kicker.transform.position.y<transform.position.y)
				return true;
			else if (itsLocomotion.TargetDirection.y<0 && kicker.transform.position.y>transform.position.y)
				return true;
		}
		return false;
	}
	//------------------------------------------------------------------------------------------------------
	void setNewDirection(Vector3 target,Vector3 closestKicker)
	{
		float x,y;
		float margin=350;
		//log ("---");
		if (Mathf.Abs(target.x-closestKicker.x)<140)
		{
			//log ("x");
			//IF I'M ON THE LEFT SIDE, MOVE MORE TO THE LEFT
			//ELSE, MOVE MORE TO THE RIGHT
			if (transform.position.x<closestKicker.x)
			{
				x=closestKicker.x-margin-transform.position.x;
			}
			else
			{
				x=closestKicker.x+margin-transform.position.x;
			}
			y=target.y-transform.position.y;
		}
		else if (Mathf.Abs(target.y-closestKicker.y)<140)
		{
			//log ("y");
			x=target.x-transform.position.x;
			if (transform.position.y<closestKicker.y)
			{
				y=closestKicker.y-margin-transform.position.y;
			}
			else
			{
				y=closestKicker.y+margin-transform.position.y;
			}
		}
		else
		{
			//log ("else");
			x=closestKicker.x-margin-transform.position.x;
			y=closestKicker.y-transform.position.y;
		}

		newDirection.x=x;
		newDirection.y=y;
	}
	//------------------------------------------------------------------------------------------------------
	Vector3 minOfclosestKickerAndEdges(Vector3 closestKicker)
	{
		float yDiff1=transform.position.y-closestKicker.y;
		float yDiff2=transform.position.y-(-500);
		float yDiff3=transform.position.y-(+500);
		float min=Mathf.Min(Mathf.Abs(yDiff1),Mathf.Abs(yDiff2),Mathf.Abs(yDiff3));
		if (min==Mathf.Abs(yDiff1))
			return closestKicker;
		else if (min==Mathf.Abs(yDiff2))
			return (new Vector3(transform.position.x,-500,transform.position.z));
		else
			return (new Vector3(transform.position.x,500,transform.position.z));
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic getClosestKicker(KickerLogic excludedKicker,Vector3 newPosition,bool onlyMyTeam)
	{
		float minimumDistance=10000;
		KickerLogic closestKicker=null;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name)// && aKickerLogic.Team==itsKickerLogic.Team)
			{
				if (onlyMyTeam==true && aKickerLogic.Team!=itsKickerLogic.Team)
					continue;
				if (excludedKicker!=null && aKickerLogic==excludedKicker)
					continue;
				float distanceToKicker=Vector3.Distance(aKickerLogic.transform.position,newPosition);
				if (distanceToKicker<minimumDistance)// && Mathf.Abs(distanceToKicker-minimumDistance)>20)
				{
					minimumDistance=distanceToKicker;
					closestKicker=aKickerLogic;
				}
			}
		}
		return closestKicker;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmBehindOurPlayer()
	{
		KickerLogic owner=itsBall.CurrentOwner;
		bool cond1=transform.position.x<owner.transform.position.x && itsKickerLogic.Team==ETeam.ALPHA;
		bool cond2=transform.position.x>owner.transform.position.x && itsKickerLogic.Team==ETeam.BETA;
		if (cond1 || cond2)
			return true;
		else
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	void OnCollisionEnter2D(Collision2D collision) 
	{
		if(!itsIsEnabled) return;

		// check if the object we collidet with is either the ball or a kicker
		if(collision.gameObject.CompareTag("Ball"))
		{
			if(itsBall.HasOwner && itsBall.CurrentOwner.Team!=itsKickerLogic.Team)
			{
				//THE OTHER TEAM HAS THE BALL
				itsLocomotion.Tackle();
			}
		}
		else if (collision.gameObject.CompareTag("Kicker") && itsBall.HasOwner)
		{
			KickerLogic closestKicker=getClosestKicker(null,transform.position,false);
			if (closestKicker.name==itsBall.CurrentOwner.name && closestKicker.Team!=itsKickerLogic.Team)
				itsLocomotion.Tackle();
		}
	}
	//------------------------------------------------------------------------------------------------------
	GameObject ourGoal()
	{
		GameObject goal=null;
		if(itsKickerLogic.Team==ETeam.ALPHA)
			goal=GameObject.FindGameObjectWithTag("GoalAlpha");
		else
			goal=GameObject.FindGameObjectWithTag("GoalBeta");
		return goal;
	}
	//------------------------------------------------------------------------------------------------------
	GameObject otherGoal()
	{
		GameObject goal=null;
		if(itsKickerLogic.Team==ETeam.ALPHA)
			goal=GameObject.FindGameObjectWithTag("GoalBeta");
		else
			goal=GameObject.FindGameObjectWithTag("GoalAlpha");
		return goal;
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic closestToGoal(GameObject goal,KickerLogic excludedKicker)
	{
		float minimumDistance=100000;
		KickerLogic closestPlayer=itsKickerLogic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (excludedKicker!=null && excludedKicker.name==aKickerLogic.name)
				continue;
			if (aKickerLogic.Team==itsKickerLogic.Team && aKickerLogic!=itsBall.CurrentOwner)
			{
				float distanceToOurGoal=Vector3.Distance(aKickerLogic.transform.position,goal.transform.position);
				if (distanceToOurGoal<minimumDistance)
				{
					minimumDistance=distanceToOurGoal;
					closestPlayer=aKickerLogic;
				}
			}
		}
		return closestPlayer;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmClosestToGoal(GameObject goal,KickerLogic excludedKicker)
	{
		float minimumDistance=100000;
		KickerLogic closestPlayer=itsKickerLogic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (excludedKicker!=null && excludedKicker.name==aKickerLogic.name)
				continue;
			if (aKickerLogic.Team==itsKickerLogic.Team && aKickerLogic!=itsBall.CurrentOwner)
			{
				float distanceToOurGoal=Vector3.Distance(aKickerLogic.transform.position,goal.transform.position);
				if (distanceToOurGoal<minimumDistance)
				{
					minimumDistance=distanceToOurGoal;
					closestPlayer=aKickerLogic;
				}
			}
		}
		//log (closestPlayer.name);
		if (closestPlayer.name==itsKickerLogic.name)
			return true;
		else 
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmFarthestToOurOwner()
	{
		float maxDistance=-1;
		KickerLogic farthestMate=null;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team==itsKickerLogic.Team)
			{
				float distanceToOwner=Vector3.Distance(aKickerLogic.transform.position,itsBall.CurrentOwner.transform.position);
				if ((distanceToOwner>maxDistance))
				{
					maxDistance=distanceToOwner;
					farthestMate=aKickerLogic;
				}
			}
		}
		if (farthestMate==itsKickerLogic)
			return true;
		else
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmClosestToBall(bool considerDestination)
	{
		float minimumDistanceToBall=100000;
		KickerLogic closestPlayer=itsKickerLogic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team==itsKickerLogic.Team)// && aKickerLogic.UsesAI)
			{
				float distanceToBall;
				if (considerDestination==true)
				{
					Vector3 ballVelocity=itsBall.Velocity;
					Vector3 ballDestination=itsBall.transform.position+ballVelocity;//+(itsBall.Velocity);
					distanceToBall=Vector3.Distance(aKickerLogic.transform.position,ballDestination);
				}
				else
					distanceToBall=Vector3.Distance(aKickerLogic.transform.position,itsBall.transform.position);
				if (distanceToBall<minimumDistanceToBall)
				{
					minimumDistanceToBall=distanceToBall;
					closestPlayer=aKickerLogic;
				}
			}
		}
		if (closestPlayer==itsKickerLogic)
		{
			return true;
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------
	public void OnBallWasShot (Vector2 theForce)
	{
		if(!itsIsEnabled) return;
		// if you don't need this event, just ignore the function, but don't delete it.

	}
	//------------------------------------------------------------------------------------------------------
	public void OnBallWasPassed (KickerLogic thePassingKicker, KickerLogic theTargetKicker, Vector2 theForce)
	{
		if(!itsIsEnabled) return;
		// if you don't need this event, just ignore the function, but don't delete it.

	}
	//------------------------------------------------------------------------------------------------------
	public void OnBallHasNewOwner (KickerLogic theNewKicker)
	{
		if(!itsIsEnabled) return;
		// if you don't need this event, just ignore the function, but don't delete it.

	}
	//------------------------------------------------------------------------------------------------------
	public void OnSomeTackleHappened (KickerLogic theTacklingKicker, KickerLogic theVictimKicker)
	{
		if(!itsIsEnabled) return;
		// this event is triggered whenever any kicker tackles any other kicker
		// even if the victim is from the same team 
		// even if the victim doesn't even have the ball
	}
	//------------------------------------------------------------------------------------------------------
	public void OnTeamScoredGoal (ETeam theTeam, int theScoreTeamAlpha, int theScoreTeamBeta)
	{

		if(!itsIsEnabled) return;
		// if you don't need this event, just ignore the function, but don't delete it.
	}
	//------------------------------------------------------------------------------------------------------
	public bool Enabled
	{
		get { return enabled; }
		set { enabled = value; }
	}
}

}