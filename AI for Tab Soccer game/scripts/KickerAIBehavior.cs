﻿using UnityEngine;
using System.Collections;

namespace AI
{
	
[RequireComponent(typeof(KickerLocomotion))]
public class KickerAIBehavior : MonoBehaviour, IKickerAIBehavior 
{
	//------------------------------------------------------------------------------------------------------
	private KickerLocomotion itsLocomotion;
	private KickerLogic itsKickerLogic;
	private Ball itsBall;
	private bool itsIsEnabled;
	//------------------------------------------------------------------------------------------------------
	AITree		m_aiTree;
	//------------------------------------------------------------------------------------------------------
	void OnEnable () 
	{
		itsIsEnabled = true;
	}
	//------------------------------------------------------------------------------------------------------
	void OnDisable () 
	{
		itsIsEnabled = false;
	}
	//------------------------------------------------------------------------------------------------------
	void Awake () 
	{
		itsLocomotion = this.GetComponent<KickerLocomotion>();
		itsKickerLogic = this.GetComponent<KickerLogic>();
		itsBall = GameManager.Instance.Ball;
	}
	//------------------------------------------------------------------------------------------------------
	void Start()
	{
		initTrees();
	}
	//------------------------------------------------------------------------------------------------------
	void initTrees()
	{
		GlobalAI.reset();
		m_aiTree=new AITree(this.gameObject, itsLocomotion, itsKickerLogic);
	}
	//------------------------------------------------------------------------------------------------------
	void Update () 
	{
		//GlobalAI.setHalfBackScales();

		if(!itsIsEnabled) 
			return;

		//if (itsKickerLogic.Team==ETeam.ALPHA)
			//return;

		m_aiTree.Tick();
	}
	//------------------------------------------------------------------------------------------------------
	KickerLogic getClosestKicker(KickerLogic excludedKicker,Vector3 newPosition,bool onlyMyTeam)
	{
		float minimumDistance=10000;
		KickerLogic closestKicker=null;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.name!=itsKickerLogic.name)// && aKickerLogic.Team==itsKickerLogic.Team)
			{
				if (onlyMyTeam==true && aKickerLogic.Team!=itsKickerLogic.Team)
					continue;
				if (excludedKicker!=null && aKickerLogic==excludedKicker)
					continue;
				float distanceToKicker=Vector3.Distance(aKickerLogic.transform.position,newPosition);
				if (distanceToKicker<minimumDistance)// && Mathf.Abs(distanceToKicker-minimumDistance)>20)
				{
					minimumDistance=distanceToKicker;
					closestKicker=aKickerLogic;
				}
			}
		}
		return closestKicker;
	}
	//------------------------------------------------------------------------------------------------------
	bool iAmBehindOurPlayer()
	{
		KickerLogic owner=itsBall.CurrentOwner;
		bool cond1=transform.position.x<owner.transform.position.x && itsKickerLogic.Team==ETeam.ALPHA;
		bool cond2=transform.position.x>owner.transform.position.x && itsKickerLogic.Team==ETeam.BETA;
		if (cond1 || cond2)
			return true;
		else
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	void OnCollisionEnter2D(Collision2D collision) 
	{
		if(!itsIsEnabled) return;
		
		// check if the object we collidet with is either the ball or a kicker
		if(collision.gameObject.CompareTag("Ball"))
		{
			if(itsBall.HasOwner && itsBall.CurrentOwner.Team!=itsKickerLogic.Team)
			{
				//THE OTHER TEAM HAS THE BALL
				itsLocomotion.Tackle();
			}
		}
		else if (collision.gameObject.CompareTag("Kicker") && itsBall.HasOwner)
		{
			KickerLogic closestKicker=getClosestKicker(null,transform.position,false);
			if (closestKicker.name==itsBall.CurrentOwner.name && closestKicker.Team!=itsKickerLogic.Team)
				itsLocomotion.Tackle();
		}
	}
	//------------------------------------------------------------------------------------------------------
	public void OnBallWasShot (Vector2 theForce)
	{
		if(!itsIsEnabled) return;
	}
	//------------------------------------------------------------------------------------------------------
	public void OnBallWasPassed (KickerLogic thePassingKicker, KickerLogic theTargetKicker, Vector2 theForce)
	{
		if(!itsIsEnabled) return;
	}
	//------------------------------------------------------------------------------------------------------
	public void OnBallHasNewOwner (KickerLogic theNewKicker)
	{
		if(!itsIsEnabled) return;
	}
	//------------------------------------------------------------------------------------------------------
	public void OnSomeTackleHappened (KickerLogic theTacklingKicker, KickerLogic theVictimKicker)
	{
		if(!itsIsEnabled) return;
		// this event is triggered whenever any kicker tackles any other kicker
		// even if the victim is from the same team 
		// even if the victim doesn't even have the ball
	}
	//------------------------------------------------------------------------------------------------------
	public void OnTeamScoredGoal (ETeam theTeam, int theScoreTeamAlpha, int theScoreTeamBeta)
	{
		initTrees();
		if(!itsIsEnabled) return;
	}
	//------------------------------------------------------------------------------------------------------
	public bool Enabled
	{
		get { return enabled; }
		set { enabled = value; }
	}
	//------------------------------------------------------------------------------------------------------
}

}