﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class MoveToGoal : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	readonly float 		MAX_DISTANCE_TO_POSITION=30;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	Vector3 			m_goalFront;
	bool				m_reachedGoal;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public MoveToGoal(GameObject goalKeeper, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=goalKeeper;
		m_locomotion=locomotion;
		m_logic=logic;
		Vector3 ourGoal=GlobalAI.ourGoal(m_logic);
		Vector3 otherGoal=GlobalAI.otherGoal(m_logic);
		m_goalFront=ourGoal+(otherGoal-ourGoal).normalized*60;
		reset();
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void reset()
	{
		m_reachedGoal=false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		var selector1=add<Selector>();
		var sequence1=selector1.add<Sequence>();
		sequence1.add<Condition>().IsTrue=notReachedGoal;
		sequence1.add<Behavior>().Update=moveToGoal;
		var sequence2=selector1.add<Sequence>();
		sequence2.add<Condition>().IsTrue=notFacingFront;
		sequence2.add<Behavior>().Update=faceFront;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void setGlobalGoalKeeper()
	{
		if (m_logic.Team==ETeam.ALPHA)
			GlobalAI.setGoalKeeperAlpha(m_kicker);
		else
			GlobalAI.setGoalKeeperBeta(m_kicker);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notReachedGoal()
	{
		float distance=Vector3.Distance(m_kicker.transform.position,m_goalFront);
		if (distance>MAX_DISTANCE_TO_POSITION && m_reachedGoal==false)
		{
			return true;
		}
		else
		{
			//REACHED GOAL
			m_reachedGoal=true;
			setGlobalGoalKeeper();
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToGoal()
	{
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(m_goalFront-m_kicker.transform.position);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notFacingFront()
	{
		float distance=Vector3.Distance(m_kicker.transform.position,m_goalFront);
		if (distance<MAX_DISTANCE_TO_POSITION+2)
			return true;
		else
		{
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status faceFront()
	{
		Vector3 direction=(new Vector3(0,0,0)-m_kicker.transform.position).normalized;
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}
}
	















































































//--------------------------------------------------------------------------------------------------------------------------------------------------