﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Forward : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	readonly float 		SHOOT_DISTANCE=500;
	readonly float		MIN_DIST_TO_FORWARD=350;
	readonly float		MIN_DIST_TO_HUMAN=200;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	KickerLogic			m_teamMate;
	GameObject			m_kicker;
	Vector3 			m_ourGoal;
	Vector3				m_otherGoal;
	Vector3 			m_goalShootPos;
	float				m_ballOwnTime;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Forward(GameObject kicker, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=kicker;
		m_locomotion=locomotion;
		m_logic=logic;
		m_ourGoal=GlobalAI.ourGoal(m_logic);
		m_otherGoal=GlobalAI.otherGoal(m_logic);
		m_goalShootPos=Vector3.zero;
		m_ballOwnTime=-1;
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		add<Condition>().IsTrue=setBallOwnTime;
		add<Condition>().IsTrue=amBallOwner;
		var sel=add<Selector>();

		var seqBehindGoal=sel.add<Sequence>();
		seqBehindGoal.add<Condition>().IsTrue=amBehindOtherGoal;
		seqBehindGoal.add<Behavior>().Update=moveBackToField;


		var seqPassBall=sel.add<Sequence>();
		var selPassBall=seqPassBall.add<Selector>();
		selPassBall.add<Condition>().IsTrue=humanPresent;

		var seqTeamMateAhead=selPassBall.add<Sequence>();
		seqTeamMateAhead.add<Condition>().IsTrue=teamMateAheadOfMe;
		var selBlocked=seqTeamMateAhead.add<Selector>();
		selBlocked.add<Condition>().IsTrue=amBlocked;
		selBlocked.add<Condition>().IsTrue=amTooBehind;

		seqPassBall.add<Behavior>().Update=passBallAhead;

		var seqShoot=sel.add<Sequence>();
		seqShoot.add<Condition>().IsTrue=inShootRange;
		seqShoot.add<Condition>().IsTrue=hasEnoughPower;
		seqShoot.add<Condition>().IsTrue=foundEmptyGoalSlot;
		seqShoot.add<Behavior>().Update=shootToGoal;

		sel.add<Behavior>().Update=moveToOtherGoal;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool isHuman()
	{
		return !m_teamMate.UsesAI;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool setBallOwnTime()
	{
		m_ballOwnTime=GlobalAI.setBallOwnTime(m_ballOwnTime, m_logic);
		return true;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool posIsBehindOtherGoal(Vector3 pos)
	{
		float xLimit=750;
		float yLimit=140;
		if (Math.Abs(pos.x)>=xLimit && Mathf.Abs(pos.y)>=yLimit && Math.Sign(pos.x)==Mathf.Sign(m_otherGoal.x))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amBehindOtherGoal()
	{
		return posIsBehindOtherGoal(m_kicker.transform.position);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveBackToField()
	{
		Vector3 post= AIMath.findEmptyPosition(m_kicker.transform.position, -300, -300, 100, -100, m_logic, false , 0);
		//Vector3 dir=(post-m_kicker.transform.position).normalized;
		Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, post, m_logic, 80, 200);
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amBallOwner()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner==m_logic)
		{
			//GlobalAI.resetHalfBacks();
//			Vector3 pos= m_kicker.transform.position;
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool humanPresent()
	{
		foreach(KickerLogic logic in KickerManager.Instance.Kickers)
		{
			if (logic.Team==m_logic.Team && !logic.UsesAI)
			{
				bool aheadCond;
				/*if (m_ourGoal.x<0)
					aheadCond=logic.transform.position.x>m_kicker.transform.position.x;
				else
					aheadCond=logic.transform.position.x<m_kicker.transform.position.x;*/
				aheadCond=true;
				if (aheadCond)
				{
					if (Vector3.Distance(logic.transform.position, m_kicker.transform.position)>MIN_DIST_TO_HUMAN)
					{
						if (true)//distToClosestEnemy(logic.transform.position)>200)
						{
							//float xDiff=Math.Abs(m_kicker.transform.position.x-logic.transform.position.x);
							if (true)//xDiff>100)
							{
								m_teamMate=logic;
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool teamMateAheadOfMe()
	{
		foreach(KickerLogic logic in KickerManager.Instance.Kickers)
		{
			if (logic.Team==m_logic.Team)
			{
				bool aheadCond;
				if (m_ourGoal.x<0)
					aheadCond=logic.transform.position.x>m_kicker.transform.position.x;
				else
					aheadCond=logic.transform.position.x<m_kicker.transform.position.x;
				if (aheadCond && !posIsBehindOtherGoal(logic.transform.position))
				{
					if (Vector3.Distance(logic.transform.position, m_kicker.transform.position)>MIN_DIST_TO_FORWARD)
					{
						if (distToClosestEnemy(logic.transform.position)>200)
						{
							float xDiff=Math.Abs(m_kicker.transform.position.x-logic.transform.position.x);
							if (xDiff>100)
							{
								m_teamMate=logic;
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amBlocked()
	{
		float x=m_kicker.transform.position.x;
		float xAhead=x+200*(Mathf.Sign(m_otherGoal.x));
		Vector3 start=m_kicker.transform.position;
		Vector3 end=new Vector3(xAhead, m_kicker.transform.position.y, 0);
		return AIMath.boundsHit(start, end, m_logic, true);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amTooBehind()
	{
		float x=m_kicker.transform.position.x;
		if ((x<120 && m_otherGoal.x>0) || (x>-120 && m_otherGoal.x<0))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	float distToClosestEnemy(Vector3 teamMate)
	{
		float minimumDistance=float.PositiveInfinity;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team!=m_logic.Team)
			{
				float distToTeamMate=Vector3.Distance(aKickerLogic.transform.position,teamMate);
				if (distToTeamMate<minimumDistance)
				{
					minimumDistance=distToTeamMate;
				}
			}
		}
		return minimumDistance;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status passBallAhead()
	{
		Vector3 direction=(m_teamMate.transform.position-m_kicker.transform.position).normalized;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, m_teamMate, m_logic, 80, 200);
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(direction);
		float ballOwnDuration=Time.time-m_ballOwnTime;
		if (direction==AIMath.toV3(m_locomotion.TargetDirection) && ballOwnDuration>0.2f)
		{
			float lerpVal=Mathf.Clamp(ballOwnDuration/Settings.MaxShootChargeTime, 0f, 1f);
			float shootPower=Mathf.Lerp(Settings.KickerMinShootPower, Settings.KickerMaxShootPower, lerpVal);
			m_locomotion.ShootBall(GlobalAI.calcShootPower(m_kicker.transform.position, m_teamMate.transform.position, shootPower));
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool hasEnoughPower()
	{
		float ballOwnDuration=Time.time-m_ballOwnTime;
		float lerpVal=Mathf.Clamp(ballOwnDuration/Settings.MaxShootChargeTime, 0f, 1f);
		float shootPower=Mathf.Lerp(Settings.KickerMinShootPower, Settings.KickerMaxShootPower, lerpVal);

		float maxDistance=550;
		float distance=Vector3.Distance(m_kicker.transform.position, m_otherGoal);
		float neededPower=(distance/maxDistance)*shootPower;
		if (shootPower>=neededPower)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool inShootRange()
	{
		if (Vector3.Distance(m_kicker.transform.position, m_otherGoal)<=SHOOT_DISTANCE)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool foundEmptyGoalSlot()
	{
		float frontX=m_otherGoal.x+Mathf.Sign(m_ourGoal.x)*80;
//		Vector3 goalFront=new Vector3(frontX, m_otherGoal.y, 0);
//		float xDistance=Vector3.Distance(m_kicker.transform.position, goalFront);
		Vector3 pos= AIMath.findEmptyPositionToGoal(m_kicker.transform.position, frontX, 70, -70, m_logic, false , 0);
		if (pos==Vector3.zero)
			return false;
		else
		{
			m_goalShootPos=pos;
			return true;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status shootToGoal()
	{
		Vector3 pos=m_goalShootPos;
		Vector3 direction=(pos-m_kicker.transform.position).normalized;
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(direction);
		if (direction==AIMath.toV3(m_locomotion.TargetDirection))
		{
			float lerpVal=Mathf.Clamp((Time.time-m_ballOwnTime)/Settings.MaxShootChargeTime, 0f, 1f);
			float shootPower=Mathf.Lerp(Settings.KickerMinShootPower, Settings.KickerMaxShootPower, lerpVal);
			m_locomotion.ShootBall(shootPower);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentNearMe()
	{
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (kicker.Team==m_logic.Team)
				continue;
			float distToMe=Vector3.Distance(kicker.transform.position, m_kicker.transform.position);
			if (distToMe<80)
			{
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentInFrontOfMe()
	{
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (kicker.Team==m_logic.Team)
				continue;
			float distToMe=Vector3.Distance(kicker.transform.position, m_kicker.transform.position);
			bool inFront=Mathf.Abs(kicker.transform.position.x)>Mathf.Abs(m_kicker.transform.position.x);
			if (distToMe<100 && inFront)
			{
				return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToOtherGoal()
	{
		bool fromCenter=true;
		float yDistance=200;
		if (Mathf.Abs(m_kicker.transform.position.x)>350)
		{
			fromCenter=true;
			yDistance=300;
		}
		Vector3 dir=AIMath.calcFreeDirection(m_kicker.transform.position, m_otherGoal, m_logic, 100, yDistance, fromCenter);
		if (Mathf.Abs(m_kicker.transform.position.x)>240 || opponentNearMe())
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
		}
		else if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(dir);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}

}







































































