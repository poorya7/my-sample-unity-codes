using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Defender : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	readonly float 		MAX_DISTANCE_TO_POSITION=20;
	readonly float		DISTANCE_RANGE=350;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	Vector3				m_postPosition;
	bool				m_inPosition;
	Vector3 			m_ourGoal;
	float 				m_ballOwnTime;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public Defender(GameObject kicker, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=kicker;
		m_locomotion=locomotion;
		m_logic=logic;
		m_ourGoal=GlobalAI.ourGoal(m_logic);
		m_postPosition=GlobalAI.getDefenderPos(m_logic);
		m_inPosition=false;
		m_ballOwnTime=-1;
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		add<Condition>().IsTrue=setBallOwnTime;
		var selTop=add<Selector>();

		var seqIHaveBall=selTop.add<Sequence>();
		seqIHaveBall.add<Condition>().IsTrue=IHaveBall;
		seqIHaveBall.add<Behavior>().Update=shootBall;

		var seqDefend=selTop.add<Sequence>();
		seqDefend.add<Condition>().IsTrue=otherTeamHasBall;
		var selBlock=seqDefend.add<Selector>();
		var seqMoveToOpp=selBlock.add<Sequence>();
		seqMoveToOpp.add<Condition>().IsTrue=opponentInRange;
		seqMoveToOpp.add<Behavior>().Update=moveToOpponent;
		var seqBlock=selBlock.add<Sequence>();
		seqBlock.add<Condition>().IsTrue=opponentInOurField;
		seqBlock.add<Behavior>().Update=blockOpponent;

		var seqFreeBall=selTop.add<Sequence>();
		seqFreeBall.add<Condition>().IsTrue=ballFree;
		seqFreeBall.add<Condition>().IsTrue=ballCloseToOurGoal;
		seqFreeBall.add<Behavior>().Update=moveToBall;

		var selMovToPost=selTop.add<Selector>();
		var seqMovToPosition=selMovToPost.add<Sequence>();
		seqMovToPosition.add<Condition>().IsTrue=notInPosition;
		seqMovToPosition.add<Behavior>().Update=moveToPosition;
		var seqFaceFront=selMovToPost.add<Sequence>();
		seqFaceFront.add<Condition>().IsTrue=notFacingFront;
		seqFaceFront.add<Behavior>().Update=faceFront;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool setBallOwnTime()
	{
		m_ballOwnTime=GlobalAI.setBallOwnTime(m_ballOwnTime, m_logic);
		return true;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void reset()
	{
		m_inPosition=false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballFree()
	{
		if (!GameManager.Instance.Ball.HasOwner)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballCloseToOurGoal()
	{
		Ball ball=GameManager.Instance.Ball;
		float ballDist=Vector3.Distance(ball.transform.position, m_ourGoal);
		float frontDist=Vector3.Distance(m_postPosition, m_ourGoal);
		if (ballDist<frontDist)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToBall()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 ballStopPos=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector3 target;
		if (ball.Velocity.magnitude<GlobalAI.BALL_SLOW_VELOCITY || GlobalAI.outOfField(ballStopPos))
			target=ball.transform.position;
		else
			target=ballStopPos;
		//Vector3 direction=(target- m_kicker.transform.position).normalized;
		Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, target, m_logic, 80, 200);
		float distance=Vector3.Distance(target ,m_kicker.transform.position);
		if (distance>10)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
			m_locomotion.MoveIntoDirection(direction);
		}
		else
		{
			if (m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(false);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*bool amDefender()
	{
		return true;
		if (GlobalAI.isOurGoalEmpty(m_logic))
			;//return false;

		float minimumDistance=float.PositiveInfinity;
		KickerLogic closestPlayer=m_logic;
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (kicker.Team!=m_logic.Team || kicker.gameObject==GlobalAI.getOurPotentialGoalKeeper(m_logic))
				continue;
			float distToDefensePost;
			distToDefensePost=Vector3.Distance(kicker.transform.position,GlobalAI.getDefenderPos(m_logic));
			if (distToDefensePost<minimumDistance)
			{
				minimumDistance=distToDefensePost;
				closestPlayer=kicker;
			}
		}
		if (closestPlayer==m_logic)
		{
			Debug.1.2f(m_kicker.transform.position, GlobalAI.getDefenderPos(m_logic));
			return true;
		}
		return false;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool otherTeamHasBall()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team!=m_logic.Team)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentInRange()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		if (Math.Abs(forwardKicker.x)>=DISTANCE_RANGE && Math.Sign(forwardKicker.x)==Mathf.Sign(m_ourGoal.x))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToOpponent()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		Vector3 direction=(forwardKicker-m_kicker.transform.position).normalized;
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentInOurField()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		if (Mathf.Sign(forwardKicker.x)==Mathf.Sign(m_ourGoal.x))
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status blockOpponent()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		reset();
		Vector3 top=new Vector3(m_kicker.transform.position.x, 180, 0);
		Vector3 bottom=new Vector3(m_kicker.transform.position.x, -180, 0);
		Vector3 intersect=AIMath.getLinesIntersect(forwardKicker, m_ourGoal, top, bottom);
		if (intersect.x<-9999)
			return Status.Success;
		Vector3 direction=(intersect-m_kicker.transform.position).normalized;
		float distance=Vector3.Distance(intersect, m_kicker.transform.position);
		if (distance>30)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
			m_locomotion.MoveIntoDirection(direction);
		}
		else
		{
			if (m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(false);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notInPosition()
	{
		float distance=Vector3.Distance(m_kicker.transform.position,m_postPosition);
		if (distance>MAX_DISTANCE_TO_POSITION && m_inPosition==false)
			return true;
		else
		{
			m_inPosition=true;
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToPosition()
	{
		Vector3 direction=(m_postPosition-m_kicker.transform.position).normalized;
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notFacingFront()
	{
		//return false;
		Vector3 direction=(new Vector3(0, 0, 0)-m_kicker.transform.position).normalized;
		if (AIMath.toV3(m_locomotion.TargetDirection)!=direction)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status faceFront()
	{
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		Vector3 direction=(new Vector3(0, 0, 0)-m_kicker.transform.position).normalized;
		m_locomotion.MoveIntoDirection(direction);
//		if (true)
		//AIMath.v3(m_locomotion.Direction)!=direction)
		return Status.Success;
//		else
//			return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool IHaveBall()
	{
//		Vector3 pos=m_kicker.transform.position;
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner==m_logic)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status shootBall()
	{
		Vector3 pos=AIMath.findEmptyPosition(m_kicker.transform.position, 600, 700, 250, -250, m_logic);
		Vector3 direction=(pos-m_kicker.transform.position).normalized;
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(direction);
		float ballOwnDuration=Time.time-m_ballOwnTime;
		if (direction==AIMath.toV3(m_locomotion.TargetDirection) && ballOwnDuration>0.2f)
		{
			float lerpVal=Mathf.Clamp(ballOwnDuration/Settings.MaxShootChargeTime, 0f, 1f);
			float shootPower=Mathf.Lerp(Settings.KickerMinShootPower, Settings.KickerMaxShootPower, lerpVal);
			m_locomotion.ShootBall(GlobalAI.calcShootPower(m_kicker.transform.position, pos, shootPower));
		}
		return Status.Success;
	}

	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


}


























































//--------------------------------------------------------------------------------------------------------------------------------------------------