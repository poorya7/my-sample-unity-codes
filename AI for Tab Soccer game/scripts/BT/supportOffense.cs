﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class supportOffense : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
//	Vector3 			m_ourGoal;
	Vector3 			m_otherGoal;
	Vector3 			m_emptyPosition;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public supportOffense(GameObject kicker, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=kicker;
		m_locomotion=locomotion;
		m_logic=logic;
//		m_ourGoal=GlobalAI.ourGoal(m_logic);
		m_otherGoal=GlobalAI.otherGoal(m_logic);
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		add<Behavior>().Update=setEmptyPos;
		add<Behavior>().Update=moveToFreePost;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Vector3 findFreePost()
	{
//		float yBound=370;
		float maxX=670;
		Vector3 ourForward=GameManager.Instance.Ball.CurrentOwner.transform.position;
		Vector3 post;
		GameObject otherHalfBack=GlobalAI.getOtherHalfBack(m_logic);
		if (otherHalfBack==null || otherHalfBack.transform.position.y<=m_kicker.transform.position.y)
		{
			//POST TOP
			post= AIMath.findEmptyPosition(ourForward, 400, 500, 360, 100, m_logic, false, 0);
		}
		else
		{
			//POST BOTTOM
			post= AIMath.findEmptyPosition(ourForward, 400, 500, -360, -100, m_logic, false, 0);
		}
		if (Mathf.Abs(post.x)>maxX)// && Vector3.Distance(ourForward, m_kicker.transform.position)>250)
		{
			float diff=Math.Abs(post.x)-maxX;
			float val=-Mathf.Sign(m_otherGoal.x)*diff;
			float newX=post.x+val*2;
			post=new Vector3(newX, post.y, 0);
		}
		return post;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status setEmptyPos()
	{
		m_emptyPosition=findFreePost();
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToFreePost()
	{
		//Vector3 dir=(m_emptyPosition-m_kicker.transform.position).normalized;
		Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, m_emptyPosition, m_logic, 80, 200);
		float dist=Vector3.Distance(m_emptyPosition, m_kicker.transform.position);
		if (dist>30)
		{
			if (m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(false);
			m_locomotion.MoveIntoDirection(direction);
			if (GlobalAI.getClosestKickerToBall(m_logic, true, false)==m_logic)
				return Status.Success;
			else
				return Status.Running;
		}
		else
			return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


}




















































































