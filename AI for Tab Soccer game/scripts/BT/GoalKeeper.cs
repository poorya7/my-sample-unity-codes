using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class GoalKeeper : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
//	readonly float 		MAX_DISTANCE_TO_POSITION=30;
	readonly float 		DISTANCE_RANGE_1=670;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	Vector3 			m_goalFront;
	MoveToGoal			m_moveToGoalTree;
	DefendGoal			m_defendGoalTree;
	float 				m_ballOwnTime;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public GoalKeeper(GameObject goalKeeper, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=goalKeeper;
		m_locomotion=locomotion;
		m_logic=logic;
		m_moveToGoalTree=new MoveToGoal(m_kicker, m_locomotion, m_logic);
		m_defendGoalTree=new DefendGoal(m_kicker, m_locomotion, m_logic);
		Vector3 ourGoal=GlobalAI.ourGoal(m_logic);
		Vector3 otherGoal=GlobalAI.otherGoal(m_logic);
		m_goalFront=ourGoal+(otherGoal-ourGoal).normalized*60;
		m_ballOwnTime=-1;
		reset();
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void reset()
	{
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		add<Condition>().IsTrue=setBallOwnTime;

		var selector=add<Selector>();

		var seqDefendGoal=selector.add<Sequence>();
		//seqDefendGoal.add<Condition>().IsTrue=amGoalKeeper;
		seqDefendGoal.add<Condition>().IsTrue=underAttack;
		seqDefendGoal.add(m_defendGoalTree);

		var seqMoveToFreeBall=selector.add<Sequence>();
		seqMoveToFreeBall.add<Condition>().IsTrue=ballFree;
		seqMoveToFreeBall.add<Condition>().IsTrue=amClosestToBall;
		//seqMoveToFreeBall.add<Condition>().IsTrue=amGoalKeeper;
		seqMoveToFreeBall.add<Behavior>().Update=moveToFreeBall;

		var seqHasBall=selector.add<Sequence>();
		seqHasBall.add<Condition>().IsTrue=haveBall;
		//seqHasBall.add<Condition>().IsTrue=amGoalKeeper;
		seqHasBall.add<Behavior>().Update=shootBall;

		var seqMoveToGoal=selector.add<Sequence>();
		//var selNotInGoal=seqMoveToGoal.add<Selector>();
		//var seqNewGoalKeeper=selNotInGoal.add<Sequence>();
		//seqNewGoalKeeper.add<Condition>().IsTrue=goalEmpty;
		//seqNewGoalKeeper.add<Condition>().IsTrue=amClosestToOurGoal;
		//var seqBackToGoal=selNotInGoal.add<Sequence>();
		//seqBackToGoal.add<Condition>().IsTrue=amGoalKeeper;
		seqMoveToGoal.add<Behavior>().Update=addMeAsPotGoalKeeper;
		seqMoveToGoal.add(m_moveToGoalTree);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool setBallOwnTime()
	{
		m_ballOwnTime=GlobalAI.setBallOwnTime(m_ballOwnTime, m_logic);
		return true;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status addMeAsPotGoalKeeper()
	{
		GlobalAI.setMeAsPotGoalKeeper(m_logic);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amGoalKeeper()
	{
		if (GlobalAI.ourGoalKeeper(m_logic)==m_kicker)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool underAttack()
	{
		if (forwardBallOwnerApproaching() || ballIsMovingToOurGoal())
		{
			m_moveToGoalTree.reset();
			return true;
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool forwardBallOwnerApproaching()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team!=m_logic.Team)
		{
			//OTHER TEAM HAS THE BALL
			Vector3 forwardKicker=ball.CurrentOwner.transform.position;
			float distance=Vector3.Distance(GlobalAI.ourGoal(m_logic),forwardKicker);
			if (distance<=DISTANCE_RANGE_1)
				return true;
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballIsMovingToOurGoal()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner)
			return false;
		Vector3 ballStopPos=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector2 goalTop=new Vector2(m_goalFront.x, 120);
		Vector2 goalBottom=new Vector2(m_goalFront.x, -120);
		Vector2 intersect=AIMath.getLinesIntersect(ball.transform.position, ballStopPos, goalTop, goalBottom);
		bool rightDirection=Mathf.Sign(GlobalAI.ourGoal(m_logic).x)==Mathf.Sign(ball.Velocity.x);
		if (rightDirection && intersect.x>-9999)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool goalEmpty()
	{
		return GlobalAI.isOurGoalEmpty(m_logic);
	}
	//------------------------------------------------------------------------------------------------------
	bool amClosestToOurGoal()
	{
		Vector3 ourGoal=GlobalAI.ourGoal(m_logic);
		float minimumDistance=float.PositiveInfinity;
		KickerLogic closestPlayer=m_logic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team==m_logic.Team && aKickerLogic!=GameManager.Instance.Ball.CurrentOwner)
			{
				float distanceToOurGoal=Vector3.Distance(aKickerLogic.transform.position,ourGoal);
				if (distanceToOurGoal<minimumDistance)
				{
					minimumDistance=distanceToOurGoal;
					closestPlayer=aKickerLogic;
				}
			}
		}
		if (closestPlayer.name==m_logic.name)
			return true;
		else 
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool ballFree()
	{
		if (!GameManager.Instance.Ball.HasOwner)
			return true;
		else
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	bool amClosestToBall()
	{
		Ball ball=GameManager.Instance.Ball;
		float minimumDistanceToBall=float.PositiveInfinity;
		KickerLogic closestPlayer=m_logic;
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			float distanceToBall;
			distanceToBall=Vector3.Distance(kicker.transform.position,ball.transform.position);
			if (distanceToBall<minimumDistanceToBall)
			{
				minimumDistanceToBall=distanceToBall;
				closestPlayer=kicker;
			}
		}
		if (closestPlayer==m_logic)
		{
			return true;
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToFreeBall()
	{
		m_moveToGoalTree.reset();
		Ball ball=GameManager.Instance.Ball;
		//Vector3 direction=(ball.transform.position- m_kicker.transform.position).normalized;
		Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, ball.transform.position, m_logic, 80, 200);
//		float distance=Vector3.Distance(ball.transform.position,m_kicker.transform.position);
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool haveBall()
	{
		if (GameManager.Instance.Ball.CurrentOwner==m_logic)
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status shootBall()
	{
		Vector3 pos=AIMath.findEmptyPosition(m_kicker.transform.position, 600, 700, 250, -250, m_logic);
		Vector3 direction=(pos-m_kicker.transform.position).normalized;
		if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		m_locomotion.MoveIntoDirection(direction);
		m_locomotion.MoveIntoDirection(direction);
		float ballOwnDuration=Time.time-m_ballOwnTime;
		if (direction==AIMath.toV3(m_locomotion.TargetDirection) && ballOwnDuration>0.2f)
		{
			float lerpVal=Mathf.Clamp(ballOwnDuration/Settings.MaxShootChargeTime, 0f, 1f);
			float shootPower=Mathf.Lerp(Settings.KickerMinShootPower, Settings.KickerMaxShootPower, lerpVal);
			m_locomotion.ShootBall(GlobalAI.calcShootPower(m_kicker.transform.position, pos, shootPower));
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


}