﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class DefendGoal : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	readonly float 		DISTANCE_RANGE_1=670;
	readonly float		DISTANCE_RANGE_2=360;
	readonly float		DISTANCE_RANGE_3=220;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	Vector3				m_goalFront;
	float				m_distanceToOpponent;
	Vector3				m_ballStartPos;
	bool				m_reachedGoal;
	bool				m_readyToDefend;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public DefendGoal(GameObject goalKeeper, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=goalKeeper;
		m_locomotion=locomotion;
		m_logic=logic;
		m_goalFront=GlobalAI.ourGoal(m_logic)+(GlobalAI.otherGoal(m_logic)-GlobalAI.ourGoal(m_logic)).normalized*60;
		m_ballStartPos=Vector3.zero;
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		var selector0=add<Selector>();
		var seqOtherHasBall=selector0.add<Sequence>();

		seqOtherHasBall.add<Condition>().IsTrue=otherTeamHasBall;
		seqOtherHasBall.add<Behavior>().Update=calcDistanceToOpponent;
		seqOtherHasBall.add<Condition>().IsTrue=opponentForwardInRange1;

		var selector1=seqOtherHasBall.add<Selector>();

		var sequence1=selector1.add<Sequence>();
		sequence1.add<Condition>().IsTrue=opponentForwardInRange3;
		sequence1.add<Behavior>().Update=moveToOpponent;

		var sequence2=selector1.add<Sequence>();
		sequence2.add<Condition>().IsTrue=opponentForwardInRange2;
		sequence2.add<Behavior>().Update=blockOpponent;

		var sequence3=selector1.add<Sequence>();
		sequence3.add<Behavior>().Update=moveVertically;

		var seqBallWasShot=selector0.add<Sequence>();
		seqBallWasShot.add<Condition>().IsTrue=ballWasShot;
		var selBallDir=seqBallWasShot.add<Selector>();

		var seqToOurGoal=selBallDir.add<Sequence>();
		seqToOurGoal.add<Condition>().IsTrue=ballIsMovingToOurGoal;
		var selector2=seqToOurGoal.add<Selector>();
		var sequence4=selector2.add<Sequence>();
		sequence4.add<Condition>().IsTrue=notInYPos;
		sequence4.add<Behavior>().Update=moveToIntersect;
		selector2.add<Behavior>().Update=moveToBall;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amIGoalKeeper()
	{
		if (GlobalAI.ourGoalKeeper(m_logic)==m_kicker)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool otherTeamHasBall()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team!=m_logic.Team)
		{
			m_ballStartPos=Vector3.zero;
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status calcDistanceToOpponent()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		m_distanceToOpponent=Vector3.Distance(GlobalAI.ourGoal(m_logic),forwardKicker);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentForwardInRange1()
	{
		if (m_distanceToOpponent<DISTANCE_RANGE_1)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentForwardInRange3()
	{
		float kickerX=GameManager.Instance.Ball.CurrentOwner.transform.position.x;
		if (kickerX<=700 && kickerX>=-700 && m_distanceToOpponent<DISTANCE_RANGE_3)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentForwardInRange2()
	{
		float kickerX=GameManager.Instance.Ball.CurrentOwner.transform.position.x;
		if (kickerX<=700 && kickerX>=-700 && m_distanceToOpponent<DISTANCE_RANGE_2)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToOpponent()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 direction=(ball.transform.position-m_kicker.transform.position).normalized;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, ball.transform.position, m_logic, 80, 200);
		float distance=Vector3.Distance(m_kicker.transform.position, ball.transform.position);
		if (distance>20)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
			m_locomotion.MoveIntoDirection(direction);
		}
		else
			m_locomotion.Tackle();
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status blockOpponent()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		Vector3 kickerFront=forwardKicker+(GlobalAI.ourGoal(m_logic)-forwardKicker).normalized*100;
		Vector3 direction=(kickerFront-m_kicker.transform.position).normalized;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, kickerFront, m_logic, 80, 200);
		float distance=Vector3.Distance(m_kicker.transform.position, kickerFront);
		if (distance>20)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(false);
			m_locomotion.MoveIntoDirection(direction);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveVertically()
	{
		Vector3 forwardKicker=GameManager.Instance.Ball.CurrentOwner.transform.position;
		float y=forwardKicker.y;
		if (Math.Abs(y)>80)
			y=m_goalFront.y;
		Vector2 intersect=kickerTarget();
		Vector3 position;
		if (intersect.x>-9999)
			position=intersect;
		else
			position=new Vector3(m_goalFront.x,y,0);
		Vector3 direction=(position-m_kicker.transform.position).normalized;
		float distance=Vector3.Distance(position, m_kicker.transform.position);
		if (distance>20)
		{
			if (!m_locomotion.IsSprinting)
					m_locomotion.ToggleSprint(true);
				m_locomotion.MoveIntoDirection(direction);
		}
		else if (m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Vector2 kickerTarget()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 forwardKicker=ball.CurrentOwner.transform.position;
		Vector2 goalTop=new Vector2(m_goalFront.x, 100);
		Vector2 goalBottom=new Vector2(m_goalFront.x, -100);
		//Debug.1.2f(goalTop, goalBottom);
		Vector2 intersect=AIMath.getLinesIntersect(forwardKicker, ball.transform.position, goalTop, goalBottom, false);
		bool cond1=GlobalAI.ourGoal(m_logic).x>0 && ball.transform.position.x>forwardKicker.x;
		bool cond2=GlobalAI.ourGoal(m_logic).x<0 && ball.transform.position.x<forwardKicker.x;
		if (intersect.x>-9999 && intersect.y>=goalBottom.y && intersect.y<=goalTop.y && (cond1 || cond2))
			return intersect;
		else
			return new Vector2(-999999, -9999999);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballWasShot()
	{
		Ball ball=GameManager.Instance.Ball;
		if (!ball.HasOwner && ball.Velocity.magnitude>0)
		{
			if (m_ballStartPos==Vector3.zero)
				m_ballStartPos=ball.transform.position;
			return true;
		}
		else
		{
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballIsMovingToOurGoal()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 ballStopPos=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector2 goalTop=new Vector2(m_goalFront.x, 120);
		Vector2 goalBottom=new Vector2(m_goalFront.x, -120);
		Vector2 intersect=AIMath.getLinesIntersect(ball.transform.position, ballStopPos, goalTop, goalBottom);
		bool rightDirection=Mathf.Sign(GlobalAI.ourGoal(m_logic).x)==Mathf.Sign(ball.Velocity.x);
		if (rightDirection && intersect.x>-9999)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notInYPos()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 ballStopPosition=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector2 goalTop=new Vector2(m_goalFront.x, 120);
		Vector2 goalBottom=new Vector2(m_goalFront.x, -120);
		Vector2 intersect=AIMath.getLinesIntersect(ball.transform.position, ballStopPosition, goalTop, goalBottom);
		float distance=Vector3.Distance(intersect, m_kicker.transform.position);
		if (intersect.x>-9999)
		{
			if (distance>20)
				return true;
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToIntersect()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 ballStopPosition=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector2 goalTop=new Vector2(m_goalFront.x, 120);
		Vector2 goalBottom=new Vector2(m_goalFront.x, -120);
		Vector2 intersect=AIMath.getLinesIntersect(ball.transform.position, ballStopPosition, goalTop, goalBottom);
		float distance=Vector3.Distance(intersect, m_kicker.transform.position);
		Vector3 direction=(AIMath.toV3(intersect)- m_kicker.transform.position).normalized;
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		m_locomotion.MoveIntoDirection(direction);
		if (intersect.x>-9999 && distance>20)
			return Status.Running;
		else
			return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToBall()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 direction=(ball.transform.position- m_kicker.transform.position).normalized;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, ball.transform.position, m_logic, 80, 200);
//		float distance=Vector3.Distance(ball.transform.position,m_kicker.transform.position);
		m_locomotion.MoveIntoDirection(direction);
		if (!ball.HasOwner && ballIsMovingToOurGoal())
			return Status.Running;
		else
			return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amNotInGoal()
	{
		return !m_readyToDefend;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool notReachedGoal()
	{
		float distance=Vector3.Distance(m_kicker.transform.position,m_goalFront);
		if (distance>30 && m_reachedGoal==false)
		{
			return true;
		}
		else
		{
			//REACHED GOAL
			m_reachedGoal=true;
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToGoal()
	{
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(true);
		Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, m_goalFront, m_logic, 80, 200);
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool isNotFacingFront()
	{
		float distance=Vector3.Distance(m_kicker.transform.position,m_goalFront);
		if (distance<30+2)
			return true;
		else
		{
			m_readyToDefend=true;
			return false;
		}
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status faceFront()
	{
		if (!m_locomotion.IsSprinting)
			m_locomotion.ToggleSprint(false);
		Vector3 direction=(new Vector3(0,0,0)-m_kicker.transform.position).normalized;
		m_locomotion.MoveIntoDirection(direction);
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


}














































































//--------------------------------------------------------------------------------------------------------------------------------------------------