﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AI
{
	
//--------------------------------------------------------------------------------------------------------------------------------------------------
public enum Status
{
	Invalid,
	Running, 
	Success, 
	Failure 
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public interface IBehavior
{
	Status Tick();
	Status Status { get; set; }
	IBehavior Parent { get; set; }
	Action Init { set; }
	Func<Status> Update { set; }
	Action<Status> Terminate { set; }
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Behavior : IBehavior
{
	public string m_name;
	public Action Init { protected get; set; }
	public Func<Status> Update { protected get; set; }
	public Action<Status> Terminate { protected get; set; }
	public IBehavior Parent { get; set;}
	public Status Status { get; set; }
	public Status Tick() 
	{
		if (Status==Status.Invalid && Init!=null)
		{
			Init();
		}
		Status=Update();
		if (Status!=Status.Running && Terminate!=null)
		{
			Terminate(Status);
		}
		return Status;
	}
}	

//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Composite : Behavior 
{
	List<IBehavior> m_children;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	protected Composite()
	{
		m_children=new List<IBehavior>();
		Init=() => { };
		Terminate= status => { } ;
		Update = () => Status.Running;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public IBehavior getChild(int index)
	{
		return m_children[index];
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public int childCount { get { return m_children.Count; } } 
	public Composite add (Composite composite) { m_children.Add(composite); return composite; }
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public T add<T>() where T: class, IBehavior, new()
	{
		var t=new T();
		t.Parent=this;
		m_children.Add (t);
		return t;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Sequence : Composite
{
	private int m_childIndex;
	public Sequence()
	{
		Update= () => 
		{
//			if (m_name!=null)
//				Debug.Log(m_name);
			for (;;)
			{
				Status status=getChild(m_childIndex).Tick();
				if (status!=Status.Success)
				{
					if (status==Status.Failure)
					{
						m_childIndex=0;
					}
					return status;
				}
				else if (++m_childIndex==childCount)
				{
					m_childIndex=0;
					return Status.Success;
				}
			}
		};
		Init= () => { m_childIndex=0; };
	}
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Selector : Composite
{
	private int m_childIndex;
	public Selector()
	{
		Update= () => 
		{
			for (;;)
			{
				Status status=getChild(m_childIndex).Tick();
				if (status!=Status.Failure)
				{
					if (status==Status.Success)
					{
						m_childIndex=0;
					}
					return status;
				}
				else if (++m_childIndex==childCount)
				{
					m_childIndex=0;
					return Status.Failure;
				}
			}
		};
		Init= () => { m_childIndex=0; };
	}
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class Condition: Behavior
{
	public Func<bool> IsTrue { 	protected get; set; }
	public Condition()
	{
		Update =() => 
		{
			if (IsTrue!=null && IsTrue())
			{
				return Status.Success;
			}
			else
			{
				return Status.Failure;
			}
		};
	}
}
//--------------------------------------------------------------------------------------------------------------------------------------------------

}








































































