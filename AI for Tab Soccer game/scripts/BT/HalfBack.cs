﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class HalfBack : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
//	readonly float 		MAX_DISTANCE_TO_POSITION=20;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	bool				m_inPosition;
//	Vector3 			m_ourGoal;
	supportOffense		m_supportOffenseTree;
	supportDefense		m_supportDefenseTree;
	float 				m_ballOwnTime;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public HalfBack(GameObject kicker, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=kicker;
		m_locomotion=locomotion;
		m_logic=logic;
//		m_ourGoal=GlobalAI.ourGoal(m_logic);
		m_supportOffenseTree=new supportOffense(m_kicker, m_locomotion, m_logic);
		m_supportDefenseTree=new supportDefense(m_kicker, m_locomotion, m_logic);
		m_ballOwnTime=-1;
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		add<Condition>().IsTrue=setBallOwnTime;
		add<Condition>().IsTrue=amNotBallOwner;

		var selLow=add<Selector>();

		var seqOffense=selLow.add<Sequence>();
		seqOffense.add<Condition>().IsTrue=weHaveBall;
		Selector selOffense=seqOffense.add<Selector>();
		selOffense.add(m_supportOffenseTree);
		selOffense.add<Behavior>().Update=alwaysSucceed;

		var seqDefense=selLow.add<Sequence>();
		seqDefense.add<Condition>().IsTrue=otherHasBall;
		Selector selDefense=seqDefense.add<Selector>();
		selDefense.add(m_supportDefenseTree);
		selDefense.add<Behavior>().Update=alwaysSucceed;

	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool setBallOwnTime()
	{
		m_ballOwnTime=GlobalAI.setBallOwnTime(m_ballOwnTime, m_logic);
		return true;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool weHaveBall()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team==m_logic.Team)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool otherHasBall()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team!=m_logic.Team)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amNotBallOwner()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner!=m_logic)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status alwaysSucceed()
	{
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}

}










































































