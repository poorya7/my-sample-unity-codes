﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AI
{
//--------------------------------------------------------------------------------------------------------------------------------------------------
public class AITree : Sequence
{
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLocomotion 	m_locomotion;
	KickerLogic			m_logic;
	GameObject			m_kicker;
	GoalKeeper			m_goalKeeperTree;
//	BallPursuit			m_ballPursuitTree;
	Defender			m_defenderTree;
	HalfBack			m_halfBackTree;
	Forward				m_forward;
//	Vector3 			m_ourGoal;
	Vector3 			m_otherGoal;
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public AITree(GameObject kicker, KickerLocomotion locomotion, KickerLogic logic)
	{
		m_kicker=kicker;
		m_locomotion=locomotion;
		m_logic=logic;

		m_goalKeeperTree=new GoalKeeper(m_kicker, m_locomotion, m_logic);
		m_defenderTree=new Defender(m_kicker, m_locomotion, m_logic);
		m_halfBackTree=new HalfBack(m_kicker, m_locomotion, m_logic);
		m_forward=new Forward(m_kicker, m_locomotion, m_logic);
		m_otherGoal=GlobalAI.otherGoal(m_logic);

		reset();
		initBehavior();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public void reset()
	{
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void initBehavior()
	{
		var selector=add<Selector>();

		var seqGoalKeeper=selector.add<Sequence>();
		var selAmGoalKeeper=seqGoalKeeper.add<Selector>();
		selAmGoalKeeper.add<Condition>().IsTrue=amGoalKeeper;
		var seqGoalEmpty=selAmGoalKeeper.add<Sequence>();
		seqGoalEmpty.add<Condition>().IsTrue=goalEmpty;
		seqGoalEmpty.add<Condition>().IsTrue=amClosestToOurGoal;
		var selGoalKeeper=seqGoalKeeper.add<Selector>();
		selGoalKeeper.add(m_goalKeeperTree);
		selGoalKeeper.add<Behavior>().Update=alwaysSucceed;

		selector.add<Behavior>().Update=setHalfBackPos;

		var seqBallFree=selector.add<Sequence>();
		seqBallFree.add<Condition>().IsTrue=ballFree;
		var selBallFree=seqBallFree.add<Selector>();
		var seqClosestToBall=selBallFree.add<Sequence>();
		seqClosestToBall.add<Condition>().IsTrue=amClosestTeamMemberToBall;
		var selMoveToBall=seqClosestToBall.add<Selector>();
		selMoveToBall.add<Behavior>().Update=moveToBall;
		selMoveToBall.add<Behavior>().Update=alwaysSucceed;
		var seqNotClosestToBall=selBallFree.add<Sequence>();
		seqNotClosestToBall.add<Condition>().IsTrue=amHalfBack;
		var selMoveToPost=seqNotClosestToBall.add<Selector>();
		selMoveToPost.add<Behavior>().Update=moveToFreePost;
		selMoveToPost.add<Behavior>().Update=alwaysSucceed;

		var seqDefender=selector.add<Sequence>();
		seqDefender.add<Condition>().IsTrue=amDefender;
		var selDefender=seqDefender.add<Selector>();
		selDefender.add(m_defenderTree);
		selDefender.add<Behavior>().Update=alwaysSucceed;

		var seqMovToOtherForward=selector.add<Sequence>();
		seqMovToOtherForward.add<Condition>().IsTrue=otherTeamHasBall;
		seqMovToOtherForward.add<Condition>().IsTrue=amClosestToOtherForward;
		seqMovToOtherForward.add<Behavior>().Update=moveToOtherForward;

		selector.add(m_halfBackTree);

		selector.add(m_forward);


	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status setHalfBackPos()
	{
		if (amDefender() || amClosestTeamMemberToBall() || amBallOwner())
		{
//			Vector3 pos=m_kicker.transform.position;
			GlobalAI.removeMeAsHalfBack(m_logic);
		}
		else if (GlobalAI.freeHalfBackPosAvailable(m_logic))
		{
			GlobalAI.setMeAsHalfBack(m_logic);
		}
		return Status.Failure;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*Status setHalfBackScales()
	{
		if (amHalfBack())
			m_kicker.transform.localScale=new Vector3(1.3f, 1.3f, 1.3f);
		else
			m_kicker.transform.localScale=new Vector3(1,1,1);
		return Status.Failure;
	}*/
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amBallOwner()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner==m_logic)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amGoalKeeper()
	{
		if (m_kicker==GlobalAI.ourGoalKeeper(m_logic))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool goalEmpty()
	{
		return GlobalAI.isOurGoalEmpty(m_logic);
	}
	//------------------------------------------------------------------------------------------------------
	bool amClosestToOurGoal()
	{
		Vector3 ourGoal=GlobalAI.ourGoal(m_logic);
		float minimumDistance=float.PositiveInfinity;
		KickerLogic closestPlayer=m_logic;
		foreach(KickerLogic aKickerLogic in KickerManager.Instance.Kickers)
		{
			if (aKickerLogic.Team==m_logic.Team && aKickerLogic!=GameManager.Instance.Ball.CurrentOwner)
			{
				float distanceToOurGoal=Vector3.Distance(aKickerLogic.transform.position,ourGoal);
				if (distanceToOurGoal<minimumDistance)
				{
					minimumDistance=distanceToOurGoal;
					closestPlayer=aKickerLogic;
				}
			}
		}
		if (closestPlayer.name==m_logic.name)
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool ballFree()
	{
		if (!GameManager.Instance.Ball.HasOwner)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amClosestTeamMemberToBall(bool exactBallPos=false)
	{
		KickerLogic closestTeamMemberToBall=GlobalAI.getClosestKickerToBall(m_logic, true, exactBallPos);

		if(closestTeamMemberToBall==m_logic)
		{
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLogic getClosestOpponentToBall()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 target=ball.transform.position;
		float minDist=float.PositiveInfinity;
		KickerLogic closestKicker=m_logic;
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (kicker.Team==m_logic.Team)
				continue;
			float distToBallStopPos;
			distToBallStopPos=Vector3.Distance(kicker.transform.position, target);
			if (distToBallStopPos<minDist)
			{
				minDist=distToBallStopPos;
				closestKicker=kicker;
			}
		}
		return closestKicker;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Vector3 findFreePost(KickerLogic closestKickerToBall)
	{
//		float yBound=370;
		float maxX=670;
		Vector3 post;
		GameObject otherHalfBack=GlobalAI.getOtherHalfBack(m_logic);
		if (otherHalfBack==null || otherHalfBack.transform.position.y<m_kicker.transform.position.y)
		{
			//POST TOP
			post= AIMath.findEmptyPosition(closestKickerToBall.transform.position, 400, 500, 360, 100, m_logic, true, 0);
		}
		else
		{
			//POST BOTTOM
			post= AIMath.findEmptyPosition(closestKickerToBall.transform.position, 400, 500, -360, -100, m_logic, true, 0);
		}
		if (Mathf.Abs(post.x)>maxX)
		{
			float diff=Math.Abs(post.x)-maxX;
			float val=-Mathf.Sign(m_otherGoal.x)*diff;
			float newX=post.x+val*2;
			post=new Vector3(newX, post.y, 0);
		}
		return post;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToFreePost()
	{
		// SET ME AS HALFBACK
		//GlobalAI.setHalfBack(m_kicker);
		KickerLogic closestKickerToBall=GlobalAI.getClosestKickerToBall(m_logic,false);
		Vector3 pos=findFreePost(closestKickerToBall);
		//Vector3 dir=(pos-m_kicker.transform.position).normalized;
		Vector3 dir=AIMath.calcFreeDirection(m_kicker.transform.position, pos, m_logic, 80, 200);
		float dist=Vector3.Distance(m_kicker.transform.position, pos);
		if (dist>30)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(false);
			m_locomotion.MoveIntoDirection(dir);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amHalfBack()
	{
		bool am= GlobalAI.amHalfBack(m_logic);
		if (am && m_kicker.transform.position.y>0)
		{
		}
		return am;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amNotDefender()
	{
		return !amDefender();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	KickerLogic getOurDefender()
	{
		float minimumDistance=float.PositiveInfinity;
		KickerLogic closestPlayer=m_logic;
		foreach(KickerLogic kicker in KickerManager.Instance.Kickers)
		{
			if (kicker.Team!=m_logic.Team || kicker.gameObject==GlobalAI.getOurPotentialGoalKeeper(m_logic))
				continue;
			float distToDefensePost;
			distToDefensePost=Vector3.Distance(kicker.transform.position,GlobalAI.getDefenderPos(m_logic));
			if (distToDefensePost<minimumDistance)
			{
				minimumDistance=distToDefensePost;
				closestPlayer=kicker;
			}
		}
		return closestPlayer;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amDefender()
	{
//		if (GlobalAI.isOurGoalEmpty(m_logic))
//			return false;

		KickerLogic ourDefender=getOurDefender();
		if (ourDefender==m_logic)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status alwaysSucceed()
	{
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	void resetDefender()
	{
		m_defenderTree.reset();
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool opponentCloserToBallThanMe()
	{
		Vector3 closestOpponent=getClosestOpponentToBall().transform.position;
		Vector3 ball=GameManager.Instance.Ball.transform.position;
		if (Vector3.Distance(ball, closestOpponent)<=Vector3.Distance(ball, m_kicker.transform.position))
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToBall()
	{
		if (amDefender())
			resetDefender();
		Ball ball=GameManager.Instance.Ball;
		Vector3 ballStopPos=ball.transform.position+ new Vector3(ball.Velocity.x, ball.Velocity.y, 0);
		Vector3 target;

		if (ball.Velocity.magnitude<GlobalAI.BALL_SLOW_VELOCITY || GlobalAI.outOfField(ballStopPos) || opponentCloserToBallThanMe())
			target=ball.transform.position;
		else
			target=ballStopPos;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, target, m_logic, 80, 200);
		Vector3 direction=(target- m_kicker.transform.position).normalized;
		float distance=Vector3.Distance(target ,m_kicker.transform.position);
		if (distance>10)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
			m_locomotion.MoveIntoDirection(direction);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool otherTeamHasBall()
	{
		Ball ball=GameManager.Instance.Ball;
		if (ball.HasOwner && ball.CurrentOwner.Team!=m_logic.Team)
			return true;
		else 
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	bool amClosestToOtherForward()
	{
//		Ball ball=GameManager.Instance.Ball;
		KickerLogic ourDefender=getOurDefender();
		KickerLogic closestTeamMate=GlobalAI.getClosestKickerToBall(m_logic, true, true, ourDefender);
		if (closestTeamMate==m_logic)
		{
//			Vector3 pos=m_kicker.transform.position;
//			Debug.1.2f(pos, new Vector3(pos.x, pos.y+100, 0));
			return true;
		}
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	Status moveToOtherForward()
	{
		Ball ball=GameManager.Instance.Ball;
		Vector3 target=ball.CurrentOwner.transform.position;
		Vector3 direction=(target- m_kicker.transform.position).normalized;
		//Vector3 direction=AIMath.calcFreeDirection(m_kicker.transform.position, target, m_logic, 80, 200);
		float distance=Vector3.Distance(target ,m_kicker.transform.position);
		if (distance>10)
		{
			if (!m_locomotion.IsSprinting)
				m_locomotion.ToggleSprint(true);
			m_locomotion.MoveIntoDirection(direction);
		}
		return Status.Success;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}


}




































































