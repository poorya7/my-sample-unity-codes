﻿using UnityEngine;
using System.Collections;


namespace AI
{
	
public static class AIMath
{
	//------------------------------------------------------------------------------------------------------
	public static Vector2 getLinesIntersect(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, bool segmentOnly=true)
	{
		//Debug.1.2f(a1,a2);
		//Debug.1.2f(b1, b2);
		Vector2 intersect=new Vector2(-99999,-99999);
		if (!isLineVertical(a1,a2) && !isLineVertical(b1, b2))
		{
			float m1=(a2.y-a1.y)/(a2.x-a1.x);
			float m2=(b2.y-b1.y)/(b2.x-b1.x);
			if (m1==m2)
			{
				return intersect;
			}
			else
			{
				float d1=a1.y-m1*(a1.x);
				float d2=b1.y-m2*(b1.x);
				float x=(d2-d1)/(m1-m2);
				float y=m1*x+d1;
				float minX1=Mathf.Min(a1.x, a2.x);
				float minX2=Mathf.Min(b1.x, b2.x);
				float maxX1=Mathf.Max(a1.x, a2.x);
				float maxX2=Mathf.Max(b1.x, b2.x);
				if (segmentOnly==false)
					return new Vector2(x,y);
				else if (x>=minX1 && x<=maxX1 && x>=minX2 && x<=maxX2)
					return new Vector2(x,y);
			}
			return intersect;
		}
		else
			return getLinesIntersectVertical(a1, a2, b1, b2, segmentOnly);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static Vector2 getLinesIntersectVertical(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, bool segmentOnly)
	{
		if (isLineVertical(a1,a2) && isLineVertical(b1, b2))
		{
			return new Vector2(-99999,-99999);
		}
		else if (isLineVertical(a1,a2))
		{
			float m2=(b2.y-b1.y)/(b2.x-b1.x);
			float d2=b1.y-m2*(b1.x);
			float x=a1.x;
			float y=m2*x+d2;
			if (segmentOnly==false)
				return new Vector2(x,y);
			else if (y>=Mathf.Min(a1.y, a2.y) && y<=Mathf.Max(a1.y, a2.y) && x>=Mathf.Min(b1.x, b2.x) && x<=Mathf.Max(b1.x, b2.x))
			{
				return new Vector2(x,y);
			}
		}
		else if (isLineVertical(b1,b2))
		{
			float m1=(a2.y-a1.y)/(a2.x-a1.x);
			float d1=a1.y-m1*(a1.x);
			float x=b1.x;
			float y=m1*x+d1;
			if (segmentOnly==false)
				return new Vector2(x,y);
			else if (y>=Mathf.Min(b1.y, b2.y) && y<=Mathf.Max(b1.y, b2.y) && x>=Mathf.Min(a1.x, a2.x) && x<=Mathf.Max(a1.x, a2.x))
			{
				return new Vector2(x,y);
			}
		}
		return new Vector2(-99999,-99999);
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	static bool isLineVertical(Vector2 a1, Vector2 a2)
	{
		if (a1.x==a2.x)
			return true;
		else
			return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static Vector3 toV3(Vector2 v2)
	{
		return new Vector3(v2.x, v2.y, 0);
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 findEmptyPath(Vector3 origin, Vector3 goal, KickerLogic logic, float yDistance, bool fromCenter=true)
	{
//		float x=origin.x;
//		Vector3 startPoint=origin;
		Vector3 cross=Vector3.Cross(origin-goal,Vector3.forward);
		cross.Normalize();
		float width=yDistance;
		Vector3 endPointTop = width*0.5f*cross+goal;
		Vector3 endPointBottom = -width*0.5f*cross+goal;
		//Debug.DrawLine(endPointTop, endPointBottom);
		Vector3 position=goal;

		if (fromCenter)
		{
			position=getFreeIndex(goal, endPointTop, origin, logic);
			if (position==Vector3.zero)
				position=getFreeIndex(goal, endPointBottom, origin, logic);
		}
		else
		{
			position=getFreeIndex(endPointTop,endPointBottom, origin, logic);
		}
		if (position==Vector3.zero)
			position=goal;
		return position;	
	}
	//------------------------------------------------------------------------------------------------------
	static Vector3 getFreeIndex(Vector3 movementStart, Vector3 movemenetEnd, Vector3 source, KickerLogic logic)
	{
		float count=0;
		Vector3 direction=(movemenetEnd-movementStart).normalized;
		Vector3 currentPos=movementStart;
		while (count<200)
		{
			Vector3 lastPos=currentPos-direction*20;
			float dist=Vector3.Distance(currentPos, movemenetEnd);
			float lastDist=Vector3.Distance(lastPos, movemenetEnd);
			if (dist>lastDist)
				break;
			if (!AIMath.boundsHit(source, currentPos, logic) && !GlobalAI.outOfField(currentPos))
			{
				//Debug.DrawLine(source, currentPos);
				return currentPos;
			}
			currentPos+=direction*20;
			count++;
		}
		return Vector3.zero;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 calcFreeDirection(Vector3 kicker, Vector3 goal, KickerLogic logic, float nextCheckpointDistanceX, float yRange, bool fromCenter=true)
	{
		Vector3 nextCheckpoint=kicker+(goal-kicker).normalized*nextCheckpointDistanceX;
		Vector3 post=AIMath.findEmptyPath(kicker, nextCheckpoint, logic, yRange, fromCenter);
		//Debug.DrawLine(kicker, post);
		if (GlobalAI.outOfField(post))
			post=goal;
		Vector3 dir=(post-kicker).normalized;
		return dir;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 findEmptyPositionToGoal(Vector3 origin, float destX, float yTop, float yBottom, KickerLogic logic, bool onlyOtherTeam=false, float minDistance=0)
	{
		Vector3 startPoint=origin;
		float y=Mathf.Min(yTop, yBottom);
		Vector3 endPoint=Vector3.zero;
		while (y<=Mathf.Max(yTop, yBottom))
		{
			endPoint=new Vector3(destX, y, 0);
			if (!AIMath.boundsHit(startPoint, endPoint, logic, onlyOtherTeam, 30))
			{
				if (Vector3.Distance(endPoint, origin)>=minDistance)
					return endPoint;
			}
			y+=2;
		}
		return Vector3.zero;
	}
	//------------------------------------------------------------------------------------------------------
	public static Vector3 findEmptyPosition(Vector3 origin, float xIndexMin, float xIndexMax, float yTop, float yBottom, KickerLogic logic, bool onlyOtherTeam=false, float minDistance=0)
	{
		float x1=origin.x+Mathf.Sign(GlobalAI.otherGoal(logic).x)*xIndexMax;
		float x2=origin.x+Mathf.Sign(GlobalAI.otherGoal(logic).x)*xIndexMin;
		float xMax=Mathf.Max(x1, x2);
		float xMin=Mathf.Min(x1, x2);
		float x=xMax;
		Vector3 startPoint=origin;
		float y=Mathf.Max(yTop, yBottom);
//		float maxFreeDistance=0;
//		Vector3 direction=Vector3.zero;
		Vector3 endPoint=Vector3.zero;
		while (x>=xMin)
		{
			while (y>=Mathf.Min(yTop, yBottom))
			{
				endPoint=new Vector3(x, y, 0);
//				direction=(endPoint-startPoint).normalized;
				if (!AIMath.boundsHit(startPoint, endPoint, logic, onlyOtherTeam))
				{
					if (Vector3.Distance(endPoint, origin)>=minDistance)
						return endPoint;
				}
				y-=10;
			}
			x-=10;
		}
		return endPoint;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	public static bool boundsHit(Vector3 start, Vector3 end, KickerLogic logic, bool onlyOtherTeam=false, float width=40)
	{
		Vector3 direction=(end-start).normalized;
		Vector3 cross=Vector3.Cross(start-end,Vector3.forward);
		cross.Normalize();

		Vector3 startPointTop = width*cross+start;
		Vector3 endPointTop = width*cross+end;
		Vector3 startPointBottom = -width*cross+start;
		Vector3 endPointBottom = -width*cross+end;
		float distance=Vector3.Distance(start, end);
		bool cond1=hitPositive(start, direction, distance, logic, onlyOtherTeam);
		bool cond2=hitPositive(startPointTop, (endPointTop-startPointTop).normalized, distance, logic, onlyOtherTeam);
		bool cond3=hitPositive(startPointBottom, (endPointBottom-startPointBottom).normalized, distance, logic, onlyOtherTeam);

		if (cond1 || cond2 || cond3) 
			return true;
		else
			return false;
	}
	//------------------------------------------------------------------------------------------------------
	static bool hitPositive(Vector3 start, Vector3 direction, float distance, KickerLogic logic, bool onlyOtherTeam)
	{
		RaycastHit2D[] hits = Physics2D.RaycastAll(start, direction, distance);
		foreach(var hit in hits)
		{
			if (hit.collider != null && hit.transform.tag=="Kicker")
			{
				bool otherTeam=hit.transform.gameObject.GetComponent<KickerLogic>().Team!=logic.Team;
				if (onlyOtherTeam==true && otherTeam==false)
					continue;
				else
					return true;
			}
		}
		return false;
	}
	//--------------------------------------------------------------------------------------------------------------------------------------------------
}

}




































































//--------------------------------------------------------------------------------------------------------------------------------------------------