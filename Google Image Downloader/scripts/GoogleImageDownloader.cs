﻿using System;
using System.Collections;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;
//-------------------------------------------------------------------------
public class GoogleImageDownloader : MonoBehaviour
{
    Texture2D   _texture2d;
    string      _imageUrl = "";
    Sprite      _sprite;
    //-------------------------------------------------------------------------
    delegate void onCompleteCallBack(WWW www);
    //-------------------------------------------------------------------------
    public IEnumerator downloadSprite(string searchTerm)
    {
        yield return StartCoroutine(getImageUrl(searchTerm));
        yield return StartCoroutine(getTextureFromUrl());
        creatreSpriteFromTexture();
    }
    //-------------------------------------------------------------------------
    IEnumerator getImageUrl(string searchTerm)
    {
        searchTerm = cleanSearchTerm(searchTerm);
        String url = "https://www.googleapis.com/customsearch/v1?key="+key;
		url+="cx="+cx";
		url+=q="earchTerm + "+transparent";
        url += "&searchType=image&imgType=clipart&fileType=png&excludeTerms=logo&imgSize=medium";
        WWW www = new WWW(url);
        yield return StartCoroutine(WaitForRequest(www, parseJson));
    }
    //-------------------------------------------------------------------------
    string cleanSearchTerm(string searchTerm)
    {
        searchTerm = searchTerm.Trim().toLower();
        return searchTerm;
    }
    //-------------------------------------------------------------------------
    IEnumerator getTextureFromUrl()
    {
        WWW www = new WWW(_imageUrl);
        yield return StartCoroutine(WaitForRequest(www, downloadTexture));
    }
    //-------------------------------------------------------------------------
    IEnumerator WaitForRequest(WWW www, onCompleteCallBack onComplete)
    {
        yield return www;
        if (www.error == null)
            onComplete(www);
        else
        {
            Debug.Log("Error downloading image from google image.");
            Debug.Log(www.error);
        }
    }
    //-------------------------------------------------------------------------
    void parseJson(WWW www)
    {
        string results = www.text;
        try
        {
            var N = JSON.Parse(results);
            JSONNode node = N["items"];
            JSONArray array = (JSONArray)node;
            string imageUrl = array[0]["link"].ToString();
            if (!imageUrl.Equals("null"))
                _imageUrl = imageUrl;
            _imageUrl = _imageUrl.Substring(1, _imageUrl.Length - 2);
        }
        catch(Exception ex)
        {
            Debug.Log("Error parsing google image json: " + ex);
        }
    }
    //-------------------------------------------------------------------------
    void downloadTexture(WWW www)
    {
        _texture2d = www.texture;
    }
    //-------------------------------------------------------------------------
    void creatreSpriteFromTexture()
    {
        if (_texture2d == null)
            return;
        Rect rect = new Rect(0.0f, 0.0f, _texture2d.width, _texture2d.height);
        _sprite = Sprite.Create(_texture2d, rect, new Vector2(0.5f, 0.5f));
    }
    //-------------------------------------------------------------------------
    public Sprite getSprite()
    {
        return _sprite;
    }
    //-------------------------------------------------------------------------
}
